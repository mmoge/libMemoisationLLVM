#include <stdio.h>
#include <constraint/BoxConstraint.h>
#include <topology/Topology.h>
#include <state/State.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <iostream>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <core/Visitor.h>
#include <core/Context.h>

BoxConstraint::BoxConstraint()
: d_box("box",BoundingBox(),this) {}

void BoxConstraint::init() {
    const std::vector<TVec3> & X0 = this->getContext()->getMstate()->get(VecID::restPosition);

    BoundingBox box = d_box.getValue();

    for (int i=0;i<3;i++) {
        if (box.min[i] > box.max[i]) {
            double tmp = box.min[i];
            box.min[i] = box.max[i];
            box.max[i] = tmp;
        }
    }

    d_box.setValue(box);

    for (unsigned i=0;i<X0.size();i++) {
        TVec3 p = X0[i];

        if (p[0]>d_box.getValue().min[0] && p[0]<d_box.getValue().max[0] &&
            p[1]>d_box.getValue().min[1] && p[1]<d_box.getValue().max[1] &&
            p[2]>d_box.getValue().min[2] && p[2]<d_box.getValue().max[2]) {

            m_indices.push_back(i);
        }
    }
}

void BoxConstraint::applyConstraint(TVecId vf) {
    std::vector<TVec3> & f = this->getContext()->getMstate()->get(vf);

    for (unsigned i=0;i<m_indices.size();i++) {
        f[m_indices[i]] = TVec3();
    }
}


void BoxConstraint::draw(DisplayFlag flag) {
    if (! flag.isActive(DisplayFlag::STATE)) return;

    State * mstate = this->getContext()->getMstate();
    if (mstate == NULL) return;

    glColor3f(1.0f,0.4f,0.4f);
    glPointSize(10);
    glBegin(GL_POINTS);
    const std::vector<TVec3> & pos = mstate->get(VecID::position);

    for (unsigned i=0;i<m_indices.size();i++) {
        TVec3 p = pos[m_indices[i]];
        glVertex3fv(p.ptr());
    }
    glEnd();
    glPointSize(1);

    glBegin(GL_LINES);
    glColor3f(0.0f,0.0f,0.0f);
    TVec3 p0 = TVec3(d_box.getValue().min[0],d_box.getValue().min[1],d_box.getValue().min[2]);
    TVec3 p1 = TVec3(d_box.getValue().min[0],d_box.getValue().min[1],d_box.getValue().max[2]);
    TVec3 p2 = TVec3(d_box.getValue().min[0],d_box.getValue().max[1],d_box.getValue().min[2]);
    TVec3 p3 = TVec3(d_box.getValue().min[0],d_box.getValue().max[1],d_box.getValue().max[2]);
    TVec3 p4 = TVec3(d_box.getValue().max[0],d_box.getValue().min[1],d_box.getValue().min[2]);
    TVec3 p5 = TVec3(d_box.getValue().max[0],d_box.getValue().min[1],d_box.getValue().max[2]);
    TVec3 p6 = TVec3(d_box.getValue().max[0],d_box.getValue().max[1],d_box.getValue().min[2]);
    TVec3 p7 = TVec3(d_box.getValue().max[0],d_box.getValue().max[1],d_box.getValue().max[2]);

    glVertex3fv(p0.ptr());glVertex3fv(p1.ptr());
    glVertex3fv(p0.ptr());glVertex3fv(p2.ptr());
    glVertex3fv(p1.ptr());glVertex3fv(p3.ptr());
    glVertex3fv(p2.ptr());glVertex3fv(p3.ptr());

    glVertex3fv(p4.ptr());glVertex3fv(p5.ptr());
    glVertex3fv(p4.ptr());glVertex3fv(p6.ptr());
    glVertex3fv(p5.ptr());glVertex3fv(p7.ptr());
    glVertex3fv(p6.ptr());glVertex3fv(p7.ptr());

    glVertex3fv(p0.ptr());glVertex3fv(p4.ptr());
    glVertex3fv(p1.ptr());glVertex3fv(p5.ptr());
    glVertex3fv(p2.ptr());glVertex3fv(p6.ptr());
    glVertex3fv(p3.ptr());glVertex3fv(p7.ptr());

    glEnd();
    glLineWidth(1);
}

DECLARE_CLASS(BoxConstraint)
