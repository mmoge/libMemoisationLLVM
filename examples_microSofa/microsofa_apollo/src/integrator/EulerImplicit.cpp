#include <stdio.h>
#include <integrator/EulerImplicit.h>
#include <constraint/Constraint.h>
#include <core/Visitor.h>
#include <core/Context.h>
#include <solver/Solver.h>
#include <mapping/Mapping.h>

EulerImplicit::EulerImplicit()
: d_rayleighMass("rayleighMass",0.1,this)
, d_rayleighStiffness("rayleighStiffness",0.1,this)
, d_vdamping("vdamping",1.0,this)
{}

void EulerImplicit::step(double dt) {
    //f = 0
    VClearVisitor (VecID::force).execute(this->getContext());

    // add internal forces f += forces
    AddForceVisitor(VecID::force).execute(this->getContext());

    // add mapped forces on the mechanical state f += mappedForce
    MappingApplyJTVisitor(VecID::force).execute(this->getContext());

    // Create a mechanical params i.e. coefficients of the matrices define alpha, beta
    // compute v += (alpha*M + beta*K) * v
    MechanicalParams params1(-d_rayleighMass.getValue(),0.0,d_rayleighStiffness.getValue() + dt);
    AddDForceVisitor(params1,VecID::force,VecID::velocity).execute(this->getContext());

    // apply constraints i.e. f[constraint] = 0
    ApplyConstraintVisitor(VecID::force).execute(this->getContext());

    // solve (mfactor * M + kfactor*K) x = f
    MechanicalParams params2(1.0 + dt * d_rayleighMass.getValue(),0.0,-dt * (dt + d_rayleighStiffness.getValue()));
    SolverVisitor(params2,VecID::vec_x,VecID::force).execute(this->getContext());

    // apply constraints i.e. x[constraint] = 0
    ApplyConstraintVisitor(VecID::vec_x).execute(this->getContext());

    // Apply solution:  v = v + h a
    VPEqBFVisitor(VecID::velocity, VecID::vec_x, dt).execute(this->getContext());

    // v = v*d_vdamping
    VEqBFVisitor(VecID::velocity, VecID::velocity, d_vdamping.getValue()).execute(this->getContext());

    // x = x + h v
    VPEqBFVisitor(VecID::position, VecID::velocity, dt).execute(this->getContext());
}


DECLARE_CLASS(EulerImplicit)
