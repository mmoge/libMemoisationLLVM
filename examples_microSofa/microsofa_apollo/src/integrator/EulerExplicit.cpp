#include <stdio.h>
#include <integrator/EulerExplicit.h>
#include <constraint/Constraint.h>
#include <core/Visitor.h>
#include <core/Context.h>
#include <forcefield/ForceField.h>
#include <dataStructures.h>
#include <mapping/Mapping.h>

EulerExplicit::EulerExplicit()
: d_vdamping("vdamping",1.0,this) {}

void EulerExplicit::step(double dt) {
    //f = 0
    VClearVisitor (VecID::force).execute(this->getContext());

    //f += forces
    AddForceVisitor(VecID::force).execute(this->getContext());

    //f += mappedForce
    MappingApplyJTVisitor(VecID::force).execute(this->getContext());

    // solve M a = f
    SolveMassVisitor(VecID::vec_x, VecID::force).execute(this->getContext());

    // f[constraint] = 0
    ApplyConstraintVisitor(VecID::vec_x).execute(this->getContext());

    // Apply solution:  v = v + h a
    VPEqBFVisitor(VecID::velocity, VecID::vec_x, dt).execute(this->getContext());

    // v = v*d_vdamping
    VEqBFVisitor(VecID::velocity, VecID::velocity, d_vdamping.getValue()).execute(this->getContext());

    // x = x + h v
    VPEqBFVisitor(VecID::position, VecID::velocity, dt).execute(this->getContext());

}


DECLARE_CLASS(EulerExplicit)
