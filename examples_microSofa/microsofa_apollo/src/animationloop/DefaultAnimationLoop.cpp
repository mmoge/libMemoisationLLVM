#include <animationloop/DefaultAnimationLoop.h>
#include <stdio.h>
#include <loader/Loader.h>
#include <core/Visitor.h>
#include <integrator/Integrator.h>
#include <mapping/Mapping.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>


class IntegratorStepVisitor : public Visitor {
public:
    IntegratorStepVisitor(double d) : Visitor(true) {
        m_dt = d;
    }

    bool processObject(BaseObject *o) {
        if (dynamic_cast<Integrator *>(o)) {
            ((Integrator *) o)->step(m_dt);
        }

        return true;
    }

    double m_dt;
};

class MappingApplyVisitor : public Visitor {
public:
    MappingApplyVisitor() : Visitor(true) {}

    bool processObject(BaseObject *o) {
        if (dynamic_cast<Mapping *>(o)) {
            ((Mapping *) o)->apply();
        }

        return true;
    }
};

class PrintResultVisitor : public Visitor {
public:
    PrintResultVisitor() : Visitor(true)
    {
    }

    bool processObject(BaseObject *o) {
        if (dynamic_cast<ForceField *>(o)) {
            ((ForceField *) o)->printPosition();
            ((ForceField *) o)->printForce();
        }
        return true;
    }
    
};


void DefaultAnimationLoop::step() {
    double dt = this->getContext()->getDt();
    m_time+=dt;
    step_counter++;

    IntegratorStepVisitor(dt).execute(this->getContext());

    MappingApplyVisitor().execute(this->getContext());
    
    if ( this->print_frequence > 0 && step_counter%this->print_frequence == 0 )
    {
        PrintResultVisitor().execute(this->getContext());
    }
}


void DefaultAnimationLoop::draw(DisplayFlag flag) {
    if (!flag.isActive(DisplayFlag::STATS)) return;

    char buf[256];
    sprintf(buf,"Simulation Time: %.3f",m_time);

    int window_width = glutGet(GLUT_WINDOW_WIDTH);
    int window_height = glutGet(GLUT_WINDOW_HEIGHT);

    glMatrixMode( GL_PROJECTION ) ;
    glPushMatrix() ; // save
    glLoadIdentity();// and clear
    gluOrtho2D(0.5f, window_width+0.5f, 0.5f, window_height+0.5f);
    glMatrixMode( GL_MODELVIEW ) ;
    glPushMatrix() ;
    glLoadIdentity() ;

    glDisable( GL_DEPTH_TEST ) ; // also disable the depth test so renders on top

    GLfloat pos[4];
    glGetFloatv(GL_CURRENT_RASTER_POSITION, pos);

    glRasterPos2f( 10,10) ; // center of screen. (-1,0) is center left.

    int i=0;
    while (buf[i] != 0) {
        glutBitmapCharacter( GLUT_BITMAP_HELVETICA_12, buf[i] );
        i++;
    }

    glRasterPos4fv(pos) ;

    glEnable( GL_DEPTH_TEST ) ; // Turn depth testing back on

    glMatrixMode( GL_PROJECTION ) ;
    glPopMatrix() ; // revert back to the matrix I had before.
    glMatrixMode( GL_MODELVIEW ) ;
    glPopMatrix() ;
}

DECLARE_CLASS(DefaultAnimationLoop)

