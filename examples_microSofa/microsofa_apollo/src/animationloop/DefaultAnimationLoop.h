#ifndef DEFAULTANIMATIONLOOP_H
#define DEFAULTANIMATIONLOOP_H

#include <animationloop/AnimationLoop.h>

class DefaultAnimationLoop : public AnimationLoop {
public:

    DefaultAnimationLoop( int print_freq = 0 ) : AnimationLoop(print_freq) {}
    
    void step();

    void draw(DisplayFlag flag);

};
#endif
