#ifndef ANIMATIONLOOP_H
#define ANIMATIONLOOP_H

#include <core/BaseObject.h>
#include <dataStructures.h>
#include <vector>

class AnimationLoop : public BaseObject {
public:

    AnimationLoop( int printFrequence = 0 ) {
        m_time = 0.0;
        step_counter = 0;
        print_frequence = printFrequence;
    }

    virtual void step() = 0;
 
    void setFrequence( int freq )
    {
        print_frequence = freq;
    }

protected:
    double m_time;
    int step_counter;
    int print_frequence;

};
#endif
