#ifndef FORCEMODE_H
#define FORCEMODE_H 

#include <chrono>

extern int dforce_mode;
extern std::chrono::microseconds dforce_total_duration;
extern int dforce_total_calls;

#endif
