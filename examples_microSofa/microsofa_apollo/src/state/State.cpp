#include <stdio.h>
#include <state/State.h>
#include <topology/Topology.h>
#include <core/Visitor.h>
#include <core/Context.h>
#include <mapping/Mapping.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

State::State() {
    m_size = 0;
    m_mapped = false;
}

void State::vClear(TVecId va) {
    std::vector<TVec3> & f = this->get(va);

    for (unsigned int i=0;i<f.size();++i)
        f[i] = TVec3();
}

void State::vEqBF(TVecId va,TVecId vb,TReal  h) {
    std::vector<TVec3> & a = this->get(va);
    std::vector<TVec3> & b = this->get(vb);

    for (unsigned int i=0;i<a.size();++i)
        a[i] = b[i]*h;
}

void State::vPEqBF(TVecId va,TVecId vb,TReal  h) {
    std::vector<TVec3> & a = this->get(va);
    std::vector<TVec3> & b = this->get(vb);

    for (unsigned int i=0;i<a.size();++i)
        a[i] += b[i]*h;
}


void State::vOp(TVecId vres, TVecId va, TVecId vb, TReal f ) {
    std::vector<TVec3> & a = this->get(va);
    std::vector<TVec3> & b = this->get(vb);
    std::vector<TVec3> & res = this->get(vres);

    for (unsigned int i=0;i<res.size();++i)
        res[i] = a[i] + b[i]*f;
}

TReal State::vDot(TVecId va, TVecId vb) {
    std::vector<TVec3> & a = this->get(va);
    std::vector<TVec3> & b = this->get(vb);

    TReal sum = 0.0f;
    for (unsigned int i=0;i<a.size();++i)
        sum += a[i]*b[i];
    return sum;
}

void State::addPoint(TVec3 p) {
    std::vector<TVec3> & X = this->get(VecID::position);
    std::vector<TVec3> & X0 = this->get(VecID::restPosition);

    X.push_back(p);
    X0.push_back(p);

    m_size = X0.size();
}

unsigned State::size() {
    return m_size;
}

void State::computeBBox(BoundingBox & bbox) {
    std::vector<TVec3> & X = this->get(VecID::position);

    for (unsigned i=0;i<X.size();i++) {
        if (X[i][0] < bbox.min[0]) bbox.min[0] = X[i][0];
        if (X[i][1] < bbox.min[1]) bbox.min[1] = X[i][1];
        if (X[i][2] < bbox.min[2]) bbox.min[2] = X[i][2];

        if (X[i][0] > bbox.max[0]) bbox.max[0] = X[i][0];
        if (X[i][1] > bbox.max[1]) bbox.max[1] = X[i][1];
        if (X[i][2] > bbox.max[2]) bbox.max[2] = X[i][2];
    }
}

std::vector<TVec3> & State::get(TVecId v) {
    std::map<TVecId,std::vector<TVec3> *>::iterator it = m_mapState.find(v);

    if (it == m_mapState.end()) {
        std::vector<TVec3> * res = new std::vector<TVec3>();
        res->resize(size());

        if (d_printLog.getValue()) {
            std::cout << this->getName() << " CREATE " << v << " with size " << size() << std::endl;
        }

        m_mapState[v] = res;
        return *res;
    } else {
        return *it->second;
    }
}

void State::setMapped(bool b) {
    m_mapped = b;
}

bool State::isMechanical() {
    return ! m_mapped;
}

DECLARE_CLASS(State)
DECLARE_ALIAS(MechanicalObject,State)
