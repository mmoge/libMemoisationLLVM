#include <stdio.h>
#include <forcefield/UniformMass.h>
#include <state/State.h>

UniformMass::UniformMass()
: d_mass("mass",1.0,this)
{}

void UniformMass::addForce(TVecId v_f) {
    TVec3 m = this->getContext()->getGravity() * d_mass.getValue();

    std::vector<TVec3> & f = this->getContext()->getMstate()->get(v_f);
    for (unsigned int i=0;i<f.size();++i)
        f[i] += m;
}

void UniformMass::addDForce(MechanicalParams params,TVecId vdx,TVecId vdf) {
    double m = d_mass.getValue() * params.mfactor;

    std::vector<TVec3> & dx = this->getContext()->getMstate()->get(vdx);
    std::vector<TVec3> & df = this->getContext()->getMstate()->get(vdf);

    for (unsigned int i=0;i<df.size();++i) {
        dx[i] += df[i] * m;
    }
}

void UniformMass::accFromF(TVecId va, TVecId vf) {
    TReal inv_mass = 1.0f / d_mass.getValue();

    std::vector<TVec3> & a = this->getContext()->getMstate()->get(va);
    std::vector<TVec3> & f = this->getContext()->getMstate()->get(vf);

    for (unsigned int i=0;i<f.size();++i)
        a[i] = f[i] * inv_mass;
}

DECLARE_CLASS(UniformMass)
