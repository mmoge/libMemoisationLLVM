#include <forcefield/TetrahedronFEMForceField.h>
#include <state/State.h>
#include <stdio.h>
#include <iostream>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <core/Visitor.h>
#include <chrono>
#include <force_mode.h>
#include <fstream>
#include <cstring>
#include <cblas.h>

extern "C" void sgemv_   (   char*   TRANS,
        int*     M,
        int*     N,
        float*    ALPHA,
        float*      A,
        int*     LDA,
        float*      X,
        int*     INCX,
        float*    BETA,
        float*      Y,
        int*     INCY
    )   ;

extern void mysgemv_   (   char*   TRANS,
        int*     M,
        int*     N,
        float*    ALPHA,
        float*      A,
        int*     LDA,
        float*      X,
        int*     INCX,
        float*    BETA,
        float*      Y,
        int*     INCY
    );


TetrahedronFEMForceField::TetrahedronFEMForceField()
: d_youngModulus("youngModulus",100.0,this)
, d_poissonRatio("poissonRatio",0.4,this)
, d_method("method","large",this)
{}

void TetrahedronFEMForceField::init() {
    Topology * m_topology = this->getContext()->getTopology();
    if (m_topology == NULL) {
        std::cerr << "Error TetrahedronFEMForceField " << this->getName() << " cannot find the loader" << std::endl;
        return;
    }

    State * m_mstate = this->getContext()->getMstate();
    if (m_mstate == NULL) {
        std::cerr << "Error TetrahedronFEMForceField " << this->getName() << " cannot find the mstate" << std::endl;
        return;
    }

    TReal lambda = (d_youngModulus.getValue()*d_poissonRatio.getValue()) / ((1+d_poissonRatio.getValue())*(1-2*d_poissonRatio.getValue()));
    TReal mu = d_youngModulus.getValue() / (2*(1+d_poissonRatio.getValue()));
    TReal mu2 = mu*2.0;

    /// strain-stress matrix D
    TMat6x6 D(
        TMat6x6::Line(lambda+mu2, lambda    , lambda    ,  0,  0,  0),
        TMat6x6::Line(    lambda, lambda+mu2, lambda    ,  0,  0,  0),
        TMat6x6::Line(    lambda, lambda    , lambda+mu2,  0,  0,  0),
        TMat6x6::Line(         0,          0,          0, mu,  0,  0),
        TMat6x6::Line(         0,          0,          0,  0, mu,  0),
        TMat6x6::Line(         0,          0,          0,  0,  0, mu)
    );/// material stiffness matrix

    const std::vector<TVec3> & X0 = m_mstate->get(VecID::restPosition);

    // assemble Ke = Bt*D*B*vol(e)
    for (unsigned  eindex = 0; eindex < m_topology->getTetras().size(); ++eindex)
    {
        TTetra& tetra = m_topology->getTetras()[eindex];

        TVec3 a = X0[tetra[0]];
        TVec3 b = X0[tetra[1]];
        TVec3 c = X0[tetra[2]];
        TVec3 d = X0[tetra[3]];

        /// vertex matrix V
        TMat4x4 matV(
            TMat4x4::Line(1,a[0],a[1],a[2]),
            TMat4x4::Line(1,b[0],b[1],b[2]),
            TMat4x4::Line(1,c[0],c[1],c[2]),
            TMat4x4::Line(1,d[0],d[1],d[2])
        );

        TMat4x4 matVinv;
        TReal detVert = matV.invert(matVinv);

        /// strain-displacement matrix B
        const double b1 = matVinv[1][0]; const double b2 = matVinv[1][1]; const double b3 = matVinv[1][2]; const double b4 = matVinv[1][3];
        const double c1 = matVinv[2][0]; const double c2 = matVinv[2][1]; const double c3 = matVinv[2][2]; const double c4 = matVinv[2][3];
        const double d1 = matVinv[3][0]; const double d2 = matVinv[3][1]; const double d3 = matVinv[3][2]; const double d4 = matVinv[3][3];

        TMat6x12 B(
            TMat6x12::Line(b1, 0, 0,b2, 0, 0,b3, 0, 0,b4, 0, 0),
            TMat6x12::Line( 0,c1, 0, 0,c2, 0, 0,c3, 0, 0,c4, 0),
            TMat6x12::Line( 0, 0,d1, 0, 0,d2, 0, 0,d3, 0, 0,d4),
            TMat6x12::Line(c1,b1, 0,c2,b2, 0,c3,b3, 0,c4,b4, 0),
            TMat6x12::Line(d1, 0,b1,d2, 0,b2,d3, 0,b3,d4, 0,b4),
            TMat6x12::Line( 0,d1,c1, 0,d2,c2, 0,d3,c3, 0,d4,c4)
        );

        TMat12x12 Ke = (detVert/TReal(6.0)) * B.transposed() * D*B;

        m_vecKe.push_back(Ke);

        TVec3 X = b-a;X.normalize();
        TVec3 Y = cross(X,c-a);Y.normalize();
        TVec3 Z = cross(X,Y);Z.normalize();

        TMat3x3 R(X,Y,Z);
        TMat3x3 iR;
        R.invert(iR);
        m_iR.push_back(iR);
        m_R.push_back(R);
    }
}

void TetrahedronFEMForceField::addForce(TVecId vf) {
    const std::vector<TVec3> & x = this->getContext()->getMstate()->get(VecID::position);
    const std::vector<TVec3> & x0 = this->getContext()->getMstate()->get(VecID::restPosition);
    const std::vector<TTetra> & tetras = this->getContext()->getTopology()->getTetras();

    std::vector<TVec3> & f = this->getContext()->getMstate()->get(vf);

    for (unsigned int eindex=0;eindex<tetras.size();++eindex) {
        const TTetra & tetra = tetras[eindex];
        const TMat12x12 & Ke = m_vecKe[eindex];

        TVec3 u0,u1,u2,u3;
        if (d_method.getValue() == "large") {
            TVec3 X = x[tetra[1]]-x[tetra[0]];
            X.normalize();
            TVec3 Y = cross(X,x[tetra[2]]-x[tetra[0]]);
            Y.normalize();
            TVec3 Z = cross(X,Y);
            Z.normalize();
            m_R[eindex] = TMat3x3(X,Y,Z) * m_iR[eindex];

            u0 = m_R[eindex].transposed()  * (x[tetra[0]] - x0[tetra[0]]);
            u1 = m_R[eindex].transposed()  * (x[tetra[1]] - x0[tetra[1]]);
            u2 = m_R[eindex].transposed()  * (x[tetra[2]] - x0[tetra[2]]);
            u3 = m_R[eindex].transposed()  * (x[tetra[3]] - x0[tetra[3]]);
        } else {
            u0 = (x[tetra[0]] - x0[tetra[0]]);
            u1 = (x[tetra[1]] - x0[tetra[1]]);
            u2 = (x[tetra[2]] - x0[tetra[2]]);
            u3 = (x[tetra[3]] - x0[tetra[3]]);
        }

        TMat6x12::Line D(u0[0],u0[1],u0[2],
                         u1[0],u1[1],u1[2],
                         u2[0],u2[1],u2[2],
                         u3[0],u3[1],u3[2]);

        TMat6x12::Line F = Ke*D;

        if (d_method.getValue() == "large") {
            u0=m_R[eindex] * TVec3(F[0],F[1],F[2]);
            u1=m_R[eindex] * TVec3(F[3],F[4],F[5]);
            u2=m_R[eindex] * TVec3(F[6],F[7],F[8]);
            u3=m_R[eindex] * TVec3(F[9],F[10],F[11]);
        } else {
            u0=TVec3(F[0],F[1],F[2]);
            u1=TVec3(F[3],F[4],F[5]);
            u2=TVec3(F[6],F[7],F[8]);
            u3=TVec3(F[9],F[10],F[11]);
        }

        f[tetra[0]] -= u0;
        f[tetra[1]] -= u1;
        f[tetra[2]] -= u2;
        f[tetra[3]] -= u3;
    }
}

void TetrahedronFEMForceField::addDForceNormal(MechanicalParams params,TVecId vdx,TVecId vdf) {
    const std::vector<TTetra> & tetras = this->getContext()->getTopology()->getTetras();

    std::vector<TVec3> & dx = this->getContext()->getMstate()->get(vdx);
    const std::vector<TVec3> & df = this->getContext()->getMstate()->get(vdf);

    for (unsigned int eindex=0;eindex<tetras.size();++eindex) {
        const TTetra & tetra = tetras[eindex];
        const TMat12x12 & Ke = m_vecKe[eindex];

        TVec3 u0,u1,u2,u3;

        if (d_method.getValue() == "large") {
            u0 = m_R[eindex].transposed() * df[tetra[0]];
            u1 = m_R[eindex].transposed() * df[tetra[1]];
            u2 = m_R[eindex].transposed() * df[tetra[2]];
            u3 = m_R[eindex].transposed() * df[tetra[3]];
        } else {
            u0 = df[tetra[0]];
            u1 = df[tetra[1]];
            u2 = df[tetra[2]];
            u3 = df[tetra[3]];
        }

        TMat6x12::Line D(u0[0],u0[1],u0[2],
                         u1[0],u1[1],u1[2],
                         u2[0],u2[1],u2[2],
                         u3[0],u3[1],u3[2]);

        TMat6x12::Line F = Ke*D*params.kfactor;

        if (d_method.getValue() == "large") {
            u0=m_R[eindex] * TVec3(F[0],F[1],F[2]);
            u1=m_R[eindex] * TVec3(F[3],F[4],F[5]);
            u2=m_R[eindex] * TVec3(F[6],F[7],F[8]);
            u3=m_R[eindex] * TVec3(F[9],F[10],F[11]);
        } else {
            u0=TVec3(F[0],F[1],F[2]);
            u1=TVec3(F[3],F[4],F[5]);
            u2=TVec3(F[6],F[7],F[8]);
            u3=TVec3(F[9],F[10],F[11]);
        }

        dx[tetra[0]] -= u0;
        dx[tetra[1]] -= u1;
        dx[tetra[2]] -= u2;
        dx[tetra[3]] -= u3;
    }
}


void TetrahedronFEMForceField::addDForceApollo(MechanicalParams params,TVecId vdx,TVecId vdf) {
  const std::vector<TTetra> & tetras = this->getContext()->getTopology()->getTetras();
  std::vector<TVec3> & dx = this->getContext()->getMstate()->get(vdx);
  const std::vector<TVec3> & df = this->getContext()->getMstate()->get(vdf);

  /////////////////////////////////////////////////////////////////////////////
  // We need to copy the data contained in C++ classes to simpler container  //
  // (e.g. std::vector< int[4] > instead of std::vector<TTetra>).            //
  // Otherwise LLVM is not able to extract the loop using CodeExtractor      //
  // because there are multiple exits to the loop caused by "try catch"      //
  // from the C++ classes                                                    //
  /////////////////////////////////////////////////////////////////////////////
  // copy tetras in C_tetra
  std::vector< int[4] > C_tetras(tetras.size());
  for (int i = 0; i < tetras.size(); ++i)
  {
      C_tetras[i][0] = tetras[i][0];
      C_tetras[i][1] = tetras[i][1];
      C_tetras[i][2] = tetras[i][2];
      C_tetras[i][3] = tetras[i][3];
  }
  // copy dx in C_dx
  std::vector< float[3] > C_dx(dx.size());
  for (int i = 0; i < dx.size(); ++i)
  {
      C_dx[i][0] = dx[i][0];
      C_dx[i][1] = dx[i][1];
      C_dx[i][2] = dx[i][2];
  }
  // copy dF in C_df
  std::vector< float[3] > C_df(df.size());
  for (int i = 0; i < df.size(); ++i)
  {
      C_df[i][0] = df[i][0];
      C_df[i][1] = df[i][1];
      C_df[i][2] = df[i][2];
  }
  // copy m_vecKe
  std::vector< float* > C_m_vecKe( m_vecKe.size());
  for (int k = 0; k < m_vecKe.size(); ++k)
  {
      C_m_vecKe[k] = (float *) malloc( 12*12*sizeof( float ) );
      for (int i = 0; i < 12; ++i)
      {
          for (int j = 0; j < 12; ++j)
          {
              C_m_vecKe[k][ i*12+j ] = m_vecKe[k][i][j];
          }
      }
  }
  // copy m_R
  std::vector< float* > C_m_R( m_R.size() );
  for (int k = 0; k < m_R.size(); ++k)
  {
      C_m_R[k] = (float *) malloc( 3*3*sizeof( float ) );
      for (int i = 0; i < 3; ++i)
      {
          for (int j = 0; j < 3; ++j)
          {
              C_m_R[k][ i*3+j ] = m_R[k][i][j];
          }
      }
  }
  /////////////////////////////////////////////////////////////////////////////

  bool d_method_large = true;
  if (d_method.getValue() == "large")
  {
      d_method_large = true;
  }
  else
  {
      d_method_large = false;
  }

  float params_kfactor = (float) params.kfactor;
  float u0[3];
  float u1[3];
  float u2[3];
  float u3[3];
  int ione = 1;
  float fone = 1.;
  float fzero = 0.;
  int M_3 = 3;
  int N_3 = 3;
  int LDA_3 = 3;
  int M_12 = 12;
  int N_12 = 12;
  int LDA_12 = 12;
  char TRANS = 'T';
  // WARNING : we cannot define a dynamically allocated array as local !!!!
  //           because then there is no way to make a local version in
  //           apollo_loop as it is not possible to get the size at IR level
  float C_D[12];
  float C_F[12];
//  float* C_D = (float *) malloc( 12*sizeof( float ) );
//  float* C_F = (float *) malloc( 12*sizeof( float ) );

// We need to specify the variables that are local to the loop in
// the apollomem pragma, using keyword islocal.
// Otherwise in LLVM IR they are defined outside of the loop, and each
// iteration uses the same adress for theses variables, which causes
// race conditions when running multiple iterations in parallel.
#pragma apollomem islocal C_Ke u0 u1 u2 u3 tetra C_D C_F TRANS
{
    for (unsigned int eindex=0;eindex<C_tetras.size();++eindex) {
      const int (&tetra)[4] = C_tetras[eindex];
      float* C_Ke = C_m_vecKe[eindex];

      if (d_method_large)
      {
          TRANS = 'N';
          //sgemv : be careful to transpose C_m_R -> WARNING: sgemv uses col major storage
//          u0 = m_R[eindex].transposed() * df[tetra[0]];
//          u1 = m_R[eindex].transposed() * df[tetra[1]];
//          u2 = m_R[eindex].transposed() * df[tetra[2]];
//          u3 = m_R[eindex].transposed() * df[tetra[3]];
          mysgemv_ (   &TRANS, &M_3, &N_3, &fone,
              C_m_R[eindex],
              &LDA_3,
              C_df[tetra[0]],
              &ione,
              &fzero,
              u0,
              &ione
              );
          mysgemv_ (   &TRANS, &M_3, &N_3, &fone,
              C_m_R[eindex],
              &LDA_3,
              C_df[tetra[1]],
              &ione,
              &fzero,
              u1,
              &ione
              );
          mysgemv_ (   &TRANS, &M_3, &N_3, &fone,
              C_m_R[eindex],
              &LDA_3,
              C_df[tetra[2]],
              &ione,
              &fzero,
              u2,
              &ione
              );
          mysgemv_ (   &TRANS, &M_3, &N_3, &fone,
              C_m_R[eindex],
              &LDA_3,
              C_df[tetra[3]],
              &ione,
              &fzero,
              u3,
              &ione
              );
      }
      else
      {
          u0[0] = C_df[tetra[0]][0];
          u1[0] = C_df[tetra[1]][0];
          u2[0] = C_df[tetra[2]][0];
          u3[0] = C_df[tetra[3]][0];
          u0[1] = C_df[tetra[0]][1];
          u1[1] = C_df[tetra[1]][1];
          u2[1] = C_df[tetra[2]][1];
          u3[1] = C_df[tetra[3]][1];
          u0[2] = C_df[tetra[0]][2];
          u1[2] = C_df[tetra[1]][2];
          u2[2] = C_df[tetra[2]][2];
          u3[2] = C_df[tetra[3]][2];
      }
          std::memcpy( &(C_D[0]), &u0[0], 3*sizeof(float) );
          std::memcpy( &(C_D[3]), &u1[0], 3*sizeof(float) );
          std::memcpy( &(C_D[6]), &u2[0], 3*sizeof(float) );
          std::memcpy( &(C_D[9]), &u3[0], 3*sizeof(float) );
          //dgemv :
          TRANS = 'T';
          //sgemv - WARNING: sgemv uses col major storage
//          TMat6x12::Line F = Ke*D*params_kfactor;
          mysgemv_ (   &TRANS, &M_12, &N_12, &params_kfactor,
              C_Ke,
              &LDA_12,
              C_D,
              &ione,
              &fzero,
              C_F,
              &ione
              );

      if (d_method_large) {

          //sgemv - WARNING: sgemv uses col major storage
//        u0=m_R[eindex] * TVec3(F[0],F[1],F[2]);
//        u1=m_R[eindex] * TVec3(F[3],F[4],F[5]);
//        u2=m_R[eindex] * TVec3(F[6],F[7],F[8]);
//        u3=m_R[eindex] * TVec3(F[9],F[10],F[11]);
          mysgemv_ (   &TRANS, &M_3, &N_3, &fone,
                C_m_R[eindex],
                &LDA_3,
                &(C_F[0]),
                &ione,
                &fzero,
                u0,
                &ione
                );
          mysgemv_ (   &TRANS, &M_3, &N_3, &fone,
                C_m_R[eindex],
                &LDA_3,
                &(C_F[3]),
                &ione,
                &fzero,
                u1,
                &ione
                );
          mysgemv_ (   &TRANS, &M_3, &N_3, &fone,
                C_m_R[eindex],
                &LDA_3,
                &(C_F[6]),
                &ione,
                &fzero,
                u2,
                &ione
                );
          mysgemv_ (   &TRANS, &M_3, &N_3, &fone,
                C_m_R[eindex],
                &LDA_3,
                &(C_F[9]),
                &ione,
                &fzero,
                u3,
                &ione
                );
      } else {
          std::memcpy( u0, &(C_F[0]), 3*sizeof(float) );
          std::memcpy( u1, &(C_F[3]), 3*sizeof(float) );
          std::memcpy( u2, &(C_F[6]), 3*sizeof(float) );
          std::memcpy( u3, &(C_F[9]), 3*sizeof(float) );
      }
      C_dx[tetra[0]][0] -= u0[0];
      C_dx[tetra[1]][0] -= u1[0];
      C_dx[tetra[2]][0] -= u2[0];
      C_dx[tetra[3]][0] -= u3[0];
      C_dx[tetra[0]][1] -= u0[1];
      C_dx[tetra[1]][1] -= u1[1];
      C_dx[tetra[2]][1] -= u2[1];
      C_dx[tetra[3]][1] -= u3[1];
      C_dx[tetra[0]][2] -= u0[2];
      C_dx[tetra[1]][2] -= u1[2];
      C_dx[tetra[2]][2] -= u2[2];
      C_dx[tetra[3]][2] -= u3[2];
    }
}
    /////////////////////////////////////////////////////////////////////////////
    // We need to copy the C-style data back to the original C++ objects       //
    /////////////////////////////////////////////////////////////////////////////
    // copy C_dx in dx //
    for (int i = 0; i < dx.size(); ++i)
    {
        dx[i][0] = C_dx[i][0];
        dx[i][1] = C_dx[i][1];
        dx[i][2] = C_dx[i][2];
    }
}


void TetrahedronFEMForceField::addDForceOmp(MechanicalParams params,TVecId vdx,TVecId vdf) {
  const std::vector<TTetra> & tetras = this->getContext()->getTopology()->getTetras();

  std::vector<TVec3> & dx = this->getContext()->getMstate()->get(vdx);
  const std::vector<TVec3> & df = this->getContext()->getMstate()->get(vdf);

#pragma omp parallel for
  for (unsigned int eindex=0;eindex<tetras.size();++eindex) {
    const TTetra & tetra = tetras[eindex];
    const TMat12x12 & Ke = m_vecKe[eindex];

    TVec3 u0,u1,u2,u3;

    if (d_method.getValue() == "large") {
      u0 = m_R[eindex].transposed() * df[tetra[0]];
      u1 = m_R[eindex].transposed() * df[tetra[1]];
      u2 = m_R[eindex].transposed() * df[tetra[2]];
      u3 = m_R[eindex].transposed() * df[tetra[3]];
    } else {
      u0 = df[tetra[0]];
      u1 = df[tetra[1]];
      u2 = df[tetra[2]];
      u3 = df[tetra[3]];
    }

    TMat6x12::Line D(u0[0],u0[1],u0[2],
		     u1[0],u1[1],u1[2],
		     u2[0],u2[1],u2[2],
		     u3[0],u3[1],u3[2]);

    TMat6x12::Line F = Ke*D*params.kfactor;

    if (d_method.getValue() == "large") {
      u0=m_R[eindex] * TVec3(F[0],F[1],F[2]);
      u1=m_R[eindex] * TVec3(F[3],F[4],F[5]);
      u2=m_R[eindex] * TVec3(F[6],F[7],F[8]);
      u3=m_R[eindex] * TVec3(F[9],F[10],F[11]);
    } else {
      u0=TVec3(F[0],F[1],F[2]);
      u1=TVec3(F[3],F[4],F[5]);
      u2=TVec3(F[6],F[7],F[8]);
      u3=TVec3(F[9],F[10],F[11]);
    }
			
    dx[tetra[0]] -= u0;
    dx[tetra[1]] -= u1;
    dx[tetra[2]] -= u2;
    dx[tetra[3]] -= u3;
  }
}


void TetrahedronFEMForceField::addDForceOmpCritical(MechanicalParams params,TVecId vdx,TVecId vdf) {
  const std::vector<TTetra> & tetras = this->getContext()->getTopology()->getTetras();

  std::vector<TVec3> & dx = this->getContext()->getMstate()->get(vdx);
  const std::vector<TVec3> & df = this->getContext()->getMstate()->get(vdf);

#pragma omp parallel for
  for (unsigned int eindex=0;eindex<tetras.size();++eindex) {
    const TTetra & tetra = tetras[eindex];
    const TMat12x12 & Ke = m_vecKe[eindex];

    TVec3 u0,u1,u2,u3;

    if (d_method.getValue() == "large") {
      u0 = m_R[eindex].transposed() * df[tetra[0]];
      u1 = m_R[eindex].transposed() * df[tetra[1]];
      u2 = m_R[eindex].transposed() * df[tetra[2]];
      u3 = m_R[eindex].transposed() * df[tetra[3]];
    } else {
      u0 = df[tetra[0]];
      u1 = df[tetra[1]];
      u2 = df[tetra[2]];
      u3 = df[tetra[3]];
    }

    TMat6x12::Line D(u0[0],u0[1],u0[2],
		     u1[0],u1[1],u1[2],
		     u2[0],u2[1],u2[2],
		     u3[0],u3[1],u3[2]);

    TMat6x12::Line F = Ke*D*params.kfactor;

    if (d_method.getValue() == "large") {
      u0=m_R[eindex] * TVec3(F[0],F[1],F[2]);
      u1=m_R[eindex] * TVec3(F[3],F[4],F[5]);
      u2=m_R[eindex] * TVec3(F[6],F[7],F[8]);
      u3=m_R[eindex] * TVec3(F[9],F[10],F[11]);
    } else {
      u0=TVec3(F[0],F[1],F[2]);
      u1=TVec3(F[3],F[4],F[5]);
      u2=TVec3(F[6],F[7],F[8]);
      u3=TVec3(F[9],F[10],F[11]);
    }

#pragma omp critical
    {
      dx[tetra[0]] -= u0;
      dx[tetra[1]] -= u1;
      dx[tetra[2]] -= u2;
      dx[tetra[3]] -= u3;
    }
  }
}


void computeL2Error( std::vector<TVec3> vdx_ref, std::vector<TVec3> vdx,
        double& error_norm_dx_abs, double& error_norm_dx_rel )
{
    std::vector<TVec3> X_error_abs;  //erreur absolue
    std::vector<TVec3> X_error_rel;   //erreur relative
    X_error_abs.resize(vdx_ref.size());
    X_error_rel.resize(vdx_ref.size());
    for( size_t i =0; i<X_error_abs.size(); ++i)
    {
        X_error_abs[i] = vdx_ref[i] - vdx[i];
    }
    for( size_t i =0; i<X_error_rel.size(); ++i)
    {
        X_error_rel[i][0] = std::abs(vdx_ref[i][0] - vdx[i][0]) / std::abs(1+vdx_ref[i][0]);
        X_error_rel[i][1] = std::abs(vdx_ref[i][1] - vdx[i][1]) / std::abs(1+vdx_ref[i][1]);
        X_error_rel[i][2] = std::abs(vdx_ref[i][2] - vdx[i][2]) / std::abs(1+vdx_ref[i][2]);
    }
    for( size_t i =0; i<X_error_abs.size(); ++i)
    {
        error_norm_dx_abs += X_error_abs[i][0]*X_error_abs[i][0];
        error_norm_dx_abs += X_error_abs[i][1]*X_error_abs[i][1];
        error_norm_dx_abs += X_error_abs[i][2]*X_error_abs[i][2];
    }
    for( size_t i =0; i<X_error_rel.size(); ++i)
    {
        error_norm_dx_rel += X_error_rel[i][0]*X_error_rel[i][0];
        error_norm_dx_rel += X_error_rel[i][1]*X_error_rel[i][1];
        error_norm_dx_rel += X_error_rel[i][2]*X_error_rel[i][2];
    }
    error_norm_dx_abs = std::sqrt(error_norm_dx_abs);
    error_norm_dx_rel = std::sqrt(error_norm_dx_rel);
}


void TetrahedronFEMForceField::addDForce(MechanicalParams params,TVecId vdx,TVecId vdf) {

  std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    
  if (dforce_mode == 0) {
    addDForceNormal(params, vdx, vdf);
  } else if (dforce_mode == 1) {
    addDForceOmp(params, vdx, vdf);
  } else if (dforce_mode == 2) {
    addDForceOmpCritical(params, vdx, vdf);
  } else if (dforce_mode == 3) {
    addDForceApollo(params, vdx, vdf);
  } else if (dforce_mode == 11) {   //compare addDForceNormal and addDForceApollo
      // this has not been tested in a ling time - might crash or give wrong results
      std::vector<TVec3> & dx = this->getContext()->getMstate()->get(vdx);
      std::vector<TVec3> & df = this->getContext()->getMstate()->get(vdf);
      //values before modif
      std::vector<TVec3> dx_original = dx;
      std::vector<TVec3> df_original = df;
      //get omp results
      addDForceApollo(params, vdx, vdf);
      std::vector<TVec3> dx_omp = dx;
      std::vector<TVec3> df_omp = df;
      //reset values
      dx = dx_original;
      df =  df_original;
      //get reference results
      addDForceNormal(params, vdx, vdf);
      std::vector<TVec3> dx_ref = dx;
      std::vector<TVec3> df_ref = df;
      //compute error
      double l2norm_dx_abs = 0.;  //erreur absolue
      double l2norm_dx_rel = 0.;  //erreur relative
      computeL2Error(dx_ref, dx_omp, l2norm_dx_abs, l2norm_dx_rel );
      //print error
        std::cout << "addDForceApollo - VecID::position  L2 norm of abs error :" << l2norm_dx_abs
                << "  -  L2 norm of rel error :" << l2norm_dx_rel << std::endl;
  } else {
    std::cout << "Unsuported dforce_mode in TetrahedronFEMForceField::addDForce" << std::endl;
    exit(-1);
  }

  std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
  std::chrono::microseconds duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 );
  dforce_total_duration += duration;
  dforce_total_calls++;
}


DECLARE_CLASS(TetrahedronFEMForceField)

