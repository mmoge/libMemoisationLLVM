#ifndef TETRAHEDRONFEMFORCEFIELD_H
#define TETRAHEDRONFEMFORCEFIELD_H

#include <forcefield/ForceField.h>
#include <state/State.h>
#include <topology/Topology.h>
#include <cblas.h>

extern "C" void sgemv_   (   char*   TRANS,
        int*     M,
        int*     N,
        float*    ALPHA,
        float*      A,
        int*     LDA,
        float*      X,
        int*     INCX,
        float*    BETA,
        float*      Y,
        int*     INCY
    )   ;
/*__attribute__((always_inline)) inline */void mysgemv_   (   char*   TRANS,
        int*     M,
        int*     N,
        float*    ALPHA,
        float*      A,
        int*     LDA,
        float*      X,
        int*     INCX,
        float*    BETA,
        float*      Y,
        int*     INCY
    )
{
//    sgemv_(TRANS,M,N,ALPHA,A,LDA,X,INCX,BETA,Y,INCY);
    if ( *BETA != 0. )
    {
        for ( int i = 0; i < (*M); ++i )
        {
            Y[i] *= (*BETA);
        }
    }
    else
    {
        for ( int i = 0; i < (*M); ++i )
        {
            Y[i] = 0;
        }
    }
    if ( *TRANS == 'N' )
    {
//        sgemv_(TRANS,M,N,ALPHA,A,LDA,X,INCX,BETA,Y,INCY);
        for ( int j = 0; j < (*N); ++j )
        {
            for ( int i = 0; i < (*M); ++i )
            {
                Y[i] += (*ALPHA)*A[i+j*(*M)]*X[j];
            }
        }
    }
    else if ( *TRANS == 'T' || *TRANS == 'C' )
    {
//        sgemv_(TRANS,M,N,ALPHA,A,LDA,X,INCX,BETA,Y,INCY);
        for ( int i = 0; i < (*M); ++i )
        {
            for ( int j = 0; j < (*N); ++j )
            {
                Y[i] += (*ALPHA)*A[j+i*(*N)]*X[j];
            }
        }
    }
}

class TetrahedronFEMForceField : public ForceField {
public:

    Data<float> d_youngModulus;
    Data<float> d_poissonRatio;
    Data<std::string> d_method;

    TetrahedronFEMForceField();

    void init();

    void addForce(TVecId f);

    void addDForce(MechanicalParams params,TVecId dx,TVecId df);
    void addDForceNormal(MechanicalParams params,TVecId dx,TVecId df);
    void addDForceTracing(MechanicalParams params,TVecId dx,TVecId df);
    void addDForceApollo(MechanicalParams params,TVecId dx,TVecId df);
    void addDForceOmp(MechanicalParams params,TVecId dx,TVecId df);
    void addDForceOmpCritical(MechanicalParams params,TVecId dx,TVecId df);
    void addDForceUs(MechanicalParams params,TVecId dx,TVecId df);
    void addDForceClustered(MechanicalParams params,TVecId dx,TVecId df);

protected:
    std::vector<TMat3x3> m_iR;
    std::vector<TMat3x3> m_R;
    std::vector<TMat12x12> m_vecKe;

};


#endif
