#include "MeshVTKLoader.h"
#include <iostream>
#include <fstream>

MeshVTKLoader::MeshVTKLoader()
: d_flipTetra("flipTetra",false,this) {}


bool MeshVTKLoader::read_vtk_nodes(State * state,std::ifstream & in) {
    unsigned int nbp = 0;
    in >> nbp;

    std::string type;
    in >> type;

    for (unsigned int i=0;i<nbp;++i)
    {
        TVec3 pos;
        in >> pos[0] >> pos[1] >> pos[2];
        pos[0] *= d_scale3d.getValue()[0];
        pos[1] *= d_scale3d.getValue()[1];
        pos[2] *= d_scale3d.getValue()[2];

        state->addPoint(pos);
    }

    return true;
}

bool MeshVTKLoader::read_vtk_elements(Topology * topo,std::ifstream & in) {
    unsigned int nbe = 0;
    in >> nbe;

    std::string num;
    in >> num;

    std::vector<TTriangle> & triangles= topo->getTriangles();
    std::vector<TTetra> & tetras = topo->getTetras();

    for (unsigned int i=0;i<nbe;++i)
    {
        int type;
        in >> type;

        if (type == 4) {
            int a,b,c,d;
            in >> a >> b >> c >> d;
            TTetra tetra;

            if (d_flipTetra.getValue()) {
                tetra[0] = a;
                tetra[1] = b;
                tetra[2] = d;
                tetra[3] = c;
            } else {
                tetra[0] = a;
                tetra[1] = b;
                tetra[2] = c;
                tetra[3] = d;
            }

            tetras.push_back(tetra);
        } else if (type == 3) {
            int a,b,c;
            in >> a >> b >> c;
            TTriangle tri;

            tri[0] = a;
            tri[1] = b;
            tri[2] = c;

            triangles.push_back(tri);
        }
    }

    return true;
}

bool MeshVTKLoader::read_vtk_textures(State * state,Topology * topo,std::ifstream & in) {
    std::string line;
    getline( in, line );

    std::vector<TTexCoord> & textures= topo->getTextures();

    for (unsigned int i=0;i<state->size();++i) {
        double t1,t2;

        in >> t1 >> t2;
        TTexCoord tex(t1,t2);

        textures.push_back(tex);
    }

    return true;
}


void MeshVTKLoader::load(Topology * topo,State * state, const char * filename) {

    std::ifstream in(filename);

    if (!in)
    {
        std::cerr << "Cannot open file " << filename << std::endl;
        return;
    }

//    std::cout << "Reading file " << filename << std::endl;
    std::string type;

    while (in >> type) {
        if (type == "POINTS") {
            if (! read_vtk_nodes(state,in)) return ;
        } else if (type == "CELLS") {
            if (! read_vtk_elements(topo,in)) return ;
        } else if (type == "POLYGONS") {
            if (! read_vtk_elements(topo,in)) return ;
        } else if (type == "TEXTURE_COORDINATES") {
            if (! read_vtk_textures(state,topo,in)) return ;
        }
    }

    if (d_printLog.getValue())
        std::cout << this->getName() << " loaded " << state->size() << " points and " << topo->getTetras().size() << " tetras and " << topo->getTriangles().size() << " triangles and " << topo->getTextures().size() << " textures" << std::endl;

    in.close();
}


DECLARE_CLASS(MeshVTKLoader)

