#include <solver/CGLinearSolver.h>
#include <stdio.h>
#include <constraint/Constraint.h>
#include <core/Visitor.h>
#include <core/Context.h>
#include <forcefield/ForceField.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

CGLinearSolver::CGLinearSolver()
: d_iteration("iterations",100,this)
, d_tolerance("tolerance",0.00001,this)
, d_threshold("threshold",0.00001,this) {}

void CGLinearSolver::buildMatrix(MechanicalParams param) {
    m_params = param;
}

void CGLinearSolver::mulMatrixVector(TVecId x, TVecId b) {
    VClearVisitor(x).execute(this->getContext());

    AddDForceVisitor(m_params, x,b).execute(this->getContext());

    ApplyConstraintVisitor(x).execute(this->getContext());
}

void CGLinearSolver::solve(TVecId x, TVecId b) {
    TVecId q = VecID::vec_q;
    TVecId d = VecID::vec_d;
    TVecId r = VecID::vec_r;

    m_cg_iter = 0;

    mulMatrixVector(q,x);

    VOpVisitor(r,b,q,-1).execute(this->getContext());

    VEqBFVisitor(d,r,1.0).execute(this->getContext());

    double d_new = VDotVisitor(r,r).compute(this->getContext());
    double d_0 = d_new;

    double tolerance = d_tolerance.getValue()*d_tolerance.getValue()*d_0;

    while (m_cg_iter < d_iteration.getValue() && d_new >= tolerance)
    {
        // q = Ad;
        mulMatrixVector(q, d);

        double den = VDotVisitor( d, q).compute(this->getContext());

        if( fabs(den)<d_threshold.getValue()) return;

        double alpha = d_new / den;

        VPEqBFVisitor(x,d,alpha).execute(this->getContext());

        VPEqBFVisitor(r,q,-alpha).execute(this->getContext());

        double d_old = d_new;
        d_new = VDotVisitor( r, r).compute(this->getContext());

        double beta = d_new / d_old;

        VOpVisitor(d,r,d,beta).execute(this->getContext());

        ++m_cg_iter;
    }
}


void CGLinearSolver::draw(DisplayFlag flag) {
    if (!flag.isActive(DisplayFlag::STATS)) return;

    char buf[256];
    sprintf(buf,"CG iterations: %d ",m_cg_iter);

    glDisable( GL_DEPTH_TEST ) ; // also disable the depth test so renders on top

    int i=0;
    while (buf[i] != 0) {
        glutBitmapCharacter( GLUT_BITMAP_HELVETICA_12, buf[i] );
        i++;
    }

    glEnable( GL_DEPTH_TEST ) ; // Turn depth testing back on
}


DECLARE_CLASS(CGLinearSolver)
