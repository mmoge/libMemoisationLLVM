//===--- SkeletonConfig.h -------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef SKELETON_CONFIG_H
#define SKELETON_CONFIG_H

#include <string>

// Macro used to define descriptors for new skeletons
#define DEFINE_SKELETON(skeleton_name, s_id, _has_binary, _has_jit)            \
  struct skeleton_name {                                                       \
    static const int id = s_id;                                                \
                                                                               \
    static SkeletonProperties getProperties() {                                \
      return SkeletonProperties(_has_binary, _has_jit, "skeleton_" #s_id);     \
    }                                                                          \
  };                                                                           \
  template <> inline std::string sk_name<skeleton_name>() {                    \
    return "skeleton_" #s_id;                                                  \
  }

// Macro used to define descriptors of function copys (they are not skeletons!)
#define DEFINE_COPY(copy_name, copy_suffix)                                    \
  struct copy_name {};                                                         \
  template <> inline std::string sk_name<copy_name>() { return copy_suffix; }

namespace apollo {
namespace skeleton {

struct SkeletonProperties {
  bool HasBinary;
  bool HasJit;
  std::string Name;

  SkeletonProperties(bool HB, bool HJ, std::string N)
      : HasBinary(HB), HasJit(HJ), Name(N) {}
};

const SkeletonProperties defaultProperties(false, false, "invalid");

template <typename Skeleton> 
inline std::string sk_name();

// Represents the original apollo_loop_%id functions.
DEFINE_COPY(apollo_loop, "")

// Skeleton declarations
DEFINE_SKELETON(static_info, 0, true, false)
DEFINE_SKELETON(instrumentation, 1, true, false)
DEFINE_SKELETON(basic_scalar_predict, 2, true, false)
DEFINE_SKELETON(code_bones, 3, false, true)
DEFINE_SKELETON(parallel, 4, true, false)
DEFINE_SKELETON(sequential, 5, true, false)

// Inspectors for the different skeleton properties
template <typename Sk> 
inline SkeletonProperties getPropertiesAux(const unsigned ID) {
  if (ID == Sk::id)
    return Sk::getProperties();
  else
    return defaultProperties;
}

template <typename Sk1, typename Sk2, typename... Sks>
inline SkeletonProperties getPropertiesAux(const unsigned ID) {
  if (ID == Sk1::id)
    return getPropertiesAux<Sk1>(ID);
  else
    return getPropertiesAux<Sk2, Sks...>(ID);
}

inline SkeletonProperties getProperties(const unsigned ID) {
  return getPropertiesAux<static_info, 
                          instrumentation, basic_scalar_predict>(ID);
}

inline bool hasJit(const unsigned ID) { 
  return getProperties(ID).HasJit; 
}

inline bool hasBinary(const unsigned ID) { 
  return getProperties(ID).HasBinary; 
}

} // end namespace skeleton
} // end namespace apollo

#endif
