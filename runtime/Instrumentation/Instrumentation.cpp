//===--- Instrumentation.cpp ----------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Instrumentation.h"
#include "InstrumentationConfig.h"
#include <algorithm>

#include <cstdarg>

/// \brief TAKEN FROM APOLLO - NOT USED !!! This functions register a new instrumentation event.
/// The registered event can be either static or dynamic. Static
/// events are all registered at the beginning of the execution of
/// the instrumentation skeleton loop while dynamic ones are recorded
/// during the loop execution.
///
/// \param Result the object where to record the event
/// \param Type the type of the event to record
/// \param Key the identifier of the event (the memory access, scalara or bound id)
/// \param Val the value of the event (e.g. the adress of memory access), only used for dynamic events
/// \param N the number of varargs
/// \param ... represents coefficients for static events and vi values for dynamic ones
extern "C" void apollo_register_event(InstrumentationResult *Result,
                                      const EventType Type,
                                      const long Key,
                                      const long Val,
                                      const long N, ...) {

  va_list Value;
  va_start(Value, N);
  long Params[N];

  for (int i = 0; i < N; i++) {
    Params[i] = va_arg(Value, long);
  }

  va_end(Value);

  if (!Result) {
	assert(false);
    return;
  }
  auto &InstrumentationResults = *Result;

  switch (Type) {
  case MemDyn:
    InstrumentationResults.registerDynamicMemAccess(Key, (void *)Val, Params, N);
    break;
  case ScaDyn:
    InstrumentationResults.registerDynamicScalar(Key, Val, Params, N);
    break;
  case BoundDyn:
    InstrumentationResults.registerDynamicBound(Key, Val, Params, N);
    break;
  case MemSta:
    InstrumentationResults.registerStaticMemAccess(Key, Params, N);
    break;
  case ScaSta:
    InstrumentationResults.registerStaticScalar(Key, Params, N);
    break;
  case BoundSta:
    InstrumentationResults.registerStaticBound(Key, Params, N);
    break;
  case FuncCall:
    InstrumentationResults.registerFunCall();
    break;
  default:
	assert(false);
    break;
  }
}



std::vector< std::vector< std::set< std::uintptr_t > > >  MemAccessInstrumentation::stores =
    std::vector< std::vector< std::set< std::uintptr_t > > >();
std::vector< std::map< std::uintptr_t, std::set< int > > > MemAccessInstrumentation::addresses_to_iterations =
    std::vector< std::map< std::uintptr_t, std::set< int > > >();

std::map< const int, size_t > apolloLoopCount::loopCount = std::map< const int, size_t >();

std::map< const int, std::vector< std::set< size_t > > > apolloDependencyGraphs::newDepGraphs = std::map< const int, std::vector< std::set< size_t > > >();
std::map< const int, std::vector< int > > apolloDependencyGraphs::newColors = std::map< const int, std::vector< int > >();

std::map< const int, int > apolloDependencyGraphs::nbColors = std::map< const int, int >();
std::map< const int, std::vector< size_t > > apolloDependencyGraphs::colorSizes = std::map< const int, std::vector< size_t > >();
std::map< const int, std::vector< size_t > > apolloDependencyGraphs::colorFirst = std::map< const int, std::vector< size_t > >();
std::map< const int, int* > apolloDependencyGraphs::itsArray = std::map< const int, int* >();



void printRegisteredStore(const int loopId)
{
    std::cout << "---- Loop " << loopId << " ----" << std::endl;
    int itecounter = 0;
    for ( std::vector< std::set< std::uintptr_t > >::const_iterator iterationit = MemAccessInstrumentation::stores[loopId].begin();
            iterationit != MemAccessInstrumentation::stores[loopId].end();
            iterationit++ )
    {
        std::cout << "iteration " << itecounter << " : ";
        for ( std::set< std::uintptr_t >::const_iterator addrit = iterationit->begin();
                addrit != iterationit->end();
                addrit++ )
        {
            std::cout << "" << *addrit << "  |  ";
        }
        std::cout << std::endl;
        itecounter++;
    }
}

int countDependencies(std::set< std::uintptr_t > iteration1, std::set< std::uintptr_t > iteration2)
{
    int count = 0;
    std::set< std::uintptr_t >::const_iterator it1_first = iteration1.begin();
    std::set< std::uintptr_t >::const_iterator it2_first = iteration2.begin();
    std::set< std::uintptr_t >::const_iterator it1_last = --iteration1.end();
    std::set< std::uintptr_t >::const_iterator it2_last = --iteration2.end();

    std::set<uintptr_t> result;
    std::set_intersection(
            it1_first, it1_last, it2_first, it2_last, std::inserter(result,result.begin()) );

    return result.size();
}

extern "C" void apollo_register_store(const long Key,
                                      const long Val,
                                      const long ViVal) {
    registerStore(Key, (void *)Val, ViVal);
}

extern "C" int apollo_loop_version_chooser(const long Key) {
    return loopVersionChooser(Key);
}

extern "C" int apollo_get_nb_colors(const long Key) {
    return getNbColors(Key);
}
extern "C" int* apollo_get_its_array(const long Key) {
    return getItsArray(Key);
}
extern "C" size_t apollo_get_color_size(const long Id, const long color) {
    return getColorSize(Id, color);
}
extern "C" size_t apollo_get_color_first(const long Id, const long color) {
    return getColorFirst(Id, color);
}
extern "C" size_t apollo_get_color_last(const long Id, const long color) {
    return getColorLast(Id, color);
}
extern "C" void apollo_create_and_color_dependency_graph(const long Id) {
    return createAndColorDependencyGraph(Id);
}




