//===--- Instrumentation.h ------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef INSTRUMENTATION_H
#define INSTRUMENTATION_H

#include "StaticInfo.h"

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include <cassert>
#include <cmath>
#include <cstring>
#include <omp.h>
#include <chrono>


/// \brief  for each apollo loop - For each iteration, contains the addresses targeted by the stores
struct MemAccessInstrumentation {
    static std::vector< std::vector< std::set< std::uintptr_t > > > stores;
    static std::vector< std::map< std::uintptr_t, std::set< int > > > addresses_to_iterations;
};
struct apolloLoopCount {
    static std::map< const int, size_t > loopCount;     //for each apollo loop, a counter of #calls
};
struct apolloDependencyGraphs {
    static std::map< const int, std::vector< std::set< size_t > > > newDepGraphs;  //dependency graphs of each apollo loop
    //colored :
    static std::map< const int, std::vector< int > > newColors;  //color of each iteration for each apollo loop
    static std::map< const int, int > nbColors;     //nb of colors in the (colored) graphs corresponding to each apollo loop
    static std::map< const int, std::vector< size_t > > colorSizes;   //nb of iterations of each color in graph corresponding to each apollo loop
    static std::map< const int, std::vector< size_t > > colorFirst;   //the index of the first iteration of color Col in arrays its for each apollo loop
    static std::map< const int, int* > itsArray;   //all iterations index reordered according to their color for each apollo loop
};

/// \brief prints MemAccessInstrumentation::stores[Id] to std out
void printRegisteredStore(const int Id);

/// \brief returns the number of common addresses in two std::set< std::uintptr_t >
///        representing the addresses accessed by two iterations of an apollo loop
int countDependencies(std::set< std::uintptr_t > iteration1, std::set< std::uintptr_t > iteration2);


/// Id is loop id
void registerStore(const int Id, void *Addr, const int ViVal) {
//    std::cout << "registerStore " << "Id " << Id  << "  ViVal = " << ViVal << "   Addr = " << Addr << std::endl;
    while ( MemAccessInstrumentation::stores.size() < Id+1 )
    {
        // create a vector<set<uintptr_t>> to register the stores in loop Id
        MemAccessInstrumentation::stores.push_back(std::vector< std::set< std::uintptr_t > >(0));
        MemAccessInstrumentation::addresses_to_iterations.push_back(std::map< std::uintptr_t, std::set< int > >());
    }
    if ( MemAccessInstrumentation::stores[Id].size() < ViVal+1 )
    {
        //create a set corresponding to iteration ViVal
        MemAccessInstrumentation::stores[Id].push_back(std::set< std::uintptr_t >());
        //insert store at address Addr
        MemAccessInstrumentation::stores[Id].back().insert(reinterpret_cast< std::uintptr_t >( Addr ));
    }
    else
    {
        //insert store at address Addr
        MemAccessInstrumentation::stores[Id].back().
                insert(reinterpret_cast< std::uintptr_t >( Addr ) );
    }
    MemAccessInstrumentation::addresses_to_iterations[Id][reinterpret_cast< std::uintptr_t >( Addr )].insert(ViVal);
}


/// \brief increments the call counter corresponding to loop Id, and returns
///        4 if current execution of the loop is to be done with parallel version
///        1 if current execution of the loop is to be done with instrumentation version
///        5 if current execution of the loop is to be done with original sequential version
int loopVersionChooser(const int Id)
{
    if ( apolloLoopCount::loopCount.find(Id) == apolloLoopCount::loopCount.end() )
    {
        apolloLoopCount::loopCount[Id] = 1;
        return 1;    //instrumentation
    }
    else
    {
        apolloLoopCount::loopCount[Id]++;
        return 4;    //parallel exec
    }
    //TODO: When do we return 5 for sequential exec ?
}

/// \brief returns the number of colors in graph corresponding to apollo loop Id
int getNbColors(const int Id)
{
    if ( apolloDependencyGraphs::nbColors.find(Id) == apolloDependencyGraphs::nbColors.end() )
    {
        std::cerr << "ERROR: the graph of apollo_loop_" << Id << " has not been colored !!!" << std::endl;
        return 1;
    }
    else
    {
        return apolloDependencyGraphs::nbColors[Id];
    }
}
/// \brief returns the its array corresponding to the colored graph corresponding to apollo loop Id
int* getItsArray(const int Id)
{
    if ( apolloDependencyGraphs::itsArray.find(Id) == apolloDependencyGraphs::itsArray.end() )
    {
        std::cerr << "ERROR: the graph of apollo_loop_" << Id << " has not been colored !!!" << std::endl;
        return nullptr;
    }
    else
    {
        return apolloDependencyGraphs::itsArray[Id];
    }
}
/// \brief returns the number of iterations of color Col in graph corresponding to apollo loop Id
size_t getColorSize(const int Id, const int Col)
{
    if ( apolloDependencyGraphs::colorSizes.find(Id) == apolloDependencyGraphs::colorSizes.end() )
    {
        std::cerr << "ERROR: the graph of apollo_loop_" << Id << " has not been colored or color " << Col << " does not exist !!!" << std::endl;
        return 0;
    }
    else
    {
//        std::cout << "    getColorSize : " << apolloDependencyGraphs::colorSizes[Id][Col] << std::endl;
        return apolloDependencyGraphs::colorSizes[Id][Col];
    }
}
/// \brief returns the index of the first iteration of color Col in arrays its
size_t getColorFirst(const int Id, const int Col)
{
    if ( apolloDependencyGraphs::colorFirst.find(Id) == apolloDependencyGraphs::colorFirst.end() )
    {
        std::cerr << "ERROR: the graph of apollo_loop_" << Id << " has not been colored or color " << Col << " does not exist !!!" << std::endl;
        return 0;
    }
    else
    {
        return apolloDependencyGraphs::colorFirst[Id][Col];
    }
}

/// \brief returns the index of the last iteration of color Col in arrays its
size_t getColorLast(const int Id, const int Col)
{
    if ( apolloDependencyGraphs::colorFirst.find(Id) == apolloDependencyGraphs::colorFirst.end() )
    {
        std::cerr << "ERROR: the graph of apollo_loop_" << Id << " has not been colored or color " << Col << " does not exist !!!" << std::endl;
        return 0;
    }
    else
    {
        return apolloDependencyGraphs::colorFirst[Id][Col]+apolloDependencyGraphs::colorSizes[Id][Col]-1;
    }
}



/// \brief creates and color dependency graph using MemAccessInstrumentation::stores
/// \param Id the id of the loop whose graph we build
void createAndColorDependencyGraph(const int Id)
{
    std::cout << "createAndColorDependencyGraph" << std::endl;
    std::chrono::steady_clock::time_point start_graph_creation(std::chrono::steady_clock::now());

    std::chrono::duration<double> countDependencies_time;

    size_t nbVertices = MemAccessInstrumentation::stores[Id].size();
    std::cout << "    nbVertices = " << nbVertices << std::endl;
    //
    // 1 - Build graph
    //
    std::chrono::steady_clock::time_point start_graph_coloring(std::chrono::steady_clock::now());
    //
    apolloDependencyGraphs::newDepGraphs[Id].resize(nbVertices);
    apolloDependencyGraphs::newColors[Id].resize(nbVertices,-1);
    std::vector< std::vector< int > > colToIndex;
    int maxColor = -1;
    // loop on all iterations of loop Id
    for ( size_t vertex_it = 0;
            vertex_it < nbVertices;
            ++vertex_it )
    {
        std::set<int> neigboring_colors;
        //loop on the addresses accessed by iteration it1_idx
        for( std::set< std::uintptr_t >::const_iterator it_addr = MemAccessInstrumentation::stores[Id][vertex_it].begin();
                it_addr != MemAccessInstrumentation::stores[Id][vertex_it].end();
                it_addr++ )
        {
            //loop on neighboring vertices of the vertex_it-th vertex
            // - i.e. we loop over all iterations that access the addresses accessed by iteration *it_addr -
            //to create the edges of the graph
            //and to get their colors
            std::set<int> toto = (MemAccessInstrumentation::addresses_to_iterations[Id])[*it_addr];
            for ( std::set< int >::const_iterator it_neigh = (MemAccessInstrumentation::addresses_to_iterations[Id][*it_addr]).begin();
                    it_neigh != MemAccessInstrumentation::addresses_to_iterations[Id][*it_addr].end();
                    it_neigh++ )
            {
                //add the new edge to the graph (if it does not exist already)
                apolloDependencyGraphs::newDepGraphs[Id][vertex_it].insert(*it_neigh);
                // REMARK: if we want to consider the weight of the edges this is the right place !
                //get the color of all neigbors
                neigboring_colors.insert( apolloDependencyGraphs::newColors[Id][*it_neigh] );
            }
        }
        int attributed_color = 0;
        std::set<int>::iterator neigboring_colors_it = neigboring_colors.begin();
        while( neigboring_colors_it != neigboring_colors.end() )
        {
            neigboring_colors_it = neigboring_colors.find(attributed_color);
            if( neigboring_colors_it != neigboring_colors.end() )
            {
                attributed_color++;
            }
        }
        if ( attributed_color > maxColor )
        {
            maxColor = attributed_color;
            colToIndex.push_back( std::vector< int >() );
        }
        apolloDependencyGraphs::newColors[Id][vertex_it] = attributed_color;
        colToIndex[attributed_color].push_back( vertex_it );
    }
    //
    //
    std::chrono::steady_clock::time_point end_graph_coloring(std::chrono::steady_clock::now());
    std::cout << "    graph colored in "
            << std::chrono::duration_cast<std::chrono::duration<double>>(
                    end_graph_coloring - start_graph_coloring).count()
            << " seconds" << std::endl;
    //
    // fill itsArray, nbColors, colorSizes, colorFirst
    //
    apolloDependencyGraphs::nbColors[Id] = maxColor+1;
    apolloDependencyGraphs::colorSizes[Id] = std::vector< size_t >(maxColor+1,0);
    apolloDependencyGraphs::colorFirst[Id] = std::vector< size_t >(maxColor+2,0);
    apolloDependencyGraphs::itsArray[Id] = new int[nbVertices];
    for ( int col = 0; col < maxColor+1; ++col )
    {
        apolloDependencyGraphs::colorSizes[Id][col] = colToIndex[col].size();
        apolloDependencyGraphs::colorFirst[Id][col+1] =
                apolloDependencyGraphs::colorFirst[Id][col] + apolloDependencyGraphs::colorSizes[Id][col];
        memcpy( &(apolloDependencyGraphs::itsArray[Id][apolloDependencyGraphs::colorFirst[Id][col]]),
                colToIndex[col].data(),
                apolloDependencyGraphs::colorSizes[Id][col] * sizeof(int) );
    }
    int colcounter = 0;
    for ( int col = 0; col < apolloDependencyGraphs::colorSizes[Id].size(); ++ col )
    {
        std::cout << "col " << col << " has " << apolloDependencyGraphs::colorSizes[Id][col] << " iterations." << std::endl;
    }
    std::cout << "createAndColorDependencyGraph   DONE" << std::endl;
}




/// \brief This class contains the collected values from dynamic
/// instrumentation and information collected statically.
///
/// Statically collected information comes from:
///
/// - apollo_register_static_info functions calls inserted by static passes
///   in the skeleton_0 (static_info) function called by runtime when
///   instrumentation chunks starts
///
/// - apollo_register_event function calls (with static parameters)
///   inserted by static passes in the skeleton_1 (instrumentation)
///   function. Before starting to loop with instrumentation on,
///   the skeleton_1 function register static events computed statically.
///
/// Dynamically collected information comes from:
///
/// - apollo_register_event function calls (with dynamic parameters)
///   inserted by static passes in the skeleton_1 (instrumentation)
///   function. Compared to static events, these calls are made all
///   along the run of the loop with instrumentation. The frequency
///   of these calls compare to the real events (memory accesses, ...)
///   depends on the sampling frequency parameters.
///
class InstrumentationResult {
public:
  struct InstrumentationEvent {
    const static size_t MaxLoopDepth = 6;
    enum class InstrumentationEventType : char {
      Memory,
      Scalar,
      Bound,
      FunctionCall
    };
    enum class InstrumentationType : char { StaticEvent, DynamicEvent };

    unsigned short Id;
    InstrumentationEventType EventType;
    InstrumentationType InstrType;

    // for dynamic events it stores the vi-values
    // for static the coefficients
    long Coefs[MaxLoopDepth];

    // for dynamic events it stores the effective value of the access, scalar, bound
    // for static it is always 0
    long Value;

    std::string typeToString() const {
      if (InstrType == InstrumentationType::StaticEvent)
        return "static";
      if (InstrType == InstrumentationType::DynamicEvent)
        return "dynamic";
      assert(false);
      return "";
    }

    std::string eventTypeToString() const {
      if (EventType == InstrumentationEventType::Memory)
        return "memory";
      if (EventType == InstrumentationEventType::Scalar)
        return "scalar";
      if (EventType == InstrumentationEventType::Bound)
        return "bound";
      if (EventType == InstrumentationEventType::FunctionCall)
        return "function_call";
      assert(false);
      return "";
    }

    void dump(std::ostream &OS) const {
      OS << "{ id=" << Id << ", value=" << Value
        << ", type=" << typeToString() << "-"
         << eventTypeToString() << ", < ";

      for (size_t v = 0;
           v < InstrumentationEvent::MaxLoopDepth && Coefs[v] != -1; ++v) {
        OS << Coefs[v] << " ";
      }
      OS << "> }";
    }
  };

private:
  InstrumentationResult(){}; // no default constructor
  //std::vector<const InstrumentationEvent *> Events;
  std::vector<std::vector<InstrumentationEvent>> Mems;
  std::vector<std::vector<InstrumentationEvent>> Scalars;
  std::vector<std::vector<InstrumentationEvent>> Bounds;
  std::vector<InstrumentationEvent> FunCalls;

public:
  InstrumentationResult(StaticNestInfo *Nest, int InstChunkSize) {
    std::vector<InstrumentationEvent> EmptyVector;
    Mems = std::vector<std::vector<InstrumentationEvent>>(Nest->Mem.size(),
                                                          EmptyVector);
    Scalars = std::vector<std::vector<InstrumentationEvent>>(
        Nest->Scalars.size(), EmptyVector);
    Bounds = std::vector<std::vector<InstrumentationEvent>>(Nest->Loops.size(),
                                                            EmptyVector);
  }

  void dump(std::ostream &OS) const {
    OS << "IntrumentationResult : dump() [\n";
//    for (size_t i = 0; i < Events.size(); ++i) {
//      auto Ev = Events[i];
//      OS << "\t" << i << " : ";
//      Ev->dump(OS);
//      OS << "\n";
//    }
    int j = 0;
    for (size_t i = 0; i < Mems.size(); ++i) {
    	for (InstrumentationEvent Ev : Mems[i]) {
			OS << "\t" << j++ << " : ";
			Ev.dump(OS);
			OS << "\n";
    	}
      }
    for (size_t i = 0; i < Scalars.size(); ++i) {
    	for (InstrumentationEvent Ev : Scalars[i]) {
			OS << "\t" << j++ << " : ";
			Ev.dump(OS);
			OS << "\n";
    	}
      }
    for (size_t i = 0; i < Bounds.size(); ++i) {
    	for (InstrumentationEvent Ev : Bounds[i]) {
			OS << "\t" << j++ << " : ";
			Ev.dump(OS);
			OS << "\n";
    	}
      }
    for (size_t i = 0; i < FunCalls.size(); ++i) {
        auto Ev = FunCalls[i];
        OS << "\t" << j++ << " : ";
        Ev.dump(OS);
        OS << "\n";
      }


    OS << "]";
  }

  const InstrumentationEvent
  createEvent(const int Id, 
                InstrumentationEvent::InstrumentationType InstrType,
                InstrumentationEvent::InstrumentationEventType EventType,
                const long Value, long *Vis, const int nVis) {
	if (EventType != InstrumentationEvent::InstrumentationEventType::FunctionCall) {
		assert(Vis);
	}
    assert(0 <= nVis);
    assert((size_t)nVis <= InstrumentationEvent::MaxLoopDepth);

    InstrumentationEvent Event;
    Event.Id = Id;
    Event.InstrType = InstrType;
    Event.EventType = EventType;
    Event.Value = Value;
    std::copy(Vis, Vis + nVis, Event.Coefs);
    std::fill(Event.Coefs + nVis,
              Event.Coefs + InstrumentationEvent::MaxLoopDepth, -1);

    return Event;
  }

  void registerFunCall() {
    FunCalls.emplace_back(this->createEvent(
        0, InstrumentationEvent::InstrumentationType::DynamicEvent,
        InstrumentationEvent::InstrumentationEventType::FunctionCall, 0,
        nullptr, 0));
  }

  void registerDynamicMemAccess(const int Id, void *Addr, long *Vis, const int nVis) {
    Mems[Id].emplace_back(this->createEvent(
        Id, InstrumentationEvent::InstrumentationType::DynamicEvent,
        InstrumentationEvent::InstrumentationEventType::Memory, (long)Addr, Vis,
        nVis));
  }

  void registerDynamicScalar(const int Id, const long Value, 
                      long *Vis, const int nVis) {
    Scalars[Id].emplace_back(this->createEvent(
        Id, InstrumentationEvent::InstrumentationType::DynamicEvent,
        InstrumentationEvent::InstrumentationEventType::Scalar, Value, Vis,
        nVis));
  }

  void registerDynamicBound(const int Id, const long Iters, 
                     long *Vis, const int nVis) {
    Bounds[Id].emplace_back(this->createEvent(
        Id, InstrumentationEvent::InstrumentationType::DynamicEvent,
        InstrumentationEvent::InstrumentationEventType::Bound, Iters, Vis,
        nVis));
  }

  void registerStaticMemAccess(const int Id, long *Coefs, const int nCoef) {
    Mems[Id].emplace_back(this->createEvent(
        Id, InstrumentationEvent::InstrumentationType::StaticEvent,
        InstrumentationEvent::InstrumentationEventType::Memory, 0, Coefs,
        nCoef));
  }

  void registerStaticScalar(const int Id, long *Coefs, const int nCoef) {
    Scalars[Id].emplace_back(this->createEvent(
        Id, InstrumentationEvent::InstrumentationType::StaticEvent,
        InstrumentationEvent::InstrumentationEventType::Scalar, 0, Coefs,
        nCoef));
  }

  void registerStaticBound(const int Id, long *Coefs, const int nCoef) {
    Bounds[Id].emplace_back(this->createEvent(
        Id, InstrumentationEvent::InstrumentationType::StaticEvent,
        InstrumentationEvent::InstrumentationEventType::Bound, 0, Coefs,
        nCoef));
  }

//  const std::vector<const InstrumentationEvent *> &getEvents() const {
//    return Events;
//  }

  const long getEventsCount() const {
	  	long count = 0;
		for (size_t i = 0; i < Mems.size(); ++i) {
			count += Mems[i].size();
		}
		for (size_t i = 0; i < Scalars.size(); ++i) {
			count += Scalars[i].size();
		}
		for (size_t i = 0; i < Bounds.size(); ++i) {
			count += Bounds[i].size();
		}
		count += FunCalls.size();
		return count;
	}

  const long getMemEventsCount() const {
	  	long count = 0;
		for (size_t i = 0; i < Mems.size(); ++i) {
			count += Mems[i].size();
		}
		return count;
	}

  const std::vector<InstrumentationEvent> &getMemEvents(const int Id) const {
    return Mems[Id];
  }

  const std::vector<InstrumentationEvent> &getScalarEvents(const int Id) const {
    return Scalars[Id];
  }

  const std::vector<InstrumentationEvent> &getBoundEvents(const int Id) const {
    return Bounds[Id];
  }

  const std::vector<InstrumentationEvent> &getFunCallsEvents() const {
    return FunCalls;
  }
};


#endif

