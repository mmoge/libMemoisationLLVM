//===--- Instrumentation.cpp ----------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "InstrumentationConfig.h"

#include <cstdarg>
#include "Parallel.h"
#include <omp.h>

/// \brief runs function loopFunc in a parallel loop
/// \param
/// \param
/// \param
extern "C" void apollo_run_in_parallel( void *Params, int colorfirst,int colorlast, int* itsarray,
        LoopFuncTy loopFunc ) {
    //we can split the call to loopFunc in multiple calls that can be run in parallel
    int colorsize = colorlast-colorfirst+1;
    int nb_slices = 8; //this is set arbitrarily - TODO: Set it via command line, or env variable or something else
    int slice_size = ceil(float(colorsize)/float(nb_slices));

    // JE POURRAIS JUSTE INSERER LA BOUCLE SUR LES ELTS DE LA COULEUR C ICI AU LIEU DE LE FAIRE EN LLVM
#pragma omp parallel for
    for ( int i = 1; i <= nb_slices; i++ )
    {
        int itmp1 = colorfirst+(i-1)*slice_size;
        int itmp2 = (itmp1+slice_size-1 <= colorlast) ? itmp1+slice_size-1 : colorlast;
        loopFunc( Params,
                itmp1, itmp2, itsarray
                );
    }
}




