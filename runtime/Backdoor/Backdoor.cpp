//===--- Backdoor.cpp -----------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Backdoor.h"

#include <iostream>
#include <fstream>
#include <iomanip>
#include <set>
#include <mutex>
#include <sys/time.h>
#include <boost/asio.hpp>

// main logging function
static void log(const std::string &Tag, const std::string &Str);
void eventStart(const std::string &Str);
void eventEnd(const std::string &Str);

struct BackdoorConfig {
  std::ostream *DebugOutput;
  std::istream *DebugInput;
  bool IOStream;
  std::mutex Mtx;
  std::set<std::string> IgnoreTags;

  BackdoorConfig() {
    DebugInput = nullptr;
    DebugOutput = nullptr;
    IOStream = false;

    IgnoreTags = {"trace", "info", "info_all", "debug", "time", "step"};
    const bool Trace = backdoorutils::getEnvBool("APOLLO_BACKDOOR", "--trace");
    const bool Info = backdoorutils::getEnvBool("APOLLO_BACKDOOR", "--info");
    const bool InfoAll = backdoorutils::getEnvBool("APOLLO_BACKDOOR", "--info_all");
    const bool Debug = backdoorutils::getEnvBool("APOLLO_BACKDOOR", "--debug");
    const bool Time = backdoorutils::getEnvBool("APOLLO_BACKDOOR", "--time");
    const bool Step = backdoorutils::getEnvBool("APOLLO_BACKDOOR", "--step");

    if (Trace)
      IgnoreTags.erase("trace");
    if (Info)
      IgnoreTags.erase("info");
    if (InfoAll)
      IgnoreTags.erase("info_all");
    if (Debug)
      IgnoreTags.erase("debug");
    if (Time)
      IgnoreTags.erase("time");
    if (Step)
      IgnoreTags.erase("step");

    const bool STDio = backdoorutils::getEnvBool("APOLLO_BACKDOOR", "--stdio");
    if (STDio) {
      DebugInput = &std::cin;
      DebugOutput = &std::cerr;
    }

    const std::string FileName =
                      backdoorutils::getEnv("APOLLO_BACKDOOR", "--file");
    if (!FileName.empty()) {
      DebugOutput = new std::ofstream(FileName);
    }

    const bool TCP = backdoorutils::getEnvBool("APOLLO_BACKDOOR", "--tcp");
    if (TCP) {
      boost::asio::ip::tcp::iostream *SocketStream =
          new boost::asio::ip::tcp::iostream("localhost", "22222");
      if (*SocketStream) {
        IOStream = true;
        DebugOutput = SocketStream;
        DebugInput = SocketStream;
      } else {
        std::cerr << "Unable to connect: " << SocketStream->error().message()
                  << "\n";
        delete SocketStream;
      }
    }

    const bool Wait = backdoorutils::getEnvBool("APOLLO_BACKDOOR", "--wait");
    if (!Wait)
      DebugInput = nullptr;
    log("log", "begin");
    eventStart("total");
  }

  ~BackdoorConfig() {
    eventEnd("total");
    log("log", "end");
    if (DebugInput && !IOStream && DebugInput != &std::cin) {
      if ((long)DebugInput != (long)DebugOutput)
        delete DebugInput;
    }
    if (DebugOutput && DebugOutput != &std::cerr) {
      DebugOutput->flush();
      delete DebugOutput;
    }
  }
};

static BackdoorConfig Config;

bool backdoorEnabled(const std::string &Tag) {
  if (!Config.DebugOutput)
    return false;
  if (Tag.size() && Config.IgnoreTags.count(Tag))
    return false;
  return true;
}

static void log(const std::string &Tag, const std::string &Str) {
  if (!backdoorEnabled(Tag))
    return;
  while (!Config.Mtx.try_lock());
  std::ostream &OS = *Config.DebugOutput;
  const bool SingleLine = Str.find("\n") == std::string::npos;
  if (SingleLine)
    OS << Tag << " { " << Str << " }\n";
  else
    OS << Tag << " {\n" << Str << "\n}\n";
  Config.Mtx.unlock();
}

extern "C" __attribute__((weak)) void start_timer(void *, const char *) {}
extern "C" __attribute__((weak)) void pause_timer(void *, const char *) {}

void eventStart(const std::string &Str) {
  if (!backdoorEnabled("time"))
    return;
  start_timer(nullptr, Str.c_str());
  struct timeval Now;
  gettimeofday(&Now, nullptr);
  const double TimeStamp = (double)Now.tv_sec + (double)Now.tv_usec / 1000000.0;
  std::stringstream Buf;
  Buf << "[start] " << Str << " at " << std::setprecision(6) << std::fixed
      << TimeStamp;
  log("time", Buf.str());
}

void eventEnd(const std::string &Str) {
  if (!backdoorEnabled("time"))
    return;
  pause_timer(nullptr, Str.c_str());
  struct timeval now;
  gettimeofday(&now, nullptr);
  const double TimeStamp = (double)now.tv_sec + (double)now.tv_usec / 1000000.0;
  std::stringstream Buf;
  Buf << "[end] " << Str << " at " << std::setprecision(6) << std::fixed
      << TimeStamp;
  log("time", Buf.str());
}

void trace(const std::string &Str) {
  log("trace", Str);
}

void trace(const std::string &Str, const long Lower, const long Upper) {
  if (!backdoorEnabled("step"))
    return;
  std::string UperString = std::to_string(Upper);
  if (Upper == -1)
    UperString = " ";
  std::string NewStr =
      Str + ": " + "[" + std::to_string(Lower) + ", " + UperString + "]";
  if (Upper == -2)
    NewStr = Str + std::to_string(Lower);
  log("step", NewStr);
}

void Info(const std::string &Str) {
  log("info", Str);
}

void Debug(const std::string &Str) {
  log("debug", Str);
}

void waitForUser() {
  if (!Config.DebugInput)
    return;
  while (!Config.Mtx.try_lock());
  std::istream &Is = *Config.DebugInput;
  while (Is.get() != '\n'){}
    Config.Mtx.unlock();
}

namespace backdoorutils {

std::string getEnv(const char *Varname, const char *SearchFor) {
  if (!Varname)
    return "";
  const char *Opt = getenv(Varname);
  if (!Opt)
    return "";
  const std::string OptionsString = Opt;
  if (!SearchFor)
    return Opt;

  std::string SearchForStr = SearchFor;
  SearchForStr.append("=");

  const size_t Position = OptionsString.find(SearchForStr);

  if (Position == std::string::npos)
    return "";
  const size_t ValueStart = Position + SearchForStr.size();
  const size_t ValueEnd = OptionsString.find(' ', ValueStart);

  if (ValueEnd == std::string::npos)
    return OptionsString.substr(ValueStart);
  return OptionsString.substr(ValueStart, ValueEnd - ValueStart);
}

float getEnvFloat(const char *Varname, const char *SearchFor, float Default) {
  const std::string Num = getEnv(Varname, SearchFor);
  if (Num.empty())
    return Default;
  return atof(Num.c_str());
}

int getEnvInt(const char *Varname, const char *SearchFor) {
  const std::string Num = getEnv(Varname, SearchFor);
  if (Num.empty())
    return 0;
  return atoi(Num.c_str());
}

bool getEnvBool(const char *Varname, const char *SearchFor, bool strict) {
  if (!Varname || !SearchFor)
    return false;
  const char *Opt = getenv(Varname);
  if (!Opt)
    return false;
  const std::string OptionsString = Opt;
  const std::string SearchForStr = SearchFor;

  if (!strict) {
	  const auto Position = OptionsString.find(SearchForStr);
	  return Position != std::string::npos;
  } else {
	  return OptionsString.compare(SearchForStr) == 0;
  }
}

} // end namesapace backdoorutils
