//===--- Backdoor.h -------------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef BACKDOOR_H

#include <string>
#include <sstream>

bool backdoorEnabled(const std::string &Tag = "");
void eventStart(const std::string &Str);
void eventEnd(const std::string &Str);
void Info(const std::string &Str);
void Debug(const std::string &Str);
void waitForUser();

namespace backdoorutils {

std::string getEnv(const char *Varname, const char *SearchFor);
int getEnvInt(const char *Varname, const char *SearchFor);
float getEnvFloat(const char *Varname, const char *SearchFor, float Default);
bool getEnvBool(const char *Varname, const char *SearchFor, bool strict=false);

template <typename T> 
std::string toString(const T Ty) {
  if (!backdoorEnabled())
    return "";
  std::stringstream Out;
  Out << Ty;
  return Out.str();
}

template <typename T> 
std::string concat(const T A) {
  if (!backdoorEnabled())
    return "";
  return toString<T>(A);
}

template <typename T1, typename T2, typename... Args>
std::string concat(T1 A, T2 B, Args... As) {
  if (!backdoorEnabled())
    return "";
  return concat<T1>(A) + concat<T2, Args...>(B, As...);
}

} // end namespace backdoorutils

void trace(const std::string &Str);
void trace(const std::string &Str, const long Lower, const long Upper);

inline void traceEnter(const std::string &Str) {
  std::string Buf = "[enter] ";
  Buf.append(Str);
  trace(Buf);
}

inline void traceExit(const std::string &Str) {
  std::string Buf = "[exit]  ";
  Buf.append(Str);
  trace(Buf);
}

inline void traceStep(const std::string &Str, long Lower, long Upper = -1) {
  trace(Str, Lower, Upper);
}

#endif
