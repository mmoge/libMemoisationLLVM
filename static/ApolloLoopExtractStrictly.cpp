//===--- ApolloLoopExtractStrictly.cpp --------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "ApolloLoopExtractStrictly.h"
#include "Utils/FunctionUtils.h"
#include "Utils/MetadataHandler.h"
#include "Utils/LoopUtils.h"
#include "Utils/Utils.h"

#include "llvm/Pass.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Dominators.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Transforms/Utils/CodeExtractor.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/IRBuilder.h"

#include <set>

using namespace llvm;
using namespace apollo;

namespace {

/// \brief Erase the apollo_scop metadata from the instruction.
/// \return true if it removed metadata, false if not.
bool removePragmaInstr(Instruction *I) {
    const bool HasMetadataApollo = metadata::hasMetadataKind(I, mdkind::apollo);
    if (HasMetadataApollo)
      I->setMetadata(mdkind::apollo, 0x0);
    return HasMetadataApollo;
}

/// \brief instructionHasPragmaDcop returns true if the instruction has a
///        pragma.
/// \param I
/// \return true if the Instruction has a pragma
bool instructionHasPragmaDcop(Instruction &I) {
  if (metadata::hasMetadataKind(&I, mdkind::apollo)) {
    const unsigned NumMdOperands =
        metadata::getMetadataNumOperands(&I, mdkind::apollo);
    for (unsigned i = 0; i < NumMdOperands; ++i) {
      MDNode *MdNode = I.getMetadata(mdkind::apollo);
      Metadata *MD = MdNode->getOperand(i);
      MDString *MdStr = dyn_cast<MDString>(MD);
      if (MdStr && MdStr->getString() == "dcop")
        return true;
    }
  }
  return false;
}

/// \brief Identifies if the loop is tagged as an apollo loop.
/// \param L a loop.
/// \param LI
/// \return true if the loop is tagged as an apollo loop.
bool loopHasPragma(Loop *L, LoopInfo *LI) {
  for (BasicBlock *block : L->getBlocks()) {
    if (LI->getLoopFor(block) == L) {
      const bool HasPragma = std::find_if(block->begin(), block->end(),
                                      instructionHasPragmaDcop) != block->end();
      if (!HasPragma)
        return false;
    }
  }
  return true;
}

/// \brief Identifies if the loop is tagged as an apollo loop.
/// \param L a loop.
/// \return true if the loop is tagged as an apollo loop.
bool loopHasAnyPragma(Loop *L) {
  for (BasicBlock *block : L->getBlocks()) {
    const bool HasPragma = std::find_if(block->begin(), block->end(),
                                      instructionHasPragmaDcop) != block->end();
    if (HasPragma)
      return true;
  }
  return false;
}

/// \brief Identifies if the block is part of an apollo loop.
/// \param BB a BasicBlock belonging to a loop.
/// \return true if the block has an instruction with a apollo pragme.
bool blockHasAnyPragma(BasicBlock *block) {
    const bool HasPragma = std::find_if(block->begin(), block->end(),
                                      instructionHasPragmaDcop) != block->end();
    return HasPragma;
}

/// \brief Extracts a loop into a new apollo_loop function.
/// \param L a Loop.
/// \param DT the dominator tree of the parent function of L.
/// \return A function containing the loop.
Function *extractApolloLoop(Loop *L, DominatorTree &DT) {
  static unsigned apolloLoopId = 0;
  CodeExtractor Extractor(DT, *L);
  Function *Extracted = Extractor.extractCodeRegion();
  // apollo loops are internal and not inlinable.
  Extracted->setName(utils::concat("apollo_loop_", apolloLoopId));
  Extracted->removeFnAttr(Attribute::AlwaysInline);
  Extracted->addFnAttr(Attribute::NoInline);
  Extracted->setLinkage(Function::PrivateLinkage);
  apolloLoopId++;
  return Extracted;
}

/// \brief For a function, obtain the outermost loop of a nest marked
///        with the pragma metadata.
/// \param F a function.
/// \param LI Loop info.
/// \return NULL if no loop found. If found return the outermost loop marked
///         with metadata.
Loop *getTopApolloLoop(Function *F, LoopInfo *LI) {
  Loop *AnyApolloLoop = 0x0;
  // First try to find a loop that has apollo pragma
  for (BasicBlock &aBlock : *F) {
      Loop *L = LI->getLoopFor(&aBlock);
      if ( L )
      {
          BasicBlock* header = L->getHeader();
          if( blockHasAnyPragma( header ) )
          {
              return L;

          }
      }
  }
  // if not found, return 0x0
  return 0x0;
}

/// \brief For a function, erase the metadata from instructions outside any loop
/// \param F a function.
/// \param LI LoopInfo from the function.
void cleanMetadataOutsideApolloLoop(Function *F, LoopInfo *LI) {
  for (BasicBlock &block : *F) {
    Loop *L = LI->getLoopFor(&block);
    const bool IsInsideApolloLoop = L && loopHasPragma(L, LI);
    if (!IsInsideApolloLoop) {
      for (Instruction &i : block) {
        removePragmaInstr(&i);
      }
    }
  }
}

/// \brief For a loop, erase the metadata from instructions inside the loop.
/// \param L a loop.
void cleanMetadataLoop(Loop *L) {
  for (BasicBlock *block : L->getBlocks()) {
    for (Instruction &i : *block) {
      removePragmaInstr(&i);
    }
  }
}

/// \brief For a function, erase the metadata from instructions inside it.
/// \param F a Function.
void cleanMetadataFunction(Function *F) {
  for (BasicBlock &block : *F) {
    for (Instruction &i : block) {
      removePragmaInstr(&i);
    }
  }
}


/// \brief Fix the function entry block. Split the entry block if it belongs
///        to the loop.
/// \param L Current Loop
void fixEntryBlock(Loop *L) {
  Function *LoopParentFunction = loop::getLoopParentFunction(L);
  BasicBlock &EntryBlock = LoopParentFunction->getEntryBlock();
  if (L->contains(&EntryBlock)) {
    BasicBlock *NewEntry =
        BasicBlock::Create(LoopParentFunction->getContext(), "new_entry",
                           LoopParentFunction, &EntryBlock);
    BranchInst::Create(&EntryBlock, NewEntry);
  }
}

/// \brief Fix the loop exit blocks. Split them if they are return statements.
/// \param L Current Loop
void fixReturnBlocks(Loop *L) {
  // If any of the exit blocks is a return block, split it.
  std::set<BasicBlock *> ExitBlocks = std::move(loop::getLoopExitBlocks(L));
  for (BasicBlock *exitBlock : ExitBlocks) {
    TerminatorInst *exitBlockTerminator = exitBlock->getTerminator();
    if (isa<ReturnInst>(exitBlockTerminator))
      exitBlock->splitBasicBlock(exitBlockTerminator,
                                 exitBlock->getName() + ".return");
  }
}

/// \brief Check if the entry terminator is a branch to the header.
/// \param L Current Loop
bool entryBranchToLoopHeader(Loop *L) {
  Function *LoopParentFunction = loop::getLoopParentFunction(L);
  BasicBlock &EntryBlock = LoopParentFunction->getEntryBlock();
  TerminatorInst *EntryTerminator = EntryBlock.getTerminator();
  return isa<BranchInst>(EntryTerminator) &&
         cast<BranchInst>(EntryTerminator)->isUnconditional() &&
         EntryTerminator->getSuccessor(0) == L->getHeader();
}

/// \brief Fix the loop for function extraction.
///        llvm loop extract may crash without this fix
/// \param L Current Loop
void apolloFixLoop(Loop *L) {
  if (!entryBranchToLoopHeader(L)) {
    fixEntryBlock(L);
    fixReturnBlocks(L);
  }
}

/// \brief we can extract the loop if this function returns true.
///        Check LLVM LoopExtractor at line 118 for more info.
bool checkLandingpad(Loop *L) {
  // For an explanation about this check llvm LoopExtractor source
  // http://llvm.org/docs/doxygen/html/LoopExtractor_8cpp_source.html
  std::set<BasicBlock *> ExitBlocks = std::move(loop::getLoopExitBlocks(L));
  for (BasicBlock *exitBlock : ExitBlocks) {
    if (exitBlock->isLandingPad()) {
      errs() << "WARNING: Can't extract the loop. Loop exit is landingpad.\n";
      return true;
    }
  }
  return false;
}

/// \brief This function is used to check if Apollo can handle a loop.
/// \return True if Apollo can handle the loop.
bool apolloCanHandle(Loop *L) {
  // Add restrictions of apollo in this function.
  // Inherited from llvm loop extractor code. Related with loops with
  // exceptions.
  const bool LandingpadCheck = checkLandingpad(L);
  return !LandingpadCheck;
}

} // end anonymous namespace

namespace apollo {

ApolloLoopExtractStrictly::ApolloLoopExtractStrictly() : ModulePass(ID) {}

void ApolloLoopExtractStrictly::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<DominatorTreeWrapperPass>();
  AU.addRequired<LoopInfoWrapperPass>();
}

bool ApolloLoopExtractStrictly::runOnModule(Module &M) {
  DEBUG_WITH_TYPE("apollo-pass-0",
			dbgs() << "Running pass on module: ApolloLoopExtractStrictly\n");
  bool Modify = false;
  std::vector<Function *> Work;
  for (Function &F : M) {
    if (!F.getBasicBlockList().empty())
      Work.push_back(&F);
  }
  for (Function *F : Work) {
    DominatorTree &DT = getAnalysis<DominatorTreeWrapperPass>(*F).getDomTree();
    LoopInfo &LI = getAnalysis<LoopInfoWrapperPass>(*F).getLoopInfo();
    if (runOnFunction(*F, DT, LI))
      Modify = true;
  }
  return Modify;
}

bool ApolloLoopExtractStrictly::runOnFunction(Function &F, DominatorTree &DT,
                                      LoopInfo &LI) {
  DEBUG_WITH_TYPE("apollo-pass-1",
			dbgs() << "   function:" << F.getName().str() << "\n");
  bool apolloLoopExtracted = false;
  Loop *apolloLoop = getTopApolloLoop(&F, &LI);

  // If there is no apolloLoop this will exit the loop.
  while (apolloLoop) {
//      std::cout << std::endl << std::endl << "-------- apolloLoop --------" << std::endl << std::endl << std::endl;
    apolloFixLoop(apolloLoop);
    if (apolloCanHandle(apolloLoop)) {
      Function *Extracted = extractApolloLoop(apolloLoop, DT);
      //
      cleanMetadataFunction(Extracted);
      //
      Extracted->addFnAttr(Attribute::NoInline);
      apolloLoopExtracted = true;
    } else {
      // remove the metadata for loops which we cannot handle.
      cleanMetadataLoop(apolloLoop);
    }
    apolloLoop = getTopApolloLoop(&F, &LI);
  }
  cleanMetadataOutsideApolloLoop(&F, &LI);
  return apolloLoopExtracted;
}

char ApolloLoopExtractStrictly::ID = 0;
//DefinePassCreator(ApolloLoopExtractStrictly);
// Register ApolloLoopExtractStrictly in LLVM pass manager
// Required to use the pass with opt or clang
static RegisterPass<ApolloLoopExtractStrictly> X("loopextractstrictly", "extracts the loop nests tagged by the frontend with pragma apollo dcop into their  own functions - DOES NOT EXTRCT THE OUTERMOST LOOP BUT ONLY THE LOOP THAT HAS APOLLO PRAGMA METADATA");

} // end namespace apollo

//namespace llvm {
//class PassRegistry;
//DeclarePassInitializator(ApolloLoopExtractStrictly);
//} // end namespace llvm

//INITIALIZE_PASS_BEGIN(ApolloLoopExtractStrictly, "APOLLO_LOOP_EXTRACT",
//                      "Apollo loop extract.", false, false)
//INITIALIZE_PASS_DEPENDENCY(DominatorTreeWrapperPass)
//INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
//INITIALIZE_PASS_END(ApolloLoopExtractStrictly, "APOLLO_LOOP_EXTRACT",
//                    "Apollo loop extract.", false, false)
