
#ifndef APOLLO_FUNCTION_CALL_CHOOSER_H
#define APOLLO_FUNCTION_CALL_CHOOSER_H

#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include <iostream>

using namespace llvm;


namespace apollo {

/// \brief inserts a call to runtime function apollo_loop_version_chooser
///        and a conditional branch to the instrumented or parallel version
///        of the apollo loop depending on the output of apollo_loop_version_chooser
  struct ApolloFunctionCallChooser : public FunctionPass {
    static char ID;
    ApolloFunctionCallChooser() : FunctionPass(ID) {}

    bool runOnFunction(Function &F);
  };

} // end namespace apollo

#endif //APOLLO_FUNCTION_CALL_CHOOSER_H
