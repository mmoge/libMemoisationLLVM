//===--- InlineCallsApolloStatements.cpp ------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "InlineCallsApolloStatements.h"
#include "Utils/Utils.h"
#include "Utils/LoopUtils.h"
#include "Utils/ConstantUtils.h"
#include "Utils/MetadataHandler.h"

#include "llvm/IR/ValueMap.h"
#include "llvm/Pass.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/raw_ostream.h"

#include "llvm/Analysis/CallGraph.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/Analysis/AssumptionCache.h"
#include "llvm/IR/Dominators.h"
#include "ApolloMem2Reg.h"

#include <unordered_map>

using namespace llvm;
using namespace apollo;

namespace {



} // end anonymous namespace

namespace apollo {

InlineCallsApolloStatements::InlineCallsApolloStatements() : FunctionPass(ID)
{
}

void InlineCallsApolloStatements::getAnalysisUsage(AnalysisUsage &AU) const {
  FunctionPass::getAnalysisUsage(AU);
  AU.addRequired<AssumptionCacheTracker>();
  AU.addRequired<DominatorTreeWrapperPass>();
  AU.addRequired<myPromoteLegacyPass>();
}

bool InlineCallsApolloStatements::runOnFunction(Function &F) {
  std::set< Function* > functions_to_inline;
  std::set< CallInst* > functioncalls_to_inline;
  std::set< InvokeInst* > functioninvokes_to_inline;
  for ( BasicBlock& BB : F )
  {
      for ( Instruction& I : BB )
      {
          CallInst *CI = dyn_cast<CallInst>(&I);
          if ( CI && metadata::hasMetadataKind(CI, mdkind::apollo) )
          {
              functions_to_inline.insert( (CI->getCalledFunction()) );
              functioncalls_to_inline.insert( CI );
//              llvm::InlineFunction( CI, IFO );
          }
          InvokeInst *II = dyn_cast<InvokeInst>(&I);
          if ( II && metadata::hasMetadataKind(II, mdkind::apollo) )
          {
              functions_to_inline.insert( (II->getCalledFunction()) );
              functioninvokes_to_inline.insert( II );
          }
      }
  }
  // 1 - MemToReg
//   done via getAnalysisUsage
  // 2 - InlineFunction
  InlineFunctionInfo IFI;
  for ( std::set< CallInst* >::iterator itcall = functioncalls_to_inline.begin();
          itcall != functioncalls_to_inline.end();
          ++itcall )
  {
      InlineFunction(*itcall, IFI);
  }
  for ( std::set< InvokeInst* >::iterator itinvoke = functioninvokes_to_inline.begin();
          itinvoke != functioninvokes_to_inline.end();
          ++itinvoke )
  {
      InlineFunction(*itinvoke, IFI);
  }
  return false;
}

char InlineCallsApolloStatements::ID = 0;
static RegisterPass<InlineCallsApolloStatements> InlineCallsApolloStatements("inlinecallsapollostatements","InlineCallsApolloStatements");

//DefinePassCreator_1(InlineCallsApolloStatements, std::string);

} // end namespace apollo

//namespace llvm {
//class PassRegistry;
//DeclarePassInitializator(InlineCallsApolloStatements);
//} // end namespace llvm
//
//
//INITIALIZE_PASS_BEGIN(InlineCallsApolloStatements,
//        "APOLLO_INLINE_CALLS",
//        "APOLLO_INLINE_CALLS", false, false)
//    INITIALIZE_PASS_DEPENDENCY(SimpleInliner)
//    INITIALIZE_PASS_DEPENDENCY(RegToMem)
//    INITIALIZE_PASS_DEPENDENCY(InlineFunction)
//    INITIALIZE_PASS_END(InlineCallsApolloStatements,
//            "APOLLO_INLINE_CALLS",
//            "APOLLO_INLINE_CALLS", false, false)

//INITIALIZE_PASS_BEGIN(InlineCallsApolloStatements, "APOLLO_REG2MEM", "Apollo Reg2Mem.", false,
//                      false)
//INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
//INITIALIZE_PASS_END(InlineCallsApolloStatements, "APOLLO_REG2MEM", "Apollo Reg2Mem.", false,
//                    false)
