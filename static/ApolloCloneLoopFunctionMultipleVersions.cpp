//===--- ApolloCloneLoopFunctionMultipleVersions.cpp --------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "ApolloCloneLoopFunctionMultipleVersions.h"
#include "Utils/Utils.h"
#include "SkeletonConfig.h"

#include "llvm/Transforms/Utils/Cloning.h"

using namespace llvm;
using namespace apollo;

namespace {

/// \brief Clone a function.
/// \param original a function.
/// \return The clone of original
Function *cloneFunctionAndInstructions(Function *F) {
  Function *Clone = Function::Create(F->getFunctionType(), F->getLinkage(), "",
                                     F->getParent());
  ValueToValueMapTy ValueMap;
  ApolloCloneLoopFunctionMultipleVersions::mapArguments(F, Clone, ValueMap);
  SmallVector<ReturnInst *, 8> Returns;
  CloneFunctionInto(Clone, F, ValueMap, false, Returns, "", 0x0);
  ApolloCloneLoopFunctionMultipleVersions::fixInstructions(Clone, ValueMap);
  return Clone;
}

} // end anonymous namespace

namespace apollo {

ApolloCloneLoopFunctionMultipleVersions::ApolloCloneLoopFunctionMultipleVersions()
  : ApolloPassOnSkeleton(ID, "", "ApolloCloneLoopFunctionMultipleVersions") {
    this->CloneToParallel = std::string("skeleton_4");
    this->CloneToInstrumented = std::string("skeleton_1");
    this->CloneToSequential = std::string("skeleton_5");
    }

ApolloCloneLoopFunctionMultipleVersions::ApolloCloneLoopFunctionMultipleVersions(std::string CloneFrom_,
        std::string CloneToPar_, std::string CloneToInstr_, std::string CloneToSeq_)
  : ApolloPassOnSkeleton(ID, CloneFrom_, "ApolloCloneLoopFunctionMultipleVersions") {
    this->CloneToParallel = CloneToPar_;
    this->CloneToInstrumented = CloneToInstr_;
    this->CloneToSequential = CloneToSeq_;
    this->CloneFrom = CloneFrom_;
}

bool ApolloCloneLoopFunctionMultipleVersions::runOnApollo(Function *apolloFunction) {
    const std::string apolloFunctionName = apolloFunction->getName();
    // if cloneFrom is not zero substract an extra character for the "_"
    const std::string apolloLoopName = apolloFunctionName.substr(
        0, apolloFunctionName.size() - CloneFrom.size() - (CloneFrom.size() > 0));
    //
    // 1 - Parallel version
    //
    Function *ClonePar = cloneFunctionAndInstructions(apolloFunction);
    const std::string CloneParName = utils::concat(apolloLoopName, "_", CloneToParallel);
    ClonePar->setName(CloneParName);
    if (CloneToParallel == skeleton::sk_name<skeleton::parallel>()) {
        ClonePar->removeFnAttr(Attribute::NoInline);
        ClonePar->addFnAttr(Attribute::AlwaysInline);
    } else {
        ClonePar->removeFnAttr(Attribute::AlwaysInline);
        ClonePar->addFnAttr(Attribute::NoInline);
    }
    //
    // 2 - Instrumented version
    //
    Function *CloneInstr = cloneFunctionAndInstructions(apolloFunction);
    const std::string CloneInstrName = utils::concat(apolloLoopName, "_", CloneToInstrumented);
    CloneInstr->setName(CloneInstrName);
    if (CloneToInstrumented == skeleton::sk_name<skeleton::instrumentation>()) {
        CloneInstr->removeFnAttr(Attribute::NoInline);
        CloneInstr->addFnAttr(Attribute::AlwaysInline);
    } else {
        CloneInstr->removeFnAttr(Attribute::AlwaysInline);
        CloneInstr->addFnAttr(Attribute::NoInline);
    }
    //
    // 3 - Sequential version
    //
    Function *CloneSeq = cloneFunctionAndInstructions(apolloFunction);
    const std::string CloneSeqName = utils::concat(apolloLoopName, "_", CloneToSequential);
    CloneSeq->setName(CloneSeqName);
    if (CloneToSequential == skeleton::sk_name<skeleton::sequential>()) {
        CloneSeq->removeFnAttr(Attribute::NoInline);
        CloneSeq->addFnAttr(Attribute::AlwaysInline);
    } else {
        CloneSeq->removeFnAttr(Attribute::AlwaysInline);
        CloneSeq->addFnAttr(Attribute::NoInline);
    }
  return true;
}

void ApolloCloneLoopFunctionMultipleVersions::mapArguments(Function *Original, Function *Clone,
                                           ValueToValueMapTy &ValueMap) {
  // Clone and map the arguments of the function.
  for (auto arg = Original->arg_begin(), arg_clone = Clone->arg_begin(),
       arg_en = Original->arg_end(); arg != arg_en; ++arg, ++arg_clone) {
    arg_clone->setName(arg->getName());
    ValueMap[&*arg] = &*arg_clone;
  }
}

void ApolloCloneLoopFunctionMultipleVersions::fixInstructions(Function *Clone,
                                              ValueToValueMapTy &ValueMap) {
  for (BasicBlock &block : *Clone) {
    for (Instruction &instruction : block) {
      Instruction::op_iterator operand, op_end = instruction.op_end();
      for (operand = instruction.op_begin(); operand != op_end; ++operand) {
        Value *OperandValue = *operand;
        if (ValueMap.count(OperandValue)) // if it is mapped
          operand->set(ValueMap[OperandValue]);
      }
    }
  }
}

Function *ApolloCloneLoopFunctionMultipleVersions::addParameters(Function *OldFunction,
                           std::vector<std::pair<std::string, Type *>> &NewArgs,
                           Type *RetTy) {
  // Create the new definition of the function.
  const std::string FunctionName = OldFunction->getName();
  FunctionType *OldFunctionType = OldFunction->getFunctionType();

  if (RetTy == 0x0)
    RetTy = OldFunctionType->getReturnType();

  OldFunction->setName(FunctionName + "_old_to_delete");
  std::vector<std::string> ArgNames;
  std::vector<Type *> ArgTypes;
  ArgTypes.insert(ArgTypes.end(), OldFunctionType->param_begin(),
                  OldFunctionType->param_end());

  for (auto &name_ty_pair : NewArgs) {
    ArgNames.push_back(name_ty_pair.first);
    ArgTypes.push_back(name_ty_pair.second);
  }

  FunctionType *NewFunctionType =
      FunctionType::get(RetTy, ArgTypes, OldFunctionType->isVarArg());
  Function *NewFunction =
      Function::Create(NewFunctionType, OldFunction->getLinkage(), FunctionName,
                       OldFunction->getParent());
  // Insert the new two arguments for the bounds.
  Function::ArgumentListType &ArgList = NewFunction->getArgumentList();
  auto arg_it = ArgList.rbegin();
  for (auto arg_name_it = ArgNames.rbegin(); arg_name_it != ArgNames.rend();
       ++arg_name_it, ++arg_it) {
    arg_it->setName(*arg_name_it);
  }
  // Clone the body of the old function into the new one.
  ValueToValueMapTy ValueMap;
  ApolloCloneLoopFunctionMultipleVersions::mapArguments(OldFunction, NewFunction, ValueMap);
  SmallVector<ReturnInst *, 8> Returns;
  CloneFunctionInto(NewFunction, OldFunction, ValueMap, false, Returns, "",
                    0x0);
  ApolloCloneLoopFunctionMultipleVersions::fixInstructions(NewFunction, ValueMap);
  // And erase the old function.
  OldFunction->eraseFromParent();
  return NewFunction;
}

char ApolloCloneLoopFunctionMultipleVersions::ID = 0;
static RegisterPass<ApolloCloneLoopFunctionMultipleVersions> X("cloneloopfunctionmultipleversions", "Apollo function cloning - clone function apollo_loop_<N> into apollo_loop_<N>_skeleton_1 -> instrumented version + apollo_loop_<N>_skeleton_0 -> parallel version + apollo_loop_<N>_skeleton_2 -> sequential version");
//DefinePassCreator_2(ApolloCloneLoopFunctionMultipleVersions, std::string, std::string);

} // end namespace apollo

//namespace llvm {
//class PassRegistry;
//DeclarePassInitializator(ApolloCloneLoopFunctionMultipleVersions);
//} // end namespace llvm
//
//INITIALIZE_PASS_BEGIN(ApolloCloneLoopFunctionMultipleVersions, "APOLLO_CLONE",
//                      "Apollo function cloning", false, false)
//INITIALIZE_PASS_END(ApolloCloneLoopFunctionMultipleVersions, "APOLLO_CLONE",
//                    "Apollo function cloning", false, false)
