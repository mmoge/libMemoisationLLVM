//===--- InsertColorLoop.cpp -------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "SkeletonConfig.h"
#include "InsertColorLoop.h"
#include "Utils/FunctionUtils.h"
#include "ApolloCloneLoopFunctionMultipleVersions.h"
#include "InstrumentationUtils.h"

#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/IR/IRBuilder.h"

#include <iostream>

using namespace apollo;
using namespace llvm;

namespace {

using NameTypePair = std::pair<std::string, llvm::Type *>;


Function *cloneAndAddParameters(Function *OldFunction,
                       std::vector< NameTypePair > &NewArgs,
                       Type *RetTy = 0x0 ) {
  // Create the new definition of the function.
  const std::string FunctionName = OldFunction->getName().str()+"_augmented";
  FunctionType *OldFunctionType = OldFunction->getFunctionType();

  if (RetTy == 0x0)
    RetTy = OldFunctionType->getReturnType();

  std::vector<std::string> ArgNames;
  std::vector<Type *> ArgTypes;
  ArgTypes.insert(ArgTypes.end(), OldFunctionType->param_begin(),
                  OldFunctionType->param_end());

  for (auto &name_ty_pair : NewArgs) {
    ArgNames.push_back(name_ty_pair.first);
    ArgTypes.push_back(name_ty_pair.second);
  }

  FunctionType *NewFunctionType =
      FunctionType::get(RetTy, ArgTypes, OldFunctionType->isVarArg());
  Function *NewFunction =
      Function::Create(NewFunctionType, OldFunction->getLinkage(), FunctionName,
                       OldFunction->getParent());
  // Insert the new two arguments for the bounds.
  Function::ArgumentListType &ArgList = NewFunction->getArgumentList();
  auto arg_it = ArgList.rbegin();
  for (auto arg_name_it = ArgNames.rbegin(); arg_name_it != ArgNames.rend();
       ++arg_name_it, ++arg_it) {
    arg_it->setName(*arg_name_it);
  }
  // Clone the body of the old function into the new one.
  ValueToValueMapTy ValueMap;
  ApolloCloneLoopFunctionMultipleVersions::mapArguments(OldFunction, NewFunction, ValueMap);
  SmallVector<ReturnInst *, 8> Returns;
  CloneFunctionInto(NewFunction, OldFunction, ValueMap, false, Returns, "",
                    0x0);
  ApolloCloneLoopFunctionMultipleVersions::fixInstructions(NewFunction, ValueMap);
//  // And erase the old function.
//  OldFunction->eraseFromParent();
  return NewFunction;
}


/// \brief Adds arguments to the apollo_loop function.
Function* cloneAndChangeFunctionDefinition(Function* OldFunction) {
  Type *i32Ty = Type::getInt32Ty(OldFunction->getContext());
  Type *i32PtrTy = Type::getInt32PtrTy(OldFunction->getContext());
  std::vector<NameTypePair> NewParameters = {
      NameTypePair("color_first_it", i32Ty),
      NameTypePair("color_last_it", i32Ty),
      NameTypePair("colored_its_array", i32PtrTy) };
  return cloneAndAddParameters(OldFunction, NewParameters);
}

Constant* getLoopId(Function* F)
{
    StringRef name = F->getName();
    std::string namestr = name.str();
    if ( namestr.substr(0,12) == std::string("apollo_loop_") )
    {
        std::string loopidstr = namestr.substr(12,namestr.find_first_not_of("0123456789",12)-1);
        Type *i64Ty = Type::getInt64Ty(F->getContext());
        Constant *loopid = ConstantInt::get(i64Ty, stoi(loopidstr));
        return loopid;
    }
    else
    {
        std::cerr << "ERROR: function " << namestr << " is not a valid apollo loop !" << std::endl;
        return nullptr;
    }
}

AllocaInst* createOuterColorLoopInductionVariable( IRBuilder<> Builder, Function* F, LLVMContext& C )
{
    Type *i32Ty = Type::getInt32Ty(C);
    AllocaInst* currentcoloralloc = Builder.CreateAlloca(i32Ty);
    currentcoloralloc->setName("currentcolor");
    Builder.CreateStore(Builder.getInt32(0),currentcoloralloc);
    return currentcoloralloc;
}
CallInst* createCallGetNbColor( IRBuilder<> Builder, Function* F, Module* M, Constant *loopid )
{
    // get nb colors
    std::vector<Value *> Params;
    Params.push_back(loopid);
    Function *GetNbColorsFun = getApolloGetNbColors(M);
    CallInst* nbcolors = Builder.CreateCall(GetNbColorsFun, Params);
    nbcolors->setName("nbcolors");
    return nbcolors;
}
CallInst* createCallGetItsArray( IRBuilder<> Builder, Function* F, Module* M, Constant *loopid )
{
    // get its array : corresponding to the colored graph
    std::vector<Value *> Params;
    Params.push_back(loopid);
    Function *GetItsArray = getApolloGetItsArray(M);
    CallInst* itsarray = Builder.CreateCall(GetItsArray, Params);
    itsarray->setName("itsarray");
    return itsarray;
}

BasicBlock* createOuterColorLoopHeader( BasicBlock* insertBefore,
        BasicBlock* insertAfter, Function* F, LLVMContext& C )
{
    BasicBlock* colorLoopHeader = BasicBlock::Create(
            C, "for.color.cond", F, insertBefore );
    insertAfter->getTerminator()->eraseFromParent();
    BranchInst::Create(colorLoopHeader, insertAfter);
    return colorLoopHeader;
}

} // end namespace anonymous

namespace apollo {
InsertColorLoop::InsertColorLoop()
  : ApolloPassOnSkeleton(ID, skeleton::sk_name<skeleton::parallel>(), "InsertColorLoop") {
}

void InsertColorLoop::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<LoopInfoWrapperPass>();
}

bool InsertColorLoop::runOnApollo(Function *F) {
    //
    // 0 - Initialize constant, types, etc
    //
    LLVMContext& C = F->getContext();
    Module *M = F->getParent();
    Type *i32Ty = Type::getInt32Ty(C);
    Type *i32Ptr = Type::getInt32PtrTy(C);
    Constant *loopid = getLoopId(F);
    Constant *zero   = ConstantInt::get(i32Ty, 0, false);
    Constant *one    = ConstantInt::get(i32Ty, 1, false);
    //
    // 1 - In function Entry Block : load colored graph variables nbcolors and itsarray
    //
    BasicBlock& entryBlock = F->getEntryBlock();
    // identify parallelLoopCallBlock
    // and check if there is a block before the parallelLoopCallBlock (other than entryBlock)
    BasicBlock* parallelLoopCallBlockPredecessor = nullptr;
    BasicBlock* parallelLoopCallBlock = nullptr;
    for ( BasicBlock& BB : *F )
    {
        // parallelLoopCallBlock should have only 2 instructions : CallInst + BranchInst
        Instruction* firstInst = &*(BB.begin());
        CallInst* CI = dyn_cast<CallInst>(firstInst);
        if( CI && CI->getCalledFunction()->getName().str().find( F->getName().str()+"_skeleton_4" ) )
        {
            //this is parallelLoopCallBlock
            parallelLoopCallBlock = &BB;
            break;
        }
    }
    if( parallelLoopCallBlock->getUniquePredecessor() != &entryBlock )
    {
        //There is a block between parallelLoopCallBlock and entryBlock
        parallelLoopCallBlockPredecessor = parallelLoopCallBlock->getUniquePredecessor();
    }
    else
    {
        parallelLoopCallBlockPredecessor = &entryBlock;
    }
//    BasicBlock* parallelLoopCallBlock = entryBlock.getUniqueSuccessor();
    //
    IRBuilder<> predecessorBlockBuilder( parallelLoopCallBlockPredecessor->getTerminator() );
    //create induction variable
    AllocaInst* currentcoloralloc = createOuterColorLoopInductionVariable( predecessorBlockBuilder, F, C );
    // get nb colors
    CallInst* nbcolors = createCallGetNbColor( predecessorBlockBuilder, F, M, loopid );
    // get its array : corresponding to the colored graph
    CallInst* coloredItsArray = createCallGetItsArray( predecessorBlockBuilder, F, M, loopid );
    //
    // 2 - Create new loop (color loop)
    //
    BasicBlock* colorLoopHeader = BasicBlock::Create(
            C, "for.color.cond", F, parallelLoopCallBlock );
    BasicBlock* colorLoopBody = BasicBlock::Create(
            C, "for.color.body", F, parallelLoopCallBlock );
    BasicBlock* colorLoopInc = BasicBlock::Create(
            C, "for.color.inc", F, parallelLoopCallBlock );
    // 2.1 - Create color loop header
    entryBlock.getTerminator()->eraseFromParent();
    BranchInst::Create(colorLoopHeader, &entryBlock);
    IRBuilder<> colorLoopHeaderBuilder(colorLoopHeader);
    //load induction variable -> currentcolor
    Value* currentcolor = colorLoopHeaderBuilder.CreateLoad(i32Ty,currentcoloralloc,"currentcolor");
    //create conditional branch inst to colorLoopBody or function exit block
    Value* colorLoopCond = colorLoopHeaderBuilder.CreateICmpSLT(currentcolor,nbcolors);
    BranchInst* colorLoopBI = BranchInst::Create(colorLoopBody, parallelLoopCallBlock->getTerminator()->getSuccessor(0),
            colorLoopCond, colorLoopHeader);
    // 2.2 - Create new loop body
    IRBuilder<> colorLoopBodyBuilder(colorLoopBody);
    // get colored graph info :
    // nb of iterations of current color and first index with current color in itsarray
    Function *GetColorSize = getApolloColorSize(M);
    CallInst* colorSize = colorLoopBodyBuilder.CreateCall(GetColorSize, {loopid,currentcolor} );
    colorSize->setName("colorsize");
    Function *GetColorFirst = getApolloColorFirst(M);
    CallInst* colorFirst = colorLoopBodyBuilder.CreateCall(GetColorFirst, {loopid,currentcolor});
    colorFirst->setName("colorfirst");
    Function *GetColorLast = getApolloColorLast(M);
    CallInst* colorLast = colorLoopBodyBuilder.CreateCall(GetColorLast, {loopid,currentcolor});
    colorLast->setName("colorlast");
    //branch to inner loop (original loop) block
    colorLoopBodyBuilder.CreateBr( parallelLoopCallBlock );
    // 2.3 - Create new loop latch (inc block)
    IRBuilder<> colorLoopIncBuilder(colorLoopInc);
    //increment color counter -> currentcolor
    currentcolor = colorLoopIncBuilder.CreateLoad( i32Ty, currentcoloralloc, "currentcolor" );
    Value* nextcolor = colorLoopIncBuilder.CreateAdd( currentcolor, one );
    colorLoopIncBuilder.CreateStore(nextcolor,currentcoloralloc);
    //branch to color loop header (cond block)
    BranchInst::Create(colorLoopHeader, colorLoopInc);
    //branch parallelLoopCallBlock to color loop latch (inc block)
    parallelLoopCallBlock->getTerminator()->setSuccessor(0, colorLoopInc);
    //
    // 3 - get parallel loop function call inst
    //
    parallelLoopCallBlock->setName("inner.loop.parallel");
    Function* parallelLoopFunc = nullptr;
    CallInst* parallelLoopFuncCallInst = nullptr;
    for ( Instruction& I : *parallelLoopCallBlock )
    {
        CallInst* CI = dyn_cast<CallInst>(&I);
        if( CI )
        {
            parallelLoopFunc = CI->getCalledFunction();
            parallelLoopFuncCallInst = CI;
            break;
        }
    }
    //
    // 4 - Add parameters to parallel loop function call inst
    //
    std::string parallelLoopFuncName = parallelLoopFunc->getName().str();
    Function* parallelLoopFuncColored = cloneAndChangeFunctionDefinition( parallelLoopFunc );
    unsigned int nbOriginalParams = parallelLoopFuncCallInst->getNumArgOperands();
    std::vector< Value* > newParams;
    for ( unsigned int paramit = 0;
            paramit < nbOriginalParams;
            ++paramit )
    {
        newParams.push_back( parallelLoopFuncCallInst->getArgOperand(paramit) );
    }
    newParams.push_back( colorFirst);
    newParams.push_back( colorLast);
    newParams.push_back( coloredItsArray);
    IRBuilder<> parallelCallBuilder(parallelLoopFuncCallInst);
    parallelCallBuilder.CreateCall(parallelLoopFuncColored,
            newParams);
    parallelLoopFuncCallInst->eraseFromParent();
    parallelLoopFunc->eraseFromParent();
    parallelLoopFuncColored->setName(parallelLoopFuncName);
  return true;
}

char InsertColorLoop::ID = 0;
static RegisterPass<InsertColorLoop> InsertColorLoop("insertcolorloop","inserts color loop in parallel version of apollo loop");

//DefinePassCreator(InsertColorLoop);

} // end namespace apollo

//namespace llvm {
//class PassRegistry;
//DeclarePassInitializator(InsertColorLoop);
//} // end namespace llvm
//
//INITIALIZE_PASS_BEGIN(InsertColorLoop,
//                      "APOLLO_INSTRUMENT_FIX_FUNCTION_SIGNATURE",
//                      "APOLLO_INSTRUMENT_FIX_FUNCTION_SIGNATURE", false, false)
//INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
//INITIALIZE_PASS_END(InsertColorLoop,
//                    "APOLLO_INSTRUMENT_FIX_FUNCTION_SIGNATURE",
//                    "APOLLO_INSTRUMENT_FIX_FUNCTION_SIGNATURE", false, false)
