//===--- ApolloStatementMetadataAdderStore.cpp ---------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "ApolloStatementMetadataAdderStore.h"

#include "MetadataKind.h"
#include "Utils/Utils.h"
#include "Utils/ViUtils.h"
#include "Utils/FunctionUtils.h"
#include "Utils/ConstantUtils.h"
#include "Utils/MetadataHandler.h"

#include "llvm/Analysis/LoopInfo.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Metadata.h"
#include "llvm/IR/InstIterator.h"

//#include "GetInductionVariable.h"
//#include "llvm/Analysis/ScalarEvolution.h"

#include <unordered_map>
#include <vector>
#include <string>

using namespace apollo;
using namespace llvm;

namespace {

unsigned getNumPredeccessors(const BasicBlock *BB) {
  unsigned Num = 0;
  for (auto it = llvm::pred_begin(BB); it != llvm::pred_end(BB); ++it) {
    Num++;
  }
  return Num;
}

void updateSuccessorsIncomming(BasicBlock *CurBlock, LoopInfo *LI,
                        std::unordered_map<BasicBlock *, unsigned> &Incomming) {

  for (auto succ_it = succ_begin(CurBlock); succ_it != succ_end(CurBlock);
       ++succ_it) {
    if (LI->getLoopFor(*succ_it)) // it must be inside the nest.
      Incomming[*succ_it] = Incomming[*succ_it] - 1;
  }
}

BasicBlock *getNextBlock(Loop *L,
             const std::unordered_map<BasicBlock *, unsigned> &Incomming,
             const std::set<BasicBlock *> &VisitedBlocks) {

  for (auto &block_incomming : Incomming) {
    const bool Visited = VisitedBlocks.count(block_incomming.first) != 0;
    const bool InsideLoop = L->contains(block_incomming.first);
    const bool IncommingVisited = block_incomming.second <= 0;
    if (!Visited && InsideLoop && IncommingVisited)
      return block_incomming.first;
  }
  return 0x0;
}

/// \brief returns an integer id for the apollo loop corresponding to function F
///        ex : apollol_loop<N> has id N
int getLoopId(Function* F) {
    std::string fname = std::string(F->getName());
    size_t begin = fname.find_first_of("0123456789");
    size_t end = fname.find_first_of("_", begin);
    std::string loopidstr = fname.substr(begin,end);
    return std::stoi(loopidstr);
}

} // end anonymous namespace

namespace apollo {

ApolloStatementMetadataAdderStore::ApolloStatementMetadataAdderStore() : ApolloPass(ID, "ApolloStatementMetadataAdderStore") {
  IdCounter = 0;
}

void ApolloStatementMetadataAdderStore::getAnalysisUsage(AnalysisUsage &AU) const {
  ApolloPass::getAnalysisUsage(AU);
  AU.addRequired<LoopInfoWrapperPass>();
//  AU.addRequired<ScalarEvolutionWrapperPass>();
}

bool ApolloStatementMetadataAdderStore::runOnApollo(Function *F) {
  IdCounter = 0;
  DepthNbUsed.clear();
  Table.clear();
  LoopInfo &LI = getAnalysis<LoopInfoWrapperPass>(*F).getLoopInfo();
  Loop *L = function::getOutermostLoopInFunction(F, &LI);

  //we attach a metadata LoopId on the terminator of the entry block of the apollo functions
  metadata::attachMetadata(
          F->getEntryBlock().getTerminator(), mdkind::LoopId,
          {constant::getInt64Constant(F->getContext(), getLoopId(F))});

  // visit the loop and do it recursively on subloops
  std::unordered_map<BasicBlock *, unsigned> IncommingBlocks;
  for (BasicBlock *block : L->getBlocks()) {
    unsigned PredNum = getNumPredeccessors(block);
    const bool IsHeader = LI[block]->getHeader() == block;
    if (IsHeader) // to break the cycles and get a topological order
      PredNum--;
    IncommingBlocks[block] = PredNum;
  }
  IncommingBlocks[L->getHeader()] = 0;
  auto EmptyVisited = std::set<BasicBlock *>();
  statementAddIdRecursively(L, std::vector<unsigned>(), 0, IncommingBlocks,
                            EmptyVisited, &LI);
  return true;
}

void ApolloStatementMetadataAdderStore::statementAddIdBlock(
    BasicBlock *CurBlock, Loop *L, std::vector<unsigned> &Path, LoopInfo *LI) {
  LLVMContext &Context = CurBlock->getContext();
  for (Instruction &inst_iter : *CurBlock) {
    // add a metadata for store instruction
      //if the operand of the store inst is marked _apollo_dcop_local,
      //then we do not want to instrument it, so we do not add metadata
      if (statement::isStore(&inst_iter) && !statement::isStorePointerOperandLocal(&inst_iter) ) {
//              // we only want to instrument stores in _apollo_dcop_shared variables  -> shared is considered the default for all non apollo_dcop_local variable
//          if (statement::isStore(&inst_iter) && !statement::isStorePointerOperandShared(&inst_iter) ) {
            MapVectorUiType::iterator mit = DepthNbUsed.find(Path);
          // if there are ids in the same level increase the biggest.
          if (mit != DepthNbUsed.end())
            DepthNbUsed[Path] = DepthNbUsed[Path] + 1;
          else
            DepthNbUsed[Path] = 0;
          std::vector<Value *> MetadataNode;
          // first value is id.
          MetadataNode.push_back(constant::getInt64Constant(Context, IdCounter));
          // in the middle the loop path.
          for (unsigned p : Path) {
            MetadataNode.push_back(constant::getInt64Constant(Context, p));
          }
          metadata::attachMetadata(&inst_iter, mdkind::ApolloStatementId,
                                   MetadataNode);
          metadata::attachMetadata(
              &inst_iter, mdkind::ParentLoop,
              {constant::getInt64Constant(Context, loop::getLoopId(L))});
          //we attach a metadata LoopId
              metadata::attachMetadata(
              &inst_iter, mdkind::LoopId,
              {constant::getInt64Constant(Context, getLoopId(inst_iter.getFunction()))});
          Table[IdCounter] = make_pair(Path, DepthNbUsed[Path]);
          ++IdCounter;
      }
  }
}

/// Remark: We do not want to instrument stores on the Induction Variable.
void ApolloStatementMetadataAdderStore::statementAddIdRecursively(
                           Loop *L, 
                           std::vector<unsigned int> Path, 
                           unsigned Position, 
                           std::unordered_map<BasicBlock *, unsigned> &Incomming,
                           std::set<BasicBlock *> &VisitedBlocks, 
                           LoopInfo *LI) {

  Path.push_back(Position);
  std::vector<Loop *> SubLoops = L->getSubLoops();
  BasicBlock *CurBlock;

  while ((CurBlock = getNextBlock(L, Incomming, VisitedBlocks)) != 0x0) {
    const bool isInThisLoop = LI->getLoopFor(CurBlock) == L;
    if (isInThisLoop) {
      // 1 - add metadata to all stores
      statementAddIdBlock(CurBlock, L, Path, LI);

      updateSuccessorsIncomming(CurBlock, LI, Incomming);
      VisitedBlocks.insert(CurBlock);

      //REMARK: we do not want to record the stores to the Induction Variable as it does
      //        not create dependencies between iterations.
      //        we look at stores only in the body of the loop
      //
      // 2 - remove the metadata from the stores to the IV
      //
      // PB: getInductionVariable is crashing, because ScalarEvolutionWrapperPass does no seem to work correctly and I do not understand why
      // so I use getCanonicalInductionVariable instead
//        Function* F = L->getHeader()->getParent();
//        ScalarEvolutionWrapperPass &SeWrapper = getAnalysis<ScalarEvolutionWrapperPass>(*F);
//        ScalarEvolution &SE = SeWrapper.getSE();
//        PHINode* IV = getInductionVariable( L, &SE );
      auto IV = L->getCanonicalInductionVariable();
      if (IV)
      {
          for(auto  U : IV->users())
          {
              if (auto IU = dyn_cast<Instruction>(U)){
//                  IU->print(outs());
//                  std::cout << std::endl;
                  if ( metadata::hasMetadataKind(IU, mdkind::ApolloStatementId) )
                  {
//                      IU->print(outs());
//                      std::cout << std::endl;
                      IU->setMetadata(mdkind::ApolloStatementId, 0x0);
                  }
              }
          }
      }
      //
      // 3 - remove the metada from the stores in the loop latches and header block
      //
      // check if CurBlock is the loop header
      BasicBlock *loopHeader = L->getHeader();
      bool isLoopHeader = false;
      if ( loopHeader == CurBlock )
      {
          isLoopHeader = true;
      }
      if ( isLoopHeader )
      {
          for( Instruction& I : *loopHeader )
          {
              StoreInst* SI = dyn_cast<StoreInst>(&I);
              if( SI )
              {
                  if ( metadata::hasMetadataKind(SI, mdkind::ApolloStatementId) )
                  {
                      SI->setMetadata(mdkind::ApolloStatementId, 0x0);
                  }
              }
          }
      }
      // check if CurBlock is a loop latch
      bool isLoopLatch = false;
      SmallVector< BasicBlock *,4> loopLatches;
      L->getLoopLatches( loopLatches );
      for( SmallVector< BasicBlock *,4>::const_iterator it = loopLatches.begin();
              it != loopLatches.end();
              ++it )
      {
          if ( *it == CurBlock )
          {
              isLoopLatch = true;
          }
      }
      if (isLoopLatch)
      {
          for( Instruction& I : *CurBlock )
          {
              StoreInst* SI = dyn_cast<StoreInst>(&I);
              if( SI )
              {
                  if ( metadata::hasMetadataKind(SI, mdkind::ApolloStatementId) )
                  {
                      SI->setMetadata(mdkind::ApolloStatementId, 0x0);
                  }
              }
          }
      }
    } else {
      // find the subloop that contains the block (it can be different from
      // LI->getLoopFor)
      Loop *ParentSubloop = 0x0;
      unsigned SubLoopPos = 0;
      for (SubLoopPos = 0; SubLoopPos < SubLoops.size() && ParentSubloop == 0x0;
           ++SubLoopPos) {
        if (SubLoops[SubLoopPos]->contains(CurBlock))
          ParentSubloop = SubLoops[SubLoopPos];
      }
      statementAddIdRecursively(ParentSubloop, Path, SubLoopPos - 1, Incomming,
                                VisitedBlocks, LI);
    }
  }
}

char ApolloStatementMetadataAdderStore::ID = 0;
//DefinePassCreator(ApolloStatementMetadataAdderStore);
static RegisterPass<ApolloStatementMetadataAdderStore> ApolloStatementMetadataAdderStore("statementmetadataadderstore","Pass that adds apollo stmt metadata on store in apollo loops");


} // end namespace apollo

//namespace llvm {
//class PassRegistry;
//DeclarePassInitializator(ApolloStatementMetadataAdderStore);
//} // end namespace llvm
//
//INITIALIZE_PASS_BEGIN(ApolloStatementMetadataAdderStore,
//                      "APOLLO_STATEMENT_METADATA_ADDER",
//                      "APOLLO_STATEMENT_METADATA_ADDER", false, false)
//INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
//INITIALIZE_PASS_END(ApolloStatementMetadataAdderStore,
//                    "APOLLO_STATEMENT_METADATA_ADDER",
//                    "APOLLO_STATEMENT_METADATA_ADDER", false, false)
