//===--- ApolloViInsertOuterLoop.cpp -----------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "SkeletonConfig.h"
#include "ApolloViInsertOuterLoop.h"
//#include "BasicScalarPredict.h"
#include "Utils/FunctionUtils.h"
#include "Utils/MetadataHandler.h"
#include "Utils/LoopUtils.h"
#include "Utils/ConstantUtils.h"
#include "Utils/Utils.h"

#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include "llvm/Support/raw_ostream.h"

using namespace apollo;
using namespace llvm;

namespace {

/// \brief Insert one preheader block in the loop.
/// \param L a loop.
/// \return the new preheader.
BasicBlock *insertPreheaderBlock(Loop *L) {
  BasicBlock *Header = L->getHeader();
  BasicBlock *preheader;
  Function *Parent = Header->getParent();
  preheader = BasicBlock::Create(
      Header->getContext(), Header->getName() + "_preheader", Parent, Header);
  for (auto predecesor = pred_begin(Header); predecesor != pred_end(Header);
       ++predecesor) {
    BasicBlock *PredBlock = cast<BasicBlock>(*predecesor);
    if (!L->contains(PredBlock))
      PredBlock->getTerminator()->replaceUsesOfWith(Header, preheader);
  }
  BranchInst::Create(Header, preheader);
  return preheader;
}

/// \brief Insert one unique loop latch in to the loop.
/// \param L a Loop.
/// \return The new loop latch.
BasicBlock *insertUniqueLatch(Loop *L) {
  BasicBlock *Header = L->getHeader();
  std::vector<BasicBlock *> Latches;
  for (auto pred = pred_begin(Header), pred_en = pred_end(Header);
       pred != pred_en; pred++) {
    BasicBlock *PredBlock = *pred;
    if (L->contains(PredBlock))
      Latches.push_back(PredBlock);
  }
  BasicBlock *UniqueLatch = BasicBlock::Create(
      Header->getContext(), "unique_latch", Header->getParent());
  BranchInst::Create(Header, UniqueLatch);
  for (BasicBlock *old_latch : Latches) {
    old_latch->getTerminator()->replaceUsesOfWith(Header, UniqueLatch);
  }
  return UniqueLatch;
}

/// \brief Function to insert the virtual iterator in Loop L
/// \param L input loop
/// \param PositionInSameDepth the index of the subloops in parent loop
void insertVirtualIterator(Loop *L, unsigned int PositionInSameDepth,
                           Value *Step) {
//    std::cout << "insertVirtualIterator" << std::endl;
  Function *F = loop::getLoopParentFunction(L);
  BasicBlock *HeaderBlock = L->getHeader();
  BasicBlock *PreheaderBlock = L->getLoopPreheader();
  const unsigned LoopId = loop::getLoopId(L);
  if (!PreheaderBlock)
    PreheaderBlock = insertPreheaderBlock(L);

  LLVMContext &Context = F->getContext();
  Type *i64Ty = Type::getInt64Ty(Context);
  const std::string VirtualItName = utils::concat("vi.", LoopId);
  AllocaInst *VirtualIt =
      new AllocaInst(i64Ty, VirtualItName, &*(F->getEntryBlock().begin()));
  metadata::attachMetadata(VirtualIt, mdkind::ApolloVi,
                           {constant::getInt64Constant(Context, LoopId)});
  Constant *i64_zero = ConstantInt::get(i64Ty, 0, false);
  StoreInst *VirtualItInit =
      new StoreInst(i64_zero, VirtualIt, VirtualIt->getNextNode());
  // add an incrementation for this virtual iterator on the increment block loop
  BasicBlock *LatchBlock = L->getLoopLatch();
  if (!LatchBlock) // the single latch may not exist.
    LatchBlock = insertUniqueLatch(L);
  Instruction *LatchTerminator = LatchBlock->getTerminator();
  LoadInst *ViLoad =
      new LoadInst(VirtualIt, VirtualItName + "_load", (HeaderBlock->getTerminator()));
  BinaryOperator *Add = BinaryOperator::CreateAdd(
      ViLoad, Step, VirtualItName + "_inc", LatchTerminator);
  new StoreInst(Add, VirtualIt, LatchTerminator);
}

/// \brief Function to insert the virtual iterator in L and all its
/// subloops
/// \param L input loop
void insertVirtualIteratorRecursively(Loop *L, unsigned PositionInSameDepth) {
  LLVMContext &Context = L->getHeader()->getContext();
  Value *Step = constant::one64(Context);
  insertVirtualIterator(L, PositionInSameDepth, Step);
  std::vector<Loop *> SubLoopsVector = L->getSubLoops();
}

} // end anonymous namespace

namespace apollo {

ApolloViInsertOuterLoop::ApolloViInsertOuterLoop() : ApolloPassOnSkeleton(ID, skeleton::sk_name<skeleton::instrumentation>(), "ApolloViInsertOuterLoop") {}
ApolloViInsertOuterLoop::ApolloViInsertOuterLoop(std::string SkeletonName)
  : ApolloPassOnSkeleton(ID, SkeletonName, "ApolloViInsertOuterLoop") {}

void ApolloViInsertOuterLoop::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<LoopInfoWrapperPass>();
}

bool ApolloViInsertOuterLoop::runOnLoop(Loop *L) {
  insertVirtualIteratorRecursively(L, 0);
  return true;
}

char ApolloViInsertOuterLoop::ID = 0;
//DefinePassCreator_1(ApolloViInsertOuterLoop, std::string);
static RegisterPass<ApolloViInsertOuterLoop> ApolloStatementMetadataAdder("viinsertouterloop","ApolloViInsertOuterLoop inserts a Virtual Iterator on the outermost loop of the apollo loop nests");


} // end namespace apollo

//namespace llvm {
//class PassRegistry;
//DeclarePassInitializator(ApolloViInsertOuterLoop);
//} // end namespace llvm
//
//INITIALIZE_PASS_BEGIN(ApolloViInsertOuterLoop, "APOLLO_VIRTUAL_ITERATORS_INSERT",
//                      "APOLLO_VIRTUAL_ITERATORS_INSERT", false, false)
//INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
//INITIALIZE_PASS_END(ApolloViInsertOuterLoop, "APOLLO_VIRTUAL_ITERATORS_INSERT",
//                    "APOLLO_VIRTUAL_ITERATORS_INSERT", false, false)
