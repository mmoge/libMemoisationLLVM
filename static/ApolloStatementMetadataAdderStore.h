//===--- ApolloStatementMetadataAdderStore.h -----------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef APOLLO_STATEMENT_METADATA_ADDERS_MODIFIED_H
#define APOLLO_STATEMENT_METADATA_ADDERS_MODIFIED_H

#include "BasicPass/ApolloPass.h"

#include <unordered_map>
#include <map>
#include <set>

namespace apollo {

/// \brief  This pass sets (through  metadata) a StatmentId  and a parent loopId  to each
///         store access of the apollo loop nest.
///         This pass is largely taken from ApolloStatementMetadataAdder
///
///         If the operand of the store inst is marked _apollo_dcop_local,
///         then we do not want to instrument it, so we do not add metadata.
///         We do not want to instrument stores on the Induction Variable.
struct ApolloStatementMetadataAdderStore : public pass::ApolloPass {
  using MapVectorUiType = std::map<std::vector<unsigned int>, unsigned int>;
  using TableType = 
          std::unordered_map<unsigned int, std::pair<std::vector<unsigned int>,
                                                     unsigned int>>;

  unsigned int IdCounter;
  MapVectorUiType DepthNbUsed;
  TableType Table;
  static char ID;

  ApolloStatementMetadataAdderStore();

  virtual void getAnalysisUsage(llvm::AnalysisUsage &AU) const;
  virtual bool runOnApollo(llvm::Function *F);

  /// \brief Function to add an id to each statement in a basic block.
  /// \param BB current block
  /// \param L input loop
  /// \param Path the path from outermost to this
  void statementAddIdBlock(llvm::BasicBlock *BB, llvm::Loop *L,
                           std::vector<unsigned> &Path, llvm::LoopInfo *LI);

  /// \brief Function to add an id to each statement in subloops of the loop L.
  ///        It iterates over the blocks and subloops in topological order.
  /// \param L input loop
  /// \param Path the path from outermost to this
  /// \param Incomming map between blocks and number of incomming basic blocks
  ///        visited.
  /// \param Visited visited basicblocks.
  /// \param LI the loop info analysis
  void statementAddIdRecursively(
      llvm::Loop *L, std::vector<unsigned> Path, unsigned Position,
      std::unordered_map<llvm::BasicBlock *, unsigned> &Incomming,
      std::set<llvm::BasicBlock *> &VisitedBlocks, llvm::LoopInfo *LI);
};

} // end namespace apollo

#endif
