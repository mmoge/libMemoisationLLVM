#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "ApolloFunctionCallChooser.h"
#include <iostream>
#include "InstrumentationUtils.h"

using namespace llvm;
using namespace apollo;

namespace {

/// \brief createVersionChooserBlock Creates the block in which the call to
///        apollo_loop_version_chooser and the conditionall branch will go.
/// \param BB the block that I belongs to
/// \param I the call inst we want to replace
/// \return The newly created block.
BasicBlock *createVersionChooserBlock(BasicBlock& BB, Instruction *I) {
    BasicBlock *BlockCont = BB.splitBasicBlock(BB.getTerminator(),
            BB.getName() + ".cont");
    BasicBlock *ChooserBlock = BasicBlock::Create(BB.getContext(),
            BB.getName() + ".chooser", BB.getParent(), BlockCont);
    BB.getTerminator()->eraseFromParent();
    BranchInst::Create(ChooserBlock, &BB);
    BranchInst::Create(BlockCont, ChooserBlock);
    return ChooserBlock;
}

} // end anonymous namespace


bool ApolloFunctionCallChooser::runOnFunction(Function &F) {
  //WARNING: if we modify a block we need to restart runOnFunction
  //         since the following instruction and the successor block become invalid
  bool Modified = false;
  for (Function::iterator fit = F.begin(); fit != F.end() && Modified == false; ++fit )
  {
      BasicBlock &B = *fit;
    for (BasicBlock::iterator bbit = B.begin(); bbit != B.end() && Modified == false; ++bbit )
    {
      Instruction &I = *bbit;
      CallInst* ci = dyn_cast<CallInst>(&I);
      if (ci ) {
          llvm::Function *oldfunc = ci->getCalledFunction();
          // WARNING: getCalledFunction returns the function called, or null if this is an indirect function invocation.
          if( oldfunc ) // i.e. if getCalledFunction returned a function
          {
              std::vector<Value *> oldparams;
              for( User::op_iterator opit = ci->arg_begin(); opit != ci->arg_end(); ++opit )
              {
                  oldparams.push_back(*opit);
              }
              StringRef name = oldfunc->getName();
              std::string namestr = name.str();
              if ( namestr.substr(0,12) == std::string("apollo_loop_") && namestr.find_first_of("_",12) == std::string::npos )
              {
                  std::string loopidstr = namestr.substr(12,namestr.find_first_not_of("0123456789",12)-1);
                  Type *i64Ty = Type::getInt64Ty(ci->getContext());
                  Constant *loopid = ConstantInt::get(i64Ty, stoi(loopidstr));
                  Type *i32Ty = Type::getInt32Ty(ci->getContext());
                  Constant *one32 = ConstantInt::get(i32Ty, 1);
                  Constant *four32 = ConstantInt::get(i32Ty, 4);
                  //creation of a new block with
                  // 1 - call to apollo_loop_header to increment the call counter
                  // 2 - call to apollo_loop_version_chooser
                  // 3 - conditional branch testing the result of apollo_loop_version_chooser
                  //     and calling apollo_loop_<N>_skeleton_1 for instrumented version
                  //                 apollo_loop_<N>_skeleton_4 for parallel version
                  //                 apollo_loop_<N>_skeleton_5 for original sequential version
                  IRBuilder<> Builder(B.getTerminator());
                  Module *M = B.getParent()->getParent();
                  //
                  std::vector<Value *> chooserparams;
                  chooserparams.push_back(loopid);
                  Function *LoopVersionChooserFun = getApolloLoopVersionChooser(M);
                  CallInst* chooserCI = Builder.CreateCall(LoopVersionChooserFun, chooserparams);
                  //
                  BasicBlock* successor = B.getUniqueSuccessor();
                  BasicBlock *parallelBlock = BasicBlock::Create( ci->getContext(), B.getName()+".parallel",&F,successor);
                  BranchInst::Create(successor, parallelBlock);
                  //add call to apollo_loop_<N>_skeleton_4
                  IRBuilder<> ParallelBlockBuilder(parallelBlock->getTerminator());
                  std::string new_namestrpara = namestr+"_skeleton_4";
                  llvm::Function *newfuncpara = M->getFunction( new_namestrpara.c_str() );
                  ParallelBlockBuilder.CreateCall(newfuncpara, oldparams );
                  //
                  BasicBlock *instrumentationBlock = BasicBlock::Create( ci->getContext(), B.getName()+".instrumentation",&F,successor);
                  BranchInst::Create(successor, instrumentationBlock);
                  //add call to apollo_loop_<N>_skeleton_1
                  IRBuilder<> InstrumentationBlockBuilder(instrumentationBlock->getTerminator());
                  std::string new_namestrinstr = namestr+"_skeleton_1";
                  llvm::Function *newfuncinstr = M->getFunction( new_namestrinstr.c_str() );
                  InstrumentationBlockBuilder.CreateCall(newfuncinstr, oldparams );
                  //
                  Value* isparallel = Builder.CreateICmpEQ(chooserCI,four32, "isparallel");
                  BranchInst* bi = Builder.CreateCondBr(isparallel,parallelBlock,instrumentationBlock);
                  B.getTerminator()->eraseFromParent();
                  //
                  // erase call to apollo_loop<N>
                  ci->replaceAllUsesWith(UndefValue::get(ci->getType()));
                  ci->eraseFromParent();
                  //
                  Modified = true;
              }
          }
      }
    }
  }
  if( Modified )
  {
      runOnFunction(F);
  }
  return false;
}

// Definition and initialization of the static ID field
// of class FunctionNamesPass
char ApolloFunctionCallChooser::ID = 0;
///////////////////////////////////////
/// Register the pass for opt usage ///
//////////////////////////////////////
static RegisterPass<ApolloFunctionCallChooser> X("apollofunctioncallchooser", "replace calls to apollo_loop_<N> with call to apollo_loop_version_chooser + conditional branch to choose which version to call");


///////////////////////////////////////////
///// Register the pass for clang usage ///
///////////////////////////////////////////
//// http://adriansampson.net/blog/clangpass.html
//static void registerApolloFunctionCallChoosers(const PassManagerBuilder &,
//                 legacy::PassManagerBase &PM) {
//  PM.add(new ApolloFunctionCallChoosers());
//}
//static RegisterStandardPasses
//RegisterMyPass(PassManagerBuilder::EP_EarlyAsPossible,
//           registerApolloFunctionCallChoosers);

