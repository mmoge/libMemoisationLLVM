//===--- RenameLocalVariables.cpp --------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "RenameLocalVariables.h"
#include "Utils/FunctionUtils.h"
#include "Utils/MetadataHandler.h"
#include "Utils/LoopUtils.h"
#include "Utils/Utils.h"

#include "llvm/Pass.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Dominators.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Transforms/Utils/CodeExtractor.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/IRBuilder.h"

#include <set>

using namespace llvm;
using namespace apollo;

namespace {

/// \brief Erase the apollo_scop metadata from the instruction.
/// \return true if it removed metadata, false if not.
bool removePragmaInstr(Instruction *I) {
    const bool HasMetadataLocal = metadata::hasMetadataKind(I, mdkind::IsLocal);
    if (HasMetadataLocal)
      I->setMetadata(mdkind::IsLocal, 0x0);
    const bool HasMetadataShared = metadata::hasMetadataKind(I, mdkind::IsShared);
    if (HasMetadataShared)
      I->setMetadata(mdkind::IsShared, 0x0);
    const bool HasMetadataPrivate = metadata::hasMetadataKind(I, mdkind::IsPrivate);
    if (HasMetadataPrivate)
      I->setMetadata(mdkind::IsPrivate, 0x0);
  return ( HasMetadataLocal || HasMetadataShared || HasMetadataPrivate );
}

/// \brief For a function, erase the metadata from instructions inside it.
/// \param F a Function.
void cleanMetadataFunction(Function *F) {
  for (BasicBlock &block : *F) {
    for (Instruction &i : block) {
      removePragmaInstr(&i);
    }
  }
}


/// \brief get the private, shared and local variables names specified in the metadata apollo
/// \param F a Function.
/// \return a map containing a vector of names for each key in {isshared, isprivate, islocal}
std::map< std::string, std::vector<std::string> > getPrivateSharedLocalVarNames(Function *F) {
//WARNING: comment le faire pour chaque apollo loop séparément ?

    std::map< std::string, std::vector<std::string> > vars;
    vars["isprivate"] = std::vector<std::string>();
    vars["isshared"] = std::vector<std::string>();
    vars["islocal"] = std::vector<std::string>();
    for (BasicBlock &block : *F) {
      for (Instruction &I : block) {
            if (metadata::hasMetadataKind(&I, mdkind::apollo)) {
              if (metadata::hasMetadataKind(&I, mdkind::IsLocal)) {
                  const unsigned NumMdOperands =
                      metadata::getMetadataNumOperands(&I, mdkind::IsLocal);
                  MDNode *MdNode = I.getMetadata(mdkind::IsLocal);
                  for (unsigned it = 0;
                          it < NumMdOperands;
                          ++it) {
                      Metadata *MD = MdNode->getOperand(it);
                      MDString *MdStr = dyn_cast<MDString>(MD);
                      vars["islocal"].push_back(MdStr->getString().str());
                  }
              }
              if (metadata::hasMetadataKind(&I, mdkind::IsShared)) {
                  const unsigned NumMdOperands =
                      metadata::getMetadataNumOperands(&I, mdkind::IsShared);
                  MDNode *MdNode = I.getMetadata(mdkind::IsShared);
                  for (unsigned it = 0;
                          it < NumMdOperands;
                          ++it) {
                      Metadata *MD = MdNode->getOperand(it);
                      MDString *MdStr = dyn_cast<MDString>(MD);
                      vars["isshared"].push_back(MdStr->getString().str());
                  }
              }
              if (metadata::hasMetadataKind(&I, mdkind::IsPrivate)) {
                  const unsigned NumMdOperands =
                      metadata::getMetadataNumOperands(&I, mdkind::IsPrivate);
                  MDNode *MdNode = I.getMetadata(mdkind::IsPrivate);
                  for (unsigned it = 0;
                          it < NumMdOperands;
                          ++it) {
                      Metadata *MD = MdNode->getOperand(it);
                      MDString *MdStr = dyn_cast<MDString>(MD);
                      vars["isprivate"].push_back(MdStr->getString().str());
                  }
              }
              //we return after parsing the metadata of the first apollo statement encountered
              return vars;
            }
      }
    }
    return vars;
}

/// \brief replace the names of the var in originalNames with <name>_
///        apollo_dcop_private / <name>_apollo_dcop_local / <name>_apollo_dcop_shared
/// \param F a Function.
/// \param originalNames map containing the original names of the private/shared/local variables.
void renamePrivateSharedLocalVar(Function *F, std::map< std::string, std::vector< std::string > > originalNames)
{
    for (BasicBlock &BB : *F) {
      for (Instruction &I : BB) {
          AllocaInst* AI = dyn_cast<AllocaInst>(&I);
          if ( AI )
          {
              for ( std::vector< std::string >::iterator it = originalNames["isprivate"].begin();
                      it != originalNames["isprivate"].end();
                      ++it )
              {
                  if ( *it == AI->getName() )
                  {
                      AI->setName(*it+"_apollo_dcop_private");
                  }
              }
              for ( std::vector< std::string >::iterator it = originalNames["isshared"].begin();
                      it != originalNames["isshared"].end();
                      ++it )
              {
                  if ( *it == AI->getName() )
                  {
                      AI->setName(*it+"_apollo_dcop_shared");
                  }
              }
              for ( std::vector< std::string >::iterator it = originalNames["islocal"].begin();
                      it != originalNames["islocal"].end();
                      ++it )
              {
                  if ( *it == AI->getName() )
                  {
                      AI->setName(*it+"_apollo_dcop_local");
                  }
              }
          }
      }
    }
}

} // end anonymous namespace

namespace apollo {

RenameLocalVariables::RenameLocalVariables() : ModulePass(ID) {}

void RenameLocalVariables::getAnalysisUsage(AnalysisUsage &AU) const {
}

bool RenameLocalVariables::runOnModule(Module &M) {
  DEBUG_WITH_TYPE("apollo-pass-0",
			dbgs() << "Running pass on module: RenameLocalVariables\n");
  bool Modify = false;
  std::vector<Function *> Work;
  for (Function &F : M) {
    if (!F.getBasicBlockList().empty())
      Work.push_back(&F);
  }
  for (Function *F : Work) {
    if (runOnFunction(*F))
      Modify = true;
  }
  return Modify;
}

bool RenameLocalVariables::runOnFunction(Function &F) {
  DEBUG_WITH_TYPE("apollo-pass-1",
			dbgs() << "   function:" << F.getName().str() << "\n");

  std::map< std::string, std::vector< std::string > > dcop_var_names = getPrivateSharedLocalVarNames(&F);
  renamePrivateSharedLocalVar( &F, dcop_var_names );
  cleanMetadataFunction(&F);

  return false;
}

char RenameLocalVariables::ID = 0;
//DefinePassCreator(RenameLocalVariables);
// Register RenameLocalVariables in LLVM pass manager
// Required to use the pass with opt or clang
static RegisterPass<RenameLocalVariables> X("renamelocalvariables", "rename variables with metadata apollomem.pragma.local, appending _apollo_dcop_local to their name + the same with shared and private");

} // end namespace apollo

//namespace llvm {
//class PassRegistry;
//DeclarePassInitializator(RenameLocalVariables);
//} // end namespace llvm

//INITIALIZE_PASS_BEGIN(RenameLocalVariables, "APOLLO_LOOP_EXTRACT",
//                      "Apollo loop extract.", false, false)
//INITIALIZE_PASS_DEPENDENCY(DominatorTreeWrapperPass)
//INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
//INITIALIZE_PASS_END(RenameLocalVariables, "APOLLO_LOOP_EXTRACT",
//                    "Apollo loop extract.", false, false)
