//===--- ViUtils.cpp ------------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "ViUtils.h"
#include "LoopUtils.h"
#include "ConstantUtils.h"
#include "MetadataHandler.h"
#include "MetadataKind.h"

#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/IRBuilder.h"
#include <iostream>
using namespace llvm;

namespace apollo {
namespace ir {

Constant *ApolloIRBuilder::getInt(const long N, const short Bits) {
  Type *Ty = IntegerType::get(this->Context, Bits);
  return ConstantInt::get(Ty, N);
}

Value *
ApolloIRBuilder::createDotProductValue(const std::vector<Value *> &VecA,
                                       const std::vector<Value *> &VecB) {
  assert(VecA.size() == VecB.size());
  Value *Result = 0x0;
  if (!VecA.empty()) {
    std::vector<Value *> Adds;
    // Do all the multiplications
    for (unsigned i = 0; i < VecA.size(); ++i) {
      Adds.push_back(this->CreateMul(VecA[i], VecB[i]));
    }
    // Do binary reductions over all the coeficients.
    // On each iteration we reduce the size by a half.
    while (Adds.size() != 1) {
      std::vector<Value *> RAdds;
      for (unsigned i = 0; i < Adds.size() / 2; ++i) {
        RAdds.push_back(this->CreateAdd(Adds[2 * i], Adds[2 * i + 1]));
      }
      if (Adds.size() % 2 == 1)
        RAdds.push_back(*Adds.rbegin());
      Adds = RAdds;
    }
    Result = Adds[0];
  } else {
    Constant *Zero = this->getInt(0, 64);
    Result =
        this->CreateIntCast(Zero, Zero->getType(),
                            cast<IntegerType>(Zero->getType())->getSignBit());
  }
  return Result;
}

Value *ApolloIRBuilder::createMax(Value *ValA, Value *ValB) {
  return this->CreateSelect(this->CreateICmpSGE(ValA, ValB), ValA, ValB);
}

Value *ApolloIRBuilder::createMin(Value *ValA, Value *ValB) {
  return this->CreateSelect(this->CreateICmpSGE(ValA, ValB), ValB, ValA);
}

Instruction *ApolloIRBuilder::createDummy(Value *Val) {
  CastInst *Cast = CastInst::CreateTruncOrBitCast(Val, Val->getType());
  return this->Insert(Cast);
}

} // end namespace ir

namespace statement {

bool isStatement(Instruction *I) {
  return metadata::hasMetadataKind(I, mdkind::ApolloStatementId);
}

bool isMemAccess(Instruction *I) {
  return isa<LoadInst>(I) || isa<StoreInst>(I);
}
bool isStore(Instruction *I) {
  return isa<StoreInst>(I);
}
bool isStorePointerOperandLocal(Instruction *I) {
    StoreInst* SI = dyn_cast<StoreInst>(I);
    bool isLocalStore = false;
    if( SI )
    {
        isLocalStore = SI->getPointerOperand()->getName().str().find("_apollo_dcop_local")
                        != std::string::npos;
    }
    return isLocalStore;
}
bool isStorePointerOperandPrivate(Instruction *I) {
    StoreInst* SI = dyn_cast<StoreInst>(I);
    bool isPrivateStore = false;
    if( SI )
    {
        isPrivateStore = SI->getPointerOperand()->getName().str().find("_apollo_dcop_private")
                        != std::string::npos;
    }
    return isPrivateStore;
}
bool isStorePointerOperandShared(Instruction *I) {
    StoreInst* SI = dyn_cast<StoreInst>(I);
    bool isVarShared = false;
    if( SI )
    {
        isVarShared = SI->getPointerOperand()->getName().str().find("_apollo_dcop_shared")
                        != std::string::npos;
    }
    return isVarShared;
}

Value *getPointerOperand(Instruction *I) {
  assert(isMemAccess(I));
  LoadInst *Load = dyn_cast<LoadInst>(I);
  if (Load)
    return Load->getPointerOperand();
  return cast<StoreInst>(I)->getPointerOperand();
}

int getStatementId(Instruction *I) {
  assert(isStatement(I));
  ConstantInt *Id = dyn_cast<ConstantInt>(
      metadata::getMetadataOperand(I, mdkind::ApolloStatementId, 0));
  assert(Id);
  return Id->getSExtValue();
}

} // end namespace statement

namespace basicscalar {

bool isBasicScalar(Instruction *I) {
  return metadata::hasMetadataKind(I, mdkind::ApolloPhiId);
}

int getBasicScalarId(Instruction *I) {
  assert(isBasicScalar(I));
  ConstantInt *Id = dyn_cast<ConstantInt>(
      metadata::getMetadataOperand(I, mdkind::ApolloPhiId, 0));
  assert(Id);
  return Id->getSExtValue();
}

} // end namespace basicscalar

namespace vi {

AllocaInst *getLoopVi(Loop *L) {
  const auto LoopId = std::move(loop::getLoopId(L));
  for (BasicBlock &BB : *loop::getLoopParentFunction(L)) {
    for (Instruction &I : BB) {
      AllocaInst *Alloc = dyn_cast<AllocaInst>(&I);
      if (Alloc && metadata::hasMetadataKind(Alloc, mdkind::ApolloVi)) {
        ConstantInt *ConstLoopId = cast<ConstantInt>(
            metadata::getMetadataOperand(Alloc, mdkind::ApolloVi));

        if (ConstLoopId->getSExtValue() == LoopId)
          return Alloc;
      }
    }
  }
  assert(false && "Virtual iterator not found!");
  return 0x0;
}

LoadInst *getLoopViLoad(Loop *L) {
  AllocaInst *ViAlloc = getLoopVi(L);
  Instruction *FirstHeaderInstr = &*(L->getHeader()->begin());
  // If such load exists, return it and don't create a new one.
  LoadInst *FirstHeaderInstrLoad = dyn_cast<LoadInst>(FirstHeaderInstr);
  if (FirstHeaderInstrLoad &&
      FirstHeaderInstrLoad->getPointerOperand() == ViAlloc)
  {
      return FirstHeaderInstrLoad;
  }
  return new LoadInst(ViAlloc, ViAlloc->getName() + ".load", FirstHeaderInstr);
}

std::vector<LoadInst *> getNestViLoad(Loop *L) {
  if (loop::isOutermostLoop(L))
    return std::vector<LoadInst *>({getLoopViLoad(L)});
  auto ViVec = getNestViLoad(L->getParentLoop());
  ViVec.push_back(getLoopViLoad(L));
  return ViVec;
}

LoadInst * getOutermostViLoad(Loop *L) {
  if (loop::isOutermostLoop(L))
      return getLoopViLoad(L);
  else
  {
      LoadInst * li = getOutermostViLoad(L->getParentLoop());
      return li;
  }
}

} // end namespace vi
} // end namespace apollo
