//===--- ConstantUtils.cpp ------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "ConstantUtils.h"
#include "GlobalUtils.h"

#include "llvm/IR/Type.h"
#include "llvm/Support/raw_ostream.h"

using namespace llvm;

namespace apollo {
namespace constant {

Constant *getInt64Constant(LLVMContext &Context, const long Number) {
  Type *i64Ty = Type::getInt64Ty(Context);
  Constant *ConstTy = ConstantInt::get(i64Ty, Number);
  return ConstTy;
}

Constant *getInt32Constant(LLVMContext &Context, const int Number) {
  Type *i32Ty = Type::getInt32Ty(Context);
  Constant *ConstTy = ConstantInt::get(i32Ty, Number);
  return ConstTy;
}

Constant *getVecConstant(Module *M, std::vector<unsigned> &Values) {
  return getVecIntConstant<unsigned>(M, Values,
                                     Type::getInt64Ty(M->getContext()));
}

Constant *zero32(LLVMContext &Context) { 
  return getInt32Constant(Context, 0); 
}

Constant *one32(LLVMContext &Context) { 
  return getInt32Constant(Context, 1); 
}

Constant *zero64(LLVMContext &Context) { 
  return getInt64Constant(Context, 0); 
}

Constant *one64(LLVMContext &Context) { 
  return getInt64Constant(Context, 1); 
}

Constant *two64(LLVMContext &Context) { 
  return getInt64Constant(Context, 2); 
}

Constant *getStringConstant(Module *M, std::string &Str) {
  Type *i8Ty = Type::getInt8Ty(M->getContext());
  ArrayType *i8ArrayTy = ArrayType::get(i8Ty, Str.size() + 1);
  std::vector<Constant *> InitStr;

  for (char c : Str) {
    InitStr.push_back(ConstantInt::get(i8Ty, c));
  }

  InitStr.push_back(ConstantInt::get(i8Ty, 0)); // zero-terminatred
  Constant *Initializer = ConstantArray::get(i8ArrayTy, InitStr);
  GlobalVariable *GlobalVar =
      new GlobalVariable(*M, i8ArrayTy, true, GlobalVariable::PrivateLinkage,
                         Initializer, global::getUnnamedGlobal(M));
  Constant *Zero = zero32(M->getContext());
  Constant *GExp = ConstantExpr::getGetElementPtr(
      GlobalVar->getType()->getContainedType(0), GlobalVar,
      std::vector<Constant *>({Zero, Zero}));
  return GExp;
}

} // end namsepace constant
} // end namespace apollo
