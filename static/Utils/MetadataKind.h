//===--- MetadataKind.h ---------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef METADATA_KIND_H
#define METADATA_KIND_H

#define DefineMetadataKind(name) static const std::string name(#name)
#define DefineMetadataKindValue(name, value)                                   \
  static const std::string name(value)

namespace apollo {
namespace mdkind {

// Identifies the md comming from the frontend
DefineMetadataKindValue(apollo, "apollo.pragma");
// Identifies values that are affine
DefineMetadataKind(IsAffine);
DefineMetadataKind(IsAffineIterator);
DefineMetadataKind(IsAffineFromVerified);
DefineMetadataKind(NonLinearAtLevel);
// Indentifying linear equation for instruction
DefineMetadataKind(LinEq);
// Identifies the loop
DefineMetadataKind(LoopId);
// Identifies the parent loop
DefineMetadataKind(ParentLoop);
// Identifying phis and statements
DefineMetadataKind(ApolloPhiId);
DefineMetadataKind(ApolloStatementId);
// Vi alloc identifier
DefineMetadataKind(ApolloVi);
// Global parameter alloc.
DefineMetadataKind(ApolloParamAlloc);
// In which field of the state the phi-node is be stored.
DefineMetadataKind(ApolloPhiStateField);
// Identifyes the instrumentation condition
DefineMetadataKind(ApolloInstrumentationCondition);
// Identifies all the verification code.
DefineMetadataKind(Verification);
// Identifies things regarding original loop bounds verification
DefineMetadataKind(OriginalLoopBound);
// Identifies the values to verify for the broken phi-nodes.
DefineMetadataKind(BasicScalarInitial);
DefineMetadataKind(BasicScalarFinal);
DefineMetadataKind(BasicScalarLinearFunction);
// Identifies the alloc for the verification condition
DefineMetadataKind(VerificationCondition);


//// Identifies private variable of a loop (typically induction variables)
//DefineMetadataKindValue(isprivate, "apollo.pragma");
//DefineMetadataKind(IsPrivate);
DefineMetadataKindValue(IsLocal, "apollomem.pragma.local");
DefineMetadataKindValue(IsShared, "apollomem.pragma.shared");
DefineMetadataKindValue(IsPrivate, "apollomem.pragma.private");


} // end namecpace mdkind
} // end namecpace apollo

#endif
