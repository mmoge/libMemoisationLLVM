//===--- FunctionUtils.h --------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef APOLLO_FUNCTION_UTILS_H
#define APOLLO_FUNCTION_UTILS_H

#include "LoopUtils.h"

#include "llvm/Analysis/LoopInfo.h"

#include <string>
#include <vector>

namespace apollo {
namespace function {

/// \brief getNestId returns the nest Id of for a function.
/// \param F A function
/// \return The nest id for that function.
unsigned getNestId(llvm::Function *F);

/// \brief getSkeletonId returns the Id of the skeleton for a function.
/// \param F A function
/// \return The skeleton id for that function.
unsigned getSkeletonId(llvm::Function *F);

/// \brief Determines if a function is one of the exceptions allowed in the
///        loop nest.
/// \param F a function.
/// \return True if it is one of the exceptions.
bool allowedInNest(llvm::Function *F);

/// \brief Returns the first found outermost loop.
///        Used for finding the loop in apollo_loop functions.
/// \param aFunction a Function.
/// \param LI LoopInfo analysis.
/// \return The first encountered outermost loop. 0x0 if none.
llvm::Loop *getOutermostLoopInFunction(llvm::Function *F, llvm::LoopInfo *LI);

/// \brief Checks if F is an "apollo_loop" function.
///        Apollo loop function name have this shape:
///        'apollo_loop_%id'. %id is a number.
/// \param F a Function.
/// \return True if it is an "apollo_loop" function.
bool isApolloFunction(llvm::Function *F);

/// \brief Checks if F is an "apollo_loop" skeleton function.
/// \param F a Function.
/// \param skeletonName the name of the skeleton.
/// \return True if it is an "apollo_loop" skeleton function.
bool isApolloSkeletonFunction(llvm::Function *F, 
                              const std::string &SkeletonName);

/// \brief Checks if F is a verification skeleton function.
/// \param F a Function.
/// \return True if it is a verification skeleton function.
bool isApolloVerificationSkeletonFunction(llvm::Function *F);

/// \brief Returns the arguments of a function but in an std::vector.
/// \param F a Function.
/// \return Arguments of F in a std::vector.
std::vector<llvm::Value *> getArgumentVector(llvm::Function *F);

/// \brief Get argument with a given name.
/// \param F a function.
/// \param argName a name.
/// \return The argument or 0x0 if not found.
llvm::Argument *getArgumentByName(llvm::Function *F,
                                  const std::string &ArgName);

} // end namespace function
} // end namespace apollo

#endif
