//===--- ViUtils.h --------------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef APOLLO_VI_UTILS_H
#define APOLLO_VI_UTILS_H

#include "LoopUtils.h"
#include "ConstantUtils.h"

#include "llvm/IR/Instructions.h"
#include "llvm/IR/IRBuilder.h"

#include <string>

namespace apollo {
namespace ir {

class ApolloIRBuilder : public llvm::IRBuilder<> {

  llvm::LLVMContext &Context;

  template <typename T>
  std::vector<llvm::Value *> toValueVec(std::vector<T> &From) {
    std::vector<llvm::Value *> To;
    for (auto e : From) {
      To.push_back(e);
    }
    return To;
  }

  llvm::Value *createDotProductValue(const std::vector<llvm::Value *> &VecA,
                                     const std::vector<llvm::Value *> &VecB);

public:
  ApolloIRBuilder(llvm::Instruction *InsertAt)
      : llvm::IRBuilder<>(InsertAt), Context(InsertAt->getContext()) {}
  ApolloIRBuilder(llvm::BasicBlock *InsertAt)
      : llvm::IRBuilder<>(InsertAt), Context(InsertAt->getContext()) {}

  llvm::Instruction *createDummy(llvm::Value *V);
  template <typename TyA, typename TyB>
  llvm::Value *createDotProduct(std::vector<TyA> &A, std::vector<TyB> &B) {
    return createDotProductValue(toValueVec<TyA>(A), toValueVec<TyB>(B));
  }

  llvm::Value *createMax(llvm::Value *ValA, llvm::Value *ValB);
  llvm::Value *createMin(llvm::Value *ValA, llvm::Value *ValB);

  llvm::Constant *getInt(long N, short Bits);
};

} // end namespace ir

namespace statement {

/// \brief Identifies statements.
/// \param An instruction
/// \return True if the instruction is a statement
bool isStatement(llvm::Instruction *I);

/// \brief return true if the instruction is a memory access
/// \param an instruction
/// \return true if is a load or store
bool isMemAccess(llvm::Instruction *I);

/// \brief return true if the instruction is a store
/// \param an instruction
/// \return true if is a store
bool isStore(llvm::Instruction *I);

/// \brief return true if the instruction is a store and the pointer operand of the store is marked as _apollo_dcop_local
/// \param an instruction
/// \return true if is a store and the pointer operand of the store is marked as _apollo_dcop_local
bool isStorePointerOperandLocal(llvm::Instruction *I);

/// \brief return true if the instruction is a store and the pointer operand of the store is marked as _apollo_dcop_private
/// \param an instruction
/// \return true if is a store and the pointer operand of the store is marked as _apollo_dcop_private
bool isStorePointerOperandPrivate(llvm::Instruction *I);

/// \brief return true if the instruction is a store and the pointer operand of the store is marked as _apollo_dcop_shared
/// \param an instruction
/// \return true if is a store and the pointer operand of the store is marked as _apollo_dcop_shared
bool isStorePointerOperandShared(llvm::Instruction *I);

/// \brief from a memory access (load or store) get the pointer.
/// \param a memory access
/// \return the pointer operand
llvm::Value *getPointerOperand(llvm::Instruction *I);

/// \brief from a statement get its id.
/// \param a statement .
/// \return the id.
int getStatementId(llvm::Instruction *I);

} // end namespace statement

namespace basicscalar {

/// \brief Returns true if it is a phi-node.
/// \param I an instruction
/// \return true if it is a phi-node
bool isBasicScalar(llvm::Instruction *I);

int getBasicScalarId(llvm::Instruction *I);

} // end namespace basicscalar

namespace vi {

/// \brief Get loop virtual iterator.
/// \param L a loop.
/// \return The AllocaInst for the virtual iterator.
llvm::AllocaInst *getLoopVi(llvm::Loop *L);

/// \brief Get loop virtual iterator load.
/// \param L a loop.
/// \return The LoadInst just at the begining of the loop header for the
///         virtual iterator.
llvm::LoadInst *getLoopViLoad(llvm::Loop *L);
llvm::LoadInst *getOutermostViLoad(llvm::Loop *L);

/// \brief Get parent loop virtual iterator loads.
/// \param L a loop.
/// \return A vector of LoadInst with the parents vi loads,
///         from outermost to innermost.
std::vector<llvm::LoadInst *> getNestViLoad(llvm::Loop *L);

/// \brief Get the loop virtual iterator type.
/// \return A Type*.
template <class T> 
llvm::Type *getViType(T *tyA) {
  return llvm::Type::getInt64Ty(tyA->getContext());
}

} // end namespace vi
} // end namespace apollo

#endif
