//===--- SCEVUtils.cpp ----------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "SCEVUtils.h"
#include "LoopUtils.h"

#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Support/raw_ostream.h"

using namespace llvm;

namespace apollo {
namespace SCEVUtils {

/// \brief This function indicates whether or not a given "object"
/// (included in a SCEV) is an invariant regarding a given loop nest
/// \param SE the result of the scalar evolution pass result on
///        the function containing the "object" used to create the SCEV Scev
/// \param Scev the "object" to check if it is invariant or not
/// \param OutermostLoop the outermost loop containing the object to be checked
bool isLoopInvariant(ScalarEvolution *SE, const SCEV *Scev,
		Loop *OutermostLoop) {
	if (!SE->isLoopInvariant(Scev, OutermostLoop)) {
		return false;
	}
	for (Loop *subloop : *OutermostLoop) {
		if (!isLoopInvariant(SE, Scev, subloop)) {
			return false;
		}
	}
	return true;
}

/// \brief This function indicates whether or not a given "object"
/// (included in a SCEV) is an affine function of a given loop nest
/// \param SE the result of the scalar evolution pass result on
///        the function containing the "object" used to create the SCEV Scev
/// \param Scev the "object" to check if it is invariant or not
/// \param OutermostLoop the outermost loop containing the object to be checked
bool isAffine(ScalarEvolution *SE, const SCEV *Scev, Loop *OutermostLoop) {
	assert(
			apollo::loop::isOutermostLoop(OutermostLoop)
					&& "ERROR! The given loop is not the outermost loop of the nest");

	// A loop invariant is the simplest affine function we can have :)
	if (isLoopInvariant(SE, Scev, OutermostLoop)) {
		return true;
	}

	// If not invariant, we need to check for affine form.
	// This means the following conditions must be met:
	//   - The Scev object must be affine (i.e, affine in the deepest loop containing it)
	//   - The step of the Scev must be invariant in the loop nest (otherwise, we have "geometric" evolution)
	//   - The start value of the Scev must be affine in the loop nest
	//
	const SCEVAddRecExpr *AddRec = dyn_cast<SCEVAddRecExpr>(Scev);
	if (AddRec != nullptr) {
		bool isAffineInDeepestLoop = AddRec->isAffine();
		if (isAffineInDeepestLoop) {
			bool isStepInvariantInAllNest = isLoopInvariant(SE,
					AddRec->getStepRecurrence(*SE), OutermostLoop);
			if (isStepInvariantInAllNest) {
				bool isStartAffineInAllNest = isAffine(SE, AddRec->getStart(),
						OutermostLoop);
				if (isStartAffineInAllNest) {
					return true;
				}
			}
		}
	}
	return false;
}

Loop *getNonLinearLoopLevel(ScalarEvolution *SE, const SCEV *Scev,
		Loop *Outermost, Loop *Innermost) {
	assert(
			apollo::loop::isOutermostLoop(Outermost)
					&& "ERROR! Loop not found ");
	const SCEVAddRecExpr *AddRec = dyn_cast<SCEVAddRecExpr>(Scev);
	if (AddRec != nullptr) {
		if (AddRec->isAffine()
				&& SCEVUtils::isLoopInvariant(SE,
						AddRec->getStepRecurrence(*SE), Outermost))
			return getNonLinearLoopLevel(SE, AddRec->getStart(), Outermost,
					Innermost);
		else
			return (Loop *) AddRec->getLoop();
	}
	Loop *L = Innermost;
	while (L) {
		if (!SE->isLoopInvariant(Scev, L))
			return L;
		L = L->getParentLoop();
	}
	return nullptr;
}

} // end namespace SCEVUtils
} // end namsepace apollo
