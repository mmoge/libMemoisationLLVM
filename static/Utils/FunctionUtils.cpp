//===--- FunctionUtils.cpp ------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Utils.h"
#include "FunctionUtils.h"

#include "llvm/Support/Regex.h"

using namespace apollo;
using namespace llvm;

namespace apollo {
namespace function {

bool allowedInNest(Function *F) {
#define AddAllowed(F, exp)                                                     \
  if (F->getName().compare(exp) == 0)                                          \
  return true
#define AddAllowedStartingWith(F, exp)                                         \
  if (F->getName().startswith(exp))                                            \
  return true
  AddAllowedStartingWith(F, "apollo_");
// math.f functions. see http://en.wikipedia.org/wiki/C_mathematical_functions
#define AddAllowedMathI(F, exp)                                                \
  AddAllowed(F, exp);                                                          \
  AddAllowed(F, "l" exp);                                                      \
  AddAllowed(F, "ll" exp);
#define AddAllowedMathF(F, exp)                                                \
  AddAllowed(F, exp "f");                                                      \
  AddAllowed(F, exp);                                                          \
  AddAllowed(F, exp "l");

  AddAllowed(F, "usleep");
  AddAllowedMathI(F, "abs");
  AddAllowedMathI(F, "div");
  AddAllowedMathF(F, "fabs");
  AddAllowedMathF(F, "fmod");
  AddAllowedMathF(F, "remainder");
  AddAllowedMathF(F, "remquo");
  AddAllowedMathF(F, "fma");
  AddAllowedMathF(F, "fmax");
  AddAllowedMathF(F, "fmin");
  AddAllowedMathF(F, "fdim");
  AddAllowedMathF(F, "nan");
  AddAllowedMathF(F, "exp");
  AddAllowedMathF(F, "exp2");
  AddAllowedMathF(F, "expm1");
  AddAllowedMathF(F, "log");
  AddAllowedMathF(F, "log2");
  AddAllowedMathF(F, "log10");
  AddAllowedMathF(F, "log1p");
  AddAllowedMathF(F, "ilogb");
  AddAllowedMathF(F, "logb");
  AddAllowedMathF(F, "sqrt");
  AddAllowedMathF(F, "cbrt");
  AddAllowedMathF(F, "hypot");
  AddAllowedMathF(F, "pow");
  AddAllowedMathF(F, "sin");
  AddAllowedMathF(F, "cos");
  AddAllowedMathF(F, "tan");
  AddAllowedMathF(F, "asin");
  AddAllowedMathF(F, "acos");
  AddAllowedMathF(F, "atan");
  AddAllowedMathF(F, "atan2");
  AddAllowedMathF(F, "sinh");
  AddAllowedMathF(F, "cosh");
  AddAllowedMathF(F, "tanh");
  AddAllowedMathF(F, "asinh");
  AddAllowedMathF(F, "acosh");
  AddAllowedMathF(F, "atanh");
  AddAllowedMathF(F, "erf");
  AddAllowedMathF(F, "erfc");
  AddAllowedMathF(F, "lgamma");
  AddAllowedMathF(F, "tgamma");
  AddAllowedMathF(F, "ceil");
  AddAllowedMathF(F, "floor");
  AddAllowedMathF(F, "trunc");
  AddAllowedMathF(F, "round");
  AddAllowedMathF(F, "lround");
  AddAllowedMathF(F, "llround");
  AddAllowedMathF(F, "nearbyint");
  AddAllowedMathF(F, "rint");
  AddAllowedMathF(F, "lrint");
  AddAllowedMathF(F, "llrint");
  AddAllowedMathF(F, "frexp");
  AddAllowedMathF(F, "ldexp");
  AddAllowedMathF(F, "modf");
  AddAllowedMathF(F, "scalbn");
  AddAllowedMathF(F, "scalbln");
  AddAllowedMathF(F, "nextafter");
  AddAllowedMathF(F, "nexttoward");
  AddAllowedMathF(F, "copysign");
  AddAllowed(F, "rand");
  AddAllowed(F, "printf");
  AddAllowed(F, "dprintf");
  AddAllowed(F, "llvm.lifetime.start");
  AddAllowed(F, "llvm.lifetime.end");
  return false;
}

unsigned getNestId(Function *F) {
  Regex RegExp("^apollo_loop_[0-9]+_skeleton_[0-9]+");
  const std::string FunctionName = F->getName();
  unsigned NestId = 0;
  if (RegExp.match(FunctionName)) {
    unsigned Dummy = 0;
    sscanf(FunctionName.c_str(), "apollo_loop_%d_skeleton_%d", &NestId, &Dummy);
  } else if (isApolloFunction(F))
           sscanf(FunctionName.c_str(), "apollo_loop_%d", &NestId);
         else
           assert(false && "Wrong function name.");
  return NestId;
}

unsigned getSkeletonId(Function *F) {
  const std::string FunctionName = F->getName();
  Regex RegExp("^apollo_loop_[0-9]+_skeleton_[0-9]+");
  assert(RegExp.match(FunctionName));
  unsigned Dummy = 0;
  unsigned SkeletonId = 0;
  sscanf(FunctionName.c_str(), "apollo_loop_%d_skeleton_%d", &Dummy,
         &SkeletonId);
  return SkeletonId;
}

Loop *getOutermostLoopInFunction(Function *F, LoopInfo *LI) {
  for (BasicBlock &BB : *F) {
    Loop *L = LI->getLoopFor(&BB);
    if (L && loop::isOutermostLoop(L)) {
      return L;
    }
  }
  return 0x0;
}

bool isApolloFunction(Function *F) {
  const std::string FunctionName = F->getName();
  Regex RegExp("^apollo_loop_[0-9]+$");
  return RegExp.match(FunctionName);
}

bool isApolloSkeletonFunction(Function *F, 
                              const std::string &SkeletonName) {
  const std::string FunctionName = F->getName();
  std::string RegExpStr = "";
  if (SkeletonName.size()) {
    RegExpStr = utils::concat("^apollo_loop_[0-9]+_", SkeletonName, "$");
    Regex RegExp(RegExpStr);
    return RegExp.match(FunctionName);
  }
  return isApolloFunction(F);
}

bool isApolloVerificationSkeletonFunction(Function *F) {
  const std::string FunctionName = F->getName();
  std::string RegExpStr = "";
  RegExpStr = utils::concat("^apollo_loop_[0-9]+_skeleton_4", "$");
  Regex RegExp(RegExpStr);
  return RegExp.match(FunctionName);
}

std::vector<Value *> getArgumentVector(Function *F) {
  Function::ArgumentListType &ArgList = F->getArgumentList();
  std::vector<Value *> Args;
  for (Argument &arg : ArgList) {
    Args.push_back(&arg);
  }
  return Args;
}

Argument *getArgumentByName(Function *F, const std::string &ArgName) {
  Function::ArgumentListType &Arglist = F->getArgumentList();
  for (Argument &arg : Arglist) {
    if (arg.getName().equals(ArgName))
      return &arg;
  }
  return 0x0;
}

} // end namespace function
} // end namespace apollo
