//===--- ParallelizeLoopCalls.cpp --------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "ParallelizeLoopCalls.h"
#include "ParallelUtils.h"
#include "SkeletonConfig.h"
#include "Utils/FunctionUtils.h"
#include "Utils/MetadataHandler.h"
#include "Utils/Utils.h"

#include "llvm/Pass.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/IRBuilder.h"

#include <set>

using namespace llvm;
using namespace apollo;

namespace {

Constant* getLoopId(Function* F)
{
    StringRef name = F->getName();
    std::string namestr = name.str();
    if ( namestr.substr(0,12) == std::string("apollo_loop_") )
    {
        std::string loopidstr = namestr.substr(12,namestr.find_first_not_of("0123456789",12)-1);
        Type *i64Ty = Type::getInt64Ty(F->getContext());
        Constant *loopid = ConstantInt::get(i64Ty, stoi(loopidstr));
        return loopid;
    }
    else
    {
        std::cerr << "ERROR: function " << namestr << " is not a valid apollo loop !" << std::endl;
        return nullptr;
    }
}


} // end anonymous namespace

namespace apollo {

ParallelizeLoopCalls::ParallelizeLoopCalls() :
        ApolloPassOnSkeleton(ID, skeleton::sk_name<skeleton::parallel>(), "ParallelizeLoopCalls")
{
}

void ParallelizeLoopCalls::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<LoopInfoWrapperPass>();
}

/// \brief in function apollo_loop<N>_skeleton_4_parallel_loop (i.e. parallel loop of the parallel version of the apollo loop)
///     replace uses of all parameters <name>_apollo_dcop_private with newly created local variables
/// \param apolloFunction
bool ParallelizeLoopCalls::runOnApollo(Function *F) {
    //
    // 0 - Initialize constant, types, etc
    //
    LLVMContext& C = F->getContext();
    Module *M = F->getParent();
    Type *i32Ty = Type::getInt32Ty(C);
    Type *i32Ptr = Type::getInt32PtrTy(C);
    Type *voidTy = Type::getVoidTy(C);
    Type *voidPtrTy = Type::getInt8PtrTy(C);
    Constant *zero   = ConstantInt::get(i32Ty, 0, false);
    Constant *one    = ConstantInt::get(i32Ty, 1, false);
    Constant *loopid = getLoopId(F);
    //
    // 1 - get the _parallel_loop function
    //
    Function* parallelLoopFunc = nullptr;
    CallInst* parallelLoopFuncCall = nullptr;
    for (BasicBlock &BB : *F) {
        for (Instruction &I : BB) {
            CallInst* CI = dyn_cast<CallInst>(&I);
            if ( CI )
            {
                if ( CI->getCalledFunction()->getName().str() == F->getName().str()+"_parallel_loop" )
                {
                    parallelLoopFuncCall = CI;
                    parallelLoopFunc = CI->getCalledFunction();
                    break;
                }
            }
        }
    }
    FunctionType* functype = parallelLoopFunc->getFunctionType();
    // get the block that calls parallelLoopFunc
    BasicBlock * innerLoopCallingBlock = parallelLoopFuncCall->getParent();
    // get the operands of the call and their type
    std::vector<Value *> ParamsExtracted;
    std::vector<Type *> ParamsExtractedType;
    unsigned int nbOperands = parallelLoopFuncCall->getNumArgOperands();
    for ( unsigned int i = 0; i < nbOperands; ++i )
    {
        ParamsExtracted.push_back(parallelLoopFuncCall->getArgOperand(i));
        ParamsExtractedType.push_back(parallelLoopFuncCall->getArgOperand(i)->getType());
    }
    //
    // 2 - replace innerLoopCallingBlock with a new block that calls the parallelizing function
    //     it is the parallelizing function that will call apollo_loop_<N>_skeleton_4_inner_loop
    //     in parallel with modified arguments
    //
    BasicBlock* innerLoopParallelizingBlock = BasicBlock::Create(
            C, "inner.loop.parallel", F, innerLoopCallingBlock );
    BranchInst::Create(innerLoopCallingBlock->getUniqueSuccessor(), innerLoopParallelizingBlock);
    innerLoopCallingBlock->replaceAllUsesWith(innerLoopParallelizingBlock);
    innerLoopCallingBlock->dropAllReferences();
    innerLoopCallingBlock->eraseFromParent();
    //
    //  I try to do something similar to CodeBonesCodeGeneration::visitClastParallelFor in apollo
    //
    IRBuilder<> innerLoopParallelizingBlockBuilder( innerLoopParallelizingBlock->getTerminator() );
    unsigned int nbParams = functype->getNumParams();
    ArrayRef< Type* > paramsType = functype->params();
    // We know the last 3 parameters of function parallelLoopFuncCall : colorfirst, colorlast, itsarray
    // but the other parameters are dependent on the body of the inner loop
    // so we encapsulate them in a struct, and pass it as a void* to the parallelizing function
    std::vector< Type* > unknownParamsType;
    std::vector< Value* > unknownParams;
    for ( unsigned int i=0; i<nbParams-3; ++i )
    {
        unknownParamsType.push_back( paramsType[i] );
        unknownParams.push_back( ParamsExtracted[i] );
    }
    //
    StructType *SymbolsStructTy = StructType::create(unknownParamsType);
    AllocaInst *StructAlloc =
            innerLoopParallelizingBlockBuilder.CreateAlloca(SymbolsStructTy, 0, "packed_symbols");
    int SymbolIdx = 0;
    std::vector< LoadInst* > newLoadedParams;
    for (auto &paramValue : unknownParams) {
      if (!isa<Function>(paramValue)) {
        llvm::Value *StructField = innerLoopParallelizingBlockBuilder.CreateStructGEP(
            cast<PointerType>(StructAlloc->getType()->getScalarType())
                ->getElementType(),
            StructAlloc, SymbolIdx);
        newLoadedParams.push_back(innerLoopParallelizingBlockBuilder.CreateLoad(cast<PointerType>(paramValue->getType())->getElementType(), paramValue));
        innerLoopParallelizingBlockBuilder.CreateStore(newLoadedParams.back()->getPointerOperand(), StructField);
        SymbolIdx++;
      }
    }
    //
    // 3 - call parallelizing function
    //
    // We need to modify Function parallelLoopFuncCall so that it takes a void* as argument
    // and then cast it back to the "real" argument types originalParamsType
    //
    FunctionType *ft = FunctionType::get(voidTy, {SymbolsStructTy->getPointerTo(), i32Ty, i32Ty, i32Ptr}, false);
    Function* GenericInnerLoop = Function::Create( ft, Function::ExternalLinkage, utils::concat(parallelLoopFuncCall->getName(), ".generic.func"), M);

    BasicBlock *Entry = BasicBlock::Create(C, "entry", GenericInnerLoop);
    BasicBlock *CallInnerLoopFunc = BasicBlock::Create(C, "call_inner_loop_func", GenericInnerLoop);
    BasicBlock *Exit = BasicBlock::Create(C, "exit", GenericInnerLoop);
    BranchInst::Create(CallInnerLoopFunc, Entry);
    BranchInst::Create(Exit, CallInnerLoopFunc);
    ReturnInst::Create(C, Exit);
    // Get the arguments
    llvm::Argument *symbolsStructArg;
    llvm::Argument *colorFirstArg;
    llvm::Argument *colorLastArg;
    llvm::Argument *itsArrayArg;
    auto argument_it = GenericInnerLoop->arg_begin();
    symbolsStructArg = &*argument_it++;
    colorFirstArg = &*argument_it++;
    colorLastArg = &*argument_it++;
    itsArrayArg = &*argument_it;
    symbolsStructArg->setName("symbolsstruct");
    colorFirstArg->setName("colorfirst");
    colorLastArg->setName("colorlast");
    itsArrayArg->setName("itsarray");
    // Reload the symbols
    std::vector< Value* > NewSymbols;
    IRBuilder<> EntryBuilder(Entry->getTerminator());
    SymbolIdx = 0;
    for (auto &symbol : unknownParams) {
      if (!isa<Function>(symbol)) {
          llvm::Value *Ptr = EntryBuilder.CreateStructGEP(
              cast<PointerType>(symbolsStructArg->getType()->getScalarType())
                  ->getElementType(),
              symbolsStructArg, SymbolIdx);
          NewSymbols.push_back(EntryBuilder.CreateLoad(Ptr));
          SymbolIdx++;
      } else {
          NewSymbols.push_back(symbol);
      }
    }
    // Call inner loop function parallelLoopFuncCall
    NewSymbols.push_back( colorFirstArg );
    NewSymbols.push_back( colorLastArg );
    NewSymbols.push_back( itsArrayArg );
    IRBuilder<> CallInnerLoopFuncBuilder(CallInnerLoopFunc->getTerminator());
    CallInst* innerLoopFuncCall = CallInnerLoopFuncBuilder.CreateCall(parallelLoopFunc, NewSymbols);

    Function *GetRunInParallel = getApolloRunInParallel(M);
    llvm::Value *StructCast = innerLoopParallelizingBlockBuilder.CreateBitCast(
        StructAlloc, voidPtrTy);
    llvm::Value *FuncCast = innerLoopParallelizingBlockBuilder.CreateBitCast(
            GenericInnerLoop, voidPtrTy);
    innerLoopParallelizingBlockBuilder.CreateCall(GetRunInParallel, {
            StructCast, ParamsExtracted[nbParams-3], ParamsExtracted[nbParams-2], ParamsExtracted[nbParams-1], FuncCast} );
    return true;
}

char ParallelizeLoopCalls::ID = 0;
//DefinePassCreator(ApolloLoopExtractStrictly);
// Register ApolloLoopExtractStrictly in LLVM pass manager
// Required to use the pass with opt or clang
static RegisterPass<ParallelizeLoopCalls> X("parallelizeloopcalls", "calls the parallel loop via calls to runtime function run_in_parallel to use openMP");

} // end namespace apollo

//namespace llvm {
//class PassRegistry;
//DeclarePassInitializator(ApolloLoopExtractStrictly);
//} // end namespace llvm

//INITIALIZE_PASS_BEGIN(ApolloLoopExtractStrictly, "APOLLO_LOOP_EXTRACT",
//                      "Apollo loop extract.", false, false)
//INITIALIZE_PASS_DEPENDENCY(DominatorTreeWrapperPass)
//INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
//INITIALIZE_PASS_END(ApolloLoopExtractStrictly, "APOLLO_LOOP_EXTRACT",
//                    "Apollo loop extract.", false, false)
