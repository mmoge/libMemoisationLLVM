//===--- FixParallelLoopFunction.cpp --------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "SkeletonConfig.h"
#include "Utils/Utils.h"
#include "Utils/FunctionUtils.h"
#include "Utils/ConstantUtils.h"
#include "Utils/MetadataHandler.h"
#include "FixParallelLoopFunction.h"
#include "GetInductionVariable.h"

#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Analysis/ScalarEvolutionExpander.h"
#include "llvm/Analysis/ScalarEvolutionExpressions.h"
#include "llvm/IR/Verifier.h"
 #include "llvm/IR/Dominators.h"


using namespace apollo;
using namespace llvm;

namespace {


} // end anonymous namespace

namespace apollo {

FixParallelLoopFunction::FixParallelLoopFunction() :
        pass::ApolloPassOnSkeleton(ID,
                skeleton::sk_name<skeleton::parallel>(),
                "FixParallelLoopFunction") {
}

void FixParallelLoopFunction::getAnalysisUsage(AnalysisUsage &AU) const {
    ApolloPassOnSkeleton::getAnalysisUsage(AU);
    AU.addRequired<LoopInfoWrapperPass>();
    AU.addRequired<ScalarEvolutionWrapperPass>();
}

bool FixParallelLoopFunction::runOnApollo( Function *F ) {
    LLVMContext& C = F->getContext();
    Module *M = F->getParent();
    Type *i32Ty = Type::getInt32Ty(C);
    Type *i32PtrTy = Type::getInt32PtrTy(C);
    Constant *zero   = ConstantInt::get(i32Ty, 0, false);
    Constant *one    = ConstantInt::get(i32Ty, 1, false);
    //
    // 1 - get the function _parallel_loop
    //
    Function* parallelLoopFunc = nullptr;
    CallInst* parallelLoopFuncCallInst = nullptr;
    for ( BasicBlock& BB : *F )
    {
        if ( BB.getName().str() == "inner.loop.parallel" )
        {
            for ( Instruction& I : BB )
            {
                CallInst* CI = dyn_cast<CallInst>(&I);
                if( CI )
                {
                    parallelLoopFunc = CI->getCalledFunction();
                    parallelLoopFuncCallInst = CI;
                    break;
                }
            }
        }
    }
    //
    // 2 - identify the induction variable of the parallel loop (outer loop)
    //
    LoopInfo &LI = getAnalysis<LoopInfoWrapperPass>(*parallelLoopFunc).getLoopInfo();
    ScalarEvolutionWrapperPass &SeWrapper = getAnalysis<ScalarEvolutionWrapperPass>(*parallelLoopFunc);
    ScalarEvolution &SE = SeWrapper.getSE();
    Loop *L = function::getOutermostLoopInFunction(parallelLoopFunc, &LI);
    assert(L && "parallelLoopFunc: Loop not found!");
    //identify parallel loop latch
    //WARNING: only works if there is a single latch block for this loop
    BasicBlock* ParallelLoopLatchBlock = L->getLoopLatch();
    //identify parallel loop header
    BasicBlock* ParallelLoopHeaderBlock = nullptr;
    for ( BasicBlock& BB : *parallelLoopFunc )
    {
        if ( LI.isLoopHeader(&BB) )
        {
            ParallelLoopHeaderBlock = &BB;
            break;
        }
    }
    BasicBlock* ParallelLoopExitBlock = L->getUniqueExitBlock();
    BasicBlock* ParallelLoopPreheaderBlock = L->getLoopPreheader();
    // identify the Induction Variable
    //TODO: use getInductionVariable - my current solution is valid only in some simple unoptimized cases
//    PHINode* oldIV = getInductionVariable( L, &SE );
    Value* oldIV = nullptr;
    for ( Instruction& I : *ParallelLoopLatchBlock )
    {
        LoadInst* LI = dyn_cast<LoadInst>(&I);
        if( LI )
        {
            oldIV = LI->getPointerOperand();
            break;
        }
    }
    //
    // 3 -
    // 3.1 - load the new args and create new induction
    //
    llvm::Argument *color_first_it;
    llvm::Argument *color_last_it;
    llvm::Argument *colored_its_array;
    auto argument_it = parallelLoopFunc->arg_end();
    colored_its_array = &*--argument_it;
    color_last_it = &*--argument_it;
    color_first_it = &*--argument_it;
    //create induction var in preheader
    IRBuilder<> ParallelLoopPreheaderBlockBuilder ( ParallelLoopPreheaderBlock->getTerminator() );
    Value* NewOuterIV =
            ParallelLoopPreheaderBlockBuilder.CreateAlloca(i32Ty, one, "vi_color_loop");
    ParallelLoopPreheaderBlockBuilder.CreateStore(color_first_it, NewOuterIV);
    //
    // 3.2 -replace header block of parallel loop using the new arguments color_first_it, color_last_it, colored_its_array
    //
    BasicBlock* NewOuterHeader =
            BasicBlock::Create( C, "color.loop.cond", parallelLoopFunc, ParallelLoopHeaderBlock );
    IRBuilder<> NewOuterHeaderBuilder ( NewOuterHeader );
    //load induction var and its_array[induction var] + do comparison
    Value* newIV = NewOuterHeaderBuilder.CreateLoad ( i32Ty, NewOuterIV, "outeriv" );
    Value* currenteltgep =
            NewOuterHeaderBuilder.CreateGEP(i32Ty, colored_its_array, newIV,"currenteltaddr");
    Value* currenteltidx =
            NewOuterHeaderBuilder.CreateLoad(i32Ty, currenteltgep,"currenteltidx");
    Value* NewOuterLoopCond =
            NewOuterHeaderBuilder.CreateICmpSLE(newIV,color_last_it);
    //create branch instruction using the successors of the original loop header
    BranchInst* NewOuterLoopCondBI =
            NewOuterHeaderBuilder.CreateCondBr(NewOuterLoopCond,
                    ParallelLoopHeaderBlock->getTerminator()->getSuccessor(0),
                    ParallelLoopHeaderBlock->getTerminator()->getSuccessor(1) );
    //replace old header block with the new one
    ParallelLoopHeaderBlock->replaceAllUsesWith(NewOuterHeader);
    //erase old header block
    ParallelLoopHeaderBlock->eraseFromParent();
    //
    // 3.3 - replace inc block (latch) with new inc block for newIV
    //
    // increment newIV in loop latch
    BasicBlock* NewOuterLatch = BasicBlock::Create( C, "color.loop.inc", parallelLoopFunc, ParallelLoopExitBlock );
    IRBuilder<> NewOuterLatchBuilder ( NewOuterLatch );
    newIV = NewOuterLatchBuilder.CreateLoad ( i32Ty, NewOuterIV );
    Value* newIVinc = NewOuterLatchBuilder.CreateAdd(newIV, one);
    NewOuterLatchBuilder.CreateStore(newIVinc, NewOuterIV);
    BranchInst* newOuterLoopLatchBI = NewOuterLatchBuilder.CreateBr(NewOuterHeader);
    //replace old inc block with the new one
    ParallelLoopLatchBlock->replaceAllUsesWith(NewOuterLatch);
    //erase old inc block
    ParallelLoopLatchBlock->eraseFromParent();
    //
    // 3.4 - at the beginning of body, store its_array[newIV], i.e. currenteltidx (or currenteltgep ?), in oldIV
    //
    BasicBlock* ParallelLoopBody = NewOuterHeader->getTerminator()->getSuccessor(0);
    IRBuilder<> ParallelLoopBodyBuilder ( ParallelLoopBody->getFirstNonPHI () );
    ParallelLoopBodyBuilder.CreateStore( currenteltidx, oldIV );


    //  PRINT //
//    parallelLoopFunc->getEntryBlock().print(outs());
//    NewOuterHeader->print(outs());
//    NewOuterLatch->print(outs());
    //

    return true;
}

char FixParallelLoopFunction::ID = 0;
static RegisterPass<FixParallelLoopFunction> X("fixparallelloopfunction", "FixParallelLoopFunction");

//DefinePassCreator(FixParallelLoopFunction);

} // end namespace apollo

//namespace llvm {
//class PassRegistry;
//DeclarePassInitializator(FixParallelLoopFunction);
//} // end namespace llvm
//
//INITIALIZE_PASS_BEGIN(FixParallelLoopFunction, "APOLLO_PHI_HANDLING",
//		"APOLLO_PHI_HANDLING", false, false)
//	INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
//	INITIALIZE_PASS_END(FixParallelLoopFunction, "APOLLO_PHI_HANDLING",
//			"APOLLO_PHI_HANDLING", false, false)
