//===--- ApolloMemAccess.cpp ----------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "ApolloInstrumentMemAccess.h"
#include "SkeletonConfig.h"
#include "InstrumentationConfig.h"
#include "MetadataKind.h"
#include "InstrumentationUtils.h"
#include "Utils/FunctionUtils.h"
#include "Utils/LoopUtils.h"
#include "Utils/ViUtils.h"
#include "Utils/ConstantUtils.h"
#include "Utils/Utils.h"
#include "Utils/MetadataHandler.h"

#include "llvm/IR/Module.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Metadata.h"
#include "llvm/IR/TypeBuilder.h"

using namespace llvm;
using namespace apollo;

namespace {

Constant* getLoopId(Function* F)
{
    StringRef name = F->getName();
    std::string namestr = name.str();
    if ( namestr.substr(0,12) == std::string("apollo_loop_") )
    {
        std::string loopidstr = namestr.substr(12,namestr.find_first_not_of("0123456789",12)-1);
        Type *i64Ty = Type::getInt64Ty(F->getContext());
        Constant *loopid = ConstantInt::get(i64Ty, stoi(loopidstr));
        return loopid;
    }
    else
    {
        std::cerr << "ERROR: function " << namestr << " is not a valid apollo loop !" << std::endl;
        return nullptr;
    }
}
/// \brief CreateRegisterBlock Creates the block in which the stmt registration
///        will go.
/// \param Block
/// \param ParentLoop
/// \return The newly created block for the statement registration.
BasicBlock *createRegisterBlock(BasicBlock &BB, Loop *ParentLoop) {
	BasicBlock *BlockCont = BB.splitBasicBlock(BB.getTerminator(),
			BB.getName() + ".cont");
	BasicBlock *RegisterBlock = BasicBlock::Create(BB.getContext(),
			BB.getName() + ".instrument", BB.getParent(), BlockCont);
	BB.getTerminator()->eraseFromParent();
    BranchInst::Create(RegisterBlock, &BB);
    BranchInst::Create(BlockCont, RegisterBlock);
	return RegisterBlock;
}

/// \brief InstrumentSingleAccess Creates the register function for one stmt.
void instrumentSingleAccess(Instruction &Stmt, Loop *ParentLoop,
		IRBuilder<> &Builder) {
    Type *i64Ty = Type::getInt64Ty(Stmt.getContext());
    Value * Vis = Builder.CreatePtrToInt(vi::getOutermostViLoad(ParentLoop),
                i64Ty);
	Value *Ptr = Builder.CreatePtrToInt(statement::getPointerOperand(&Stmt),
			i64Ty);
	std::vector<Value *> Params;
    Params.push_back(metadata::getMetadataOperand(&Stmt, mdkind::LoopId));
	Params.push_back(Ptr);
	Params.push_back(Vis);
	Module *M = Stmt.getParent()->getParent()->getParent();
	Function *RegFun = getApolloRegisterStore(M);
	  Builder.CreateCall(RegFun, Params);
}

/// \brief InstrumentAccessesInBlock This function creates a registration block
///        and registers all the memory accesses for 'block' in it.
void instrumentAccessesInBlock(BasicBlock &BB, LoopInfo &LI) {
	std::vector<Instruction *> Stmts;
	for (Instruction &stmt : BB) {
		if (statement::isStatement(&stmt)) {
			Stmts.push_back(&stmt);
		}
	}
	if (!Stmts.empty()) {
		Loop *ParentLoop = LI.getLoopFor(&BB);
		BasicBlock *RegisterBlock = createRegisterBlock(BB, ParentLoop);
		IRBuilder<> Builder(RegisterBlock->getTerminator());
		for (Instruction *stmt : Stmts) {
			instrumentSingleAccess(*stmt, ParentLoop, Builder);
		}
	}
}

} // end anonymous namespace

namespace apollo {

ApolloInstrumentMemAccess::ApolloInstrumentMemAccess() :
		pass::ApolloPassOnSkeleton(ID,
				skeleton::sk_name<skeleton::instrumentation>(),
				"ApolloInstrumentMemAccess") {
}

void ApolloInstrumentMemAccess::getAnalysisUsage(AnalysisUsage &AU) const {
	AU.addRequired<LoopInfoWrapperPass>();
}

bool ApolloInstrumentMemAccess::runOnLoopLI(Loop *L, LoopInfo *LI) {
	std::vector<BasicBlock *> LoopBlocks = L->getBlocks();
	for (BasicBlock *block : LoopBlocks) {
		instrumentAccessesInBlock(*block, *LI);
	}
	// add a call to create_and_color_dependency_graph at the end of the apollo loop instrumented version
    Function* F = LoopBlocks.back()->getParent();
    Constant *loopid = getLoopId(F);
    std::vector<Value *> Params;
    Params.push_back(loopid);
    Module *M = F->getParent();
    Function *CreateAndColorDependencyGraph = getApolloCreateAndColorDependencyGraph(M);
    for ( BasicBlock& BB : *F )
    {
        for ( Instruction& I : BB )
        {
            if (ReturnInst *RI = dyn_cast<ReturnInst>(&I))
            {
                IRBuilder<> exitBlockBuilder( RI );
                exitBlockBuilder.CreateCall( CreateAndColorDependencyGraph, Params );
            }
        }
    }

	return true;
}

char ApolloInstrumentMemAccess::ID = 0;
//DefinePassCreator(ApolloInstrumentMemAccess);
static RegisterPass<ApolloInstrumentMemAccess> ApolloInstrumentMemAccess("instrumentmemaccess","Pass that Instruments MemAccess");


} // end namespace apollo

//namespace llvm {
//class PassRegistry;
//DeclarePassInitializator(ApolloInstrumentMemAccess);
//} // end namespace llvm
//
//INITIALIZE_PASS_BEGIN(ApolloInstrumentMemAccess, "APOLLO_INSTRUMENT_MEM_ACCESS",
//		"APOLLO_INSTRUMENT_MEM_ACCESS", false, false)
//	INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
//	INITIALIZE_PASS_END(ApolloInstrumentMemAccess,
//			"APOLLO_INSTRUMENT_MEM_ACCESS", "APOLLO_INSTRUMENT_MEM_ACCESS",
//			false, false)

