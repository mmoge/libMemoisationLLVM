//===--- ParallelLoopMakeVariablesLocal.cpp --------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "ParallelLoopMakeVariablesLocal.h"

#include "SkeletonConfig.h"
#include "Utils/FunctionUtils.h"
#include "Utils/MetadataHandler.h"
#include "Utils/Utils.h"

#include "llvm/Pass.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/IRBuilder.h"

#include <set>

using namespace llvm;
using namespace apollo;

namespace {

} // end anonymous namespace

namespace apollo {

ParallelLoopMakeVariablesLocal::ParallelLoopMakeVariablesLocal() :
        ApolloPassOnSkeleton(ID, skeleton::sk_name<skeleton::parallel>(), "ParallelLoopMakeVariablesLocal")
{
}

void ParallelLoopMakeVariablesLocal::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<LoopInfoWrapperPass>();
}

/// \brief in function apollo_loop<N>_skeleton_4_parallel_loop (i.e. parallel loop of the parallel version of the apollo loop)
///     replace uses of all parameters <name>_apollo_dcop_local with newly created local variables
/// \param apolloFunction
bool ParallelLoopMakeVariablesLocal::runOnApollo(Function *apolloFunction) {
    Type *i32Ty = Type::getInt32Ty(apolloFunction->getContext());
    Type *i64Ty = Type::getInt64Ty(apolloFunction->getContext());
    Constant *one    = ConstantInt::get(i32Ty, 1, false);
    //
    // get the _parallel_loop function
    //
    Function* parallelLoopFunc = nullptr;
    for (BasicBlock &BB : *apolloFunction) {
        for (Instruction &I : BB) {
            CallInst* CI = dyn_cast<CallInst>(&I);
            if ( CI )
            {
                if ( CI->getCalledFunction()->getName().str() == apolloFunction->getName().str()+"_parallel_loop" )
                {
                    parallelLoopFunc = CI->getCalledFunction();
                    break;
                }
            }
        }
    }
    //
    // check if parallelLoopFunc has local parameters
    //
    std::vector < llvm::Argument* > localArguments;
    for ( auto argument_it = parallelLoopFunc->arg_begin();
            argument_it != parallelLoopFunc->arg_end();
            argument_it++ )
    {
        if ( argument_it->getName().str().find("_apollo_dcop_local")
                != std::string::npos )
        {
            localArguments.push_back(&*argument_it);
        }
    }
    //
    // privatize all local parameters of parallelLoopFunc
    //
    BasicBlock& entryBlock = parallelLoopFunc->getEntryBlock();
    IRBuilder<> entryBlockBuilder( entryBlock.getTerminator() );
    for ( auto local_argument_it = localArguments.begin();
            local_argument_it != localArguments.end();
            local_argument_it++ )
    {
        Type *argTy = (*local_argument_it)->getType()->getPointerElementType();

        AllocaInst* newPrivateVar = nullptr;
        // if integer, float, or double, initialize to zero
        // and replace uses of *local_argument_it with newPrivateVar
        if ( argTy->isIntegerTy() )
        {
            newPrivateVar = entryBlockBuilder.CreateAlloca( argTy, one, (*local_argument_it)->getName().str()+"_looplocal" );
            Constant *zero = ConstantInt::get(argTy, 0, false);
            entryBlockBuilder.CreateStore( zero, newPrivateVar );
            (*local_argument_it)->replaceAllUsesWith(newPrivateVar);
        }
        else if ( argTy->isFloatTy() || argTy->isDoubleTy() )
        {
            newPrivateVar = entryBlockBuilder.CreateAlloca( argTy, one, (*local_argument_it)->getName().str()+"_looplocal" );
            Constant *zero = ConstantFP::get(argTy, 0.);
            entryBlockBuilder.CreateStore( zero, newPrivateVar );
            (*local_argument_it)->replaceAllUsesWith(newPrivateVar);
        }
        else if ( argTy->isArrayTy() )
        {
            newPrivateVar = entryBlockBuilder.CreateAlloca( argTy, one, (*local_argument_it)->getName().str()+"_looplocal" );
            (*local_argument_it)->replaceAllUsesWith(newPrivateVar);
        }
        else if ( argTy->isVectorTy() )
        {
            newPrivateVar = entryBlockBuilder.CreateAlloca( argTy, one, (*local_argument_it)->getName().str()+"_looplocal" );
            (*local_argument_it)->replaceAllUsesWith(newPrivateVar);
        }
        else if ( argTy->isPointerTy() )
        {
            PointerType* ptrArgTy = dyn_cast<PointerType>(argTy);
            AllocaInst* newPrivateVarPointee = entryBlockBuilder.CreateAlloca( ptrArgTy->getElementType(), one );
            newPrivateVar = entryBlockBuilder.CreateAlloca( ptrArgTy, one, (*local_argument_it)->getName().str()+"_looplocal" );
            entryBlockBuilder.CreateStore( newPrivateVarPointee, newPrivateVar );
            (*local_argument_it)->replaceAllUsesWith(newPrivateVar);
        }
        // else, ERROR
        else
        {
            std::cerr << "ERROR: only integers or scalars can be local variables in pragma apollo dcop !" << std::endl;
            std::cerr << "    variable " << (*local_argument_it)->getName().str() << " is of type ";
            argTy->print(errs());
            std::cerr << std::endl;
        }
    }

    return true;
}

char ParallelLoopMakeVariablesLocal::ID = 0;
//DefinePassCreator(ApolloLoopExtractStrictly);
// Register ApolloLoopExtractStrictly in LLVM pass manager
// Required to use the pass with opt or clang
static RegisterPass<ParallelLoopMakeVariablesLocal> X("loopmakevariableslocal", "makes the variables marked as local in the pragma dcop local to the apollo_loop_<N> functions");

} // end namespace apollo

//namespace llvm {
//class PassRegistry;
//DeclarePassInitializator(ApolloLoopExtractStrictly);
//} // end namespace llvm

//INITIALIZE_PASS_BEGIN(ApolloLoopExtractStrictly, "APOLLO_LOOP_EXTRACT",
//                      "Apollo loop extract.", false, false)
//INITIALIZE_PASS_DEPENDENCY(DominatorTreeWrapperPass)
//INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
//INITIALIZE_PASS_END(ApolloLoopExtractStrictly, "APOLLO_LOOP_EXTRACT",
//                    "Apollo loop extract.", false, false)
