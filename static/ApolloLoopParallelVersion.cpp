//===--- ApolloLoopParallelVersion.cpp ----------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "SkeletonConfig.h"
#include "InstrumentationConfig.h"
#include "MetadataKind.h"
#include "ApolloLoopParallelVersion.h"
#include "InstrumentationUtils.h"
//#include "Transformations/Instrumentation/ApolloLinearEquation.h"
//#include "Transformations/Instrumentation/ApolloOnCondition.h"
#include "Utils/FunctionUtils.h"
#include "Utils/LoopUtils.h"
#include "Utils/ViUtils.h"
#include "Utils/ConstantUtils.h"
#include "Utils/Utils.h"
#include "Utils/MetadataHandler.h"

#include "llvm/IR/Module.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Metadata.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/DerivedTypes.h"

#include "llvm/Transforms/Utils/CodeExtractor.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/Attributes.h"
#include "ParallelUtils.h"
#include "llvm/Transforms/Utils/ValueMapper.h"
#include "llvm/IR/CallSite.h"

using namespace llvm;
using namespace apollo;

namespace {

Constant* getLoopId(Function* F)
{
    StringRef name = F->getName();
    std::string namestr = name.str();
    if ( namestr.substr(0,12) == std::string("apollo_loop_") )
    {
        std::string loopidstr = namestr.substr(12,namestr.find_first_not_of("0123456789",12)-1);
        Type *i64Ty = Type::getInt64Ty(F->getContext());
        Constant *loopid = ConstantInt::get(i64Ty, stoi(loopidstr));
        return loopid;
    }
    else
    {
        std::cerr << "ERROR: function " << namestr << " is not a valid apollo loop !" << std::endl;
        return nullptr;
    }
}



//get the type of the function
void getFunctionTypeInCppFormat( Constant* loopid, Function* func, std::vector<Value *> params )
{
    FunctionType* functype = func->getFunctionType();
    //
    std::cout << "\n\n    getFunctionTypeInCppFormat  \n\n" << std::endl;
    functype->print(outs());
    std::cout << std::endl;
    Type*  retType = functype->getReturnType();
    unsigned int nbParams = functype->getNumParams();
    ArrayRef< Type* > paramsType = functype->params();
    for( unsigned int i = 0; i < nbParams; ++i )
    {
        if( paramsType[i]->isIntegerTy() )
        {
            std::cout << i << "-th params is of type integer " << std::endl;
        }
        else if( paramsType[i]->isDoubleTy() )
        {
            std::cout << i << "-th params is of type double " << std::endl;
        }
        else if( paramsType[i]->isVectorTy() )
        {
            std::cout << i << "-th params is of type vector " << std::endl;
        }
        else if( paramsType[i]->isArrayTy() )
        {
            std::cout << i << "-th params is of type array " << std::endl;
        }
        else if( paramsType[i]->isPointerTy() )
        {
            std::cout << i << "-th params is of type pointer " << std::endl;
        }
        else if( paramsType[i]->isStructTy() )
        {
            std::cout << i << "-th params is of type struct " << std::endl;
        }
        else
        {
            std::cout << i << "-th params is of unknown type " << std::endl;
        }
    }
    std::cout << "\n\n    END getFunctionTypeInCppFormat  \n\n" << std::endl;
    //
    if ( nbParams == 7 )
    {
//        InnerLoopFunction::innerLoopFunc[ loopid->getSExtValue() ] = std::bind( &func, std::placeholders::_1,
//                std::placeholders::_2,
//                std::placeholders::_3,
//                std::placeholders::_4,
//                params[4], params[5], params[6] );
        auto fn_binded = std::bind( &func, std::placeholders::_1,
            std::placeholders::_2,
            std::placeholders::_3,
            std::placeholders::_4,
            params[4], params[5], params[6] );
    }
    else
    {
        std::cout << " \n\n\n\n\n\n\n ERROR !!!!! nbParams != 7 !!!!!  \n\n\n\n\n\n\n\n" << std::endl;
    }
//    for( unsigned int i = 4; i < nbParams; ++i )
//    {
//
//    }

}

} // end anonymous namespace

namespace apollo {

ApolloLoopParallelVersion::ApolloLoopParallelVersion() :
		pass::ApolloPassOnSkeleton(ID,
				skeleton::sk_name<skeleton::parallel>(),
				"ApolloLoopParallelVersion") {
}

void ApolloLoopParallelVersion::getAnalysisUsage(AnalysisUsage &AU) const {
	AU.addRequired<LoopInfoWrapperPass>();
}


BasicBlock* createOuterColorLoopHeader( BasicBlock* insertBefore,
        BasicBlock* insertAfter, Function* F, LLVMContext& C )
{
    BasicBlock* colorLoopHeader = BasicBlock::Create(
            C, "for.color.cond", F, insertBefore );
    insertAfter->getTerminator()->eraseFromParent();
    BranchInst::Create(colorLoopHeader, insertAfter);
    std::cout << "\n\n From createOuterColorLoopHeader" << std::endl;
    colorLoopHeader->print(outs());
    return colorLoopHeader;
}
AllocaInst* createOuterColorLoopInductionVariable( IRBuilder<> Builder, Function* F, LLVMContext& C )
{
    Type *i32Ty = Type::getInt32Ty(C);
    AllocaInst* currentcoloralloc = Builder.CreateAlloca(i32Ty);
    currentcoloralloc->setName("currentcolor");
    Builder.CreateStore(Builder.getInt32(0),currentcoloralloc);
    return currentcoloralloc;
}
CallInst* createCallGetNbColor( IRBuilder<> Builder, Function* F, Module* M, Constant *loopid )
{
    // get nb colors
    std::vector<Value *> Params;
    Params.push_back(loopid);
    Function *GetNbColorsFun = getApolloGetNbColors(M);
    CallInst* nbcolors = Builder.CreateCall(GetNbColorsFun, Params);
    nbcolors->setName("nbcolors");
    return nbcolors;
}
CallInst* createCallGetItsArray( IRBuilder<> Builder, Function* F, Module* M, Constant *loopid )
{
    // get its array : corresponding to the colored graph
    std::vector<Value *> Params;
    Params.push_back(loopid);
    Function *GetItsArray = getApolloGetItsArray(M);
    CallInst* itsarray = Builder.CreateCall(GetItsArray, Params);
    itsarray->setName("itsarray");
    return itsarray;
}

bool ApolloLoopParallelVersion::runOnLoopLI(Loop *L, LoopInfo *LI) {
    if ( L->getLoopDepth() == 1 )
    {
        //
        // 0 - Initialize constant, types, etc
        //
        Function* F = L->getHeader()->getParent();
        LLVMContext& C = L->getHeader()->getContext();
        Module *M = F->getParent();
        Type *i32Ty = Type::getInt32Ty(C);
        Type *i32Ptr = Type::getInt32PtrTy(C);
        Type *voidTy = Type::getVoidTy(C);
        Type *voidPtrTy = Type::getInt8PtrTy(C);
        Constant *zero   = ConstantInt::get(i32Ty, 0, false);
        Constant *one    = ConstantInt::get(i32Ty, 1, false);
        Constant *loopid = getLoopId(F);

        //
        // 1 - In function Entry Block : load colored graph variables nbcolors and itsarray
        //
        BasicBlock* entryBlock = &F->getEntryBlock();
        IRBuilder<> entryBlockBuilder( entryBlock->getTerminator() );
        //create induction variable
        AllocaInst* currentcoloralloc = createOuterColorLoopInductionVariable( entryBlockBuilder, F, C );
        // get nb colors
        CallInst* nbcolors = createCallGetNbColor( entryBlockBuilder, F, M, loopid );
        // get its array : corresponding to the colored graph
        CallInst* itsarray = createCallGetItsArray( entryBlockBuilder, F, M, loopid );
        // print
//        entryBlock->print(outs());
        //
        // 2 - Create new loop (color loop)
        //
        BasicBlock* originalLoopHeader = entryBlock->getUniqueSuccessor();
        BasicBlock* originalLoopBody = originalLoopHeader->getTerminator()->getSuccessor(0);
        BasicBlock* colorLoopHeader = createOuterColorLoopHeader(
                originalLoopHeader,entryBlock, F, C );
        BasicBlock* colorLoopBody = BasicBlock::Create(
                C, "for.color.body", F, originalLoopHeader );
        BasicBlock* colorLoopInc = BasicBlock::Create(
                C, "for.color.inc", F, originalLoopHeader );
        // 2.1 - Create new loop header
        IRBuilder<> colorLoopHeaderBuilder(colorLoopHeader);
        //load induction variable -> currentcolor
        Value* currentcolor = colorLoopHeaderBuilder.CreateLoad(i32Ty,currentcoloralloc,"currentcolor");
        //create conditional branch inst to colorLoopBody or function exit block
        Value* colorLoopCond = colorLoopHeaderBuilder.CreateICmpSLT(currentcolor,nbcolors);
        BranchInst* colorLoopBI = BranchInst::Create(colorLoopBody, originalLoopHeader->getTerminator()->getSuccessor(1),
                colorLoopCond, colorLoopHeader);
        // 2.2 - Create new loop body
        IRBuilder<> colorLoopBodyBuilder(colorLoopBody);
        // get colored graph info :
        // nb of iterations of current color and first index with current color in itsarray
        Function *GetColorSize = getApolloColorSize(M);
        CallInst* colorSize = colorLoopBodyBuilder.CreateCall(GetColorSize, {loopid,currentcolor} );
        colorSize->setName("colorsize");
        Function *GetColorFirst = getApolloColorFirst(M);
        CallInst* colorFirst = colorLoopBodyBuilder.CreateCall(GetColorFirst, {loopid,currentcolor});
        colorFirst->setName("colorfirst");

        //WARNING : not necessary here !!!! I try to remove it
//        //create induction variable of inner loop -> innerloopiv and initialize it to 0
//        Value* innerloopivalloc = colorLoopBodyBuilder.CreateAlloca(i32Ty);
//        innerloopivalloc->setName("innerloopivaddr");
//        colorLoopBodyBuilder.CreateStore( zero, innerloopivalloc );
        //END WARNING

//        //branch to inner loop (original loop) block
//        colorLoopBodyBuilder.CreateBr( originalLoopHeader );
//        colorLoopBody->print(outs());
        // 2.3 - Create new loop latch (inc block)
        IRBuilder<> colorLoopIncBuilder(colorLoopInc);
        //increment color counter -> currentcolor
        currentcolor = colorLoopIncBuilder.CreateLoad( i32Ty, currentcoloralloc, "currentcolor" );
        Value* nextcolor = colorLoopIncBuilder.CreateAdd( currentcolor, one );
        colorLoopIncBuilder.CreateStore(nextcolor,currentcoloralloc);
        //branch to color loop header (cond block)
        BranchInst::Create(colorLoopHeader, colorLoopInc);
        // print
//        colorLoopHeader->print(outs());
//        colorLoopBody->print(outs());
//        colorLoopInc->print(outs());

        //
        // 3 - Create a new block to allocate and initialize the induction variable and bounds of inner loop
        //     the new induction variable iterates from 0 to colorSize
        //
        BasicBlock* innerLoopRoot = BasicBlock::Create(
                C, "inner.loop.root", F, originalLoopHeader );
        BranchInst::Create(innerLoopRoot, colorLoopBody);
        innerLoopRoot->setName("inner.loop.entry");
        IRBuilder<> innerLoopRootBuilder( innerLoopRoot );
        // create induction variable of modified inner loop -> innerloopiv and initialize it to 0
        Value* innerloopiv = innerLoopRootBuilder.CreateAlloca(i32Ty, one, "innerloopiv");
        innerLoopRootBuilder.CreateStore( zero, innerloopiv );
        //
        // 4 - replace originalLoopHeader with a new header innerLoopCond
        //     the new induction variable iterates from 0 to colorSize
        //
        BasicBlock* innerLoopCond = BasicBlock::Create(
                C, "inner.loop.cond", F, originalLoopHeader );
        BasicBlock* innerLoopBody = BasicBlock::Create(
                C, "inner.loop.body", F, originalLoopHeader );
        BranchInst::Create(innerLoopCond, innerLoopRoot);
        IRBuilder<> innerLoopCondBuilder( innerLoopCond );
        // 4.1 - load induction variable of modified inner loop -> innerloopiv
        Value* innerloopivvalue = innerLoopCondBuilder.CreateLoad(i32Ty,innerloopiv);
        // 4.2 - conditional branch inst
        Value* newInnerLoopCond = innerLoopCondBuilder.CreateICmpSLT(innerloopivvalue,colorSize);
        BranchInst* newInnerLoopBI = innerLoopCondBuilder.CreateCondBr(newInnerLoopCond,
                innerLoopBody, colorLoopInc);
        // 4.3 - in old loop latch (inc block): identify the old induction variable
        BasicBlock* originalLoopLatch = L->getLoopLatch();
        Value* oldiv;
        for ( Instruction &I : *originalLoopLatch )
        {
            LoadInst* Load = dyn_cast<LoadInst>(&I);
            if( Load )
            {
                Value* lop = Load->getPointerOperand();
                //if it has more than 1 use it must be the induction variable
                // (it is used in the condition + at least in the inc block,
                //  whereas loop bounds are used only once )  ->  TODO: CHECK IF THIS IS TRUE !!! IF NOT, FIND A WAY TO IDENTIFY INDUCTION VARIABLE  ->  il suffit de regarder si c'est utilisé dans le .inc bloc ?
                if ( !lop->hasOneUse() )
                {
                    oldiv = lop;
                }
            }
        }
        // 4.4 - replace loop latch with a new block that uses the new induction var innerloopivalloc
        BasicBlock* innerLoopInc = BasicBlock::Create(
                C, "inner.loop.inc", F, originalLoopHeader );
        BranchInst::Create(innerLoopCond, innerLoopInc);
        IRBuilder<> innerLoopIncBuilder( innerLoopInc->getTerminator() );
        Value* innerloopivinc = innerLoopIncBuilder.CreateAdd(innerloopivvalue, one);
        innerLoopIncBuilder.CreateStore(innerloopivinc,innerloopiv);
        //
        // in inner loop body :
        //    begin by loading itsarray[colorfirst+innerloopiv]
        //    then replace all uses of the old induction variable with itsarray[colorfirst+innerloopiv]
        //
        //Peut-on faire des additions sur des adresses pour obtenir l'elt suivant dans un array ?
        //-> si oui on pourrait faire un load de its[first] dans le begin block de la color loop
        //   puis faire des ++ a chaque iteration de la inner loop
//        BranchInst::Create(innerLoopInc, innerLoopBody);
        BranchInst::Create(originalLoopHeader->getTerminator()->getSuccessor(0), innerLoopBody);
        IRBuilder<> innerLoopBodyBuilder( &(innerLoopBody->front()) );
        Value* currenteltidx = innerLoopBodyBuilder.CreateAdd( colorFirst, innerloopivvalue );
        Value* currenteltgep = innerLoopBodyBuilder.CreateGEP(i32Ty, itsarray,
                currenteltidx,"currenteltAddr");
//        Value* currentelt = innerLoopBodyBuilder.CreateLoad(i32Ty, currenteltgep,"currentelt");

        //print
        std::cout << "\n\n\n ------------------------------ \n" << std::endl;
        innerLoopBody->print(outs());
        originalLoopHeader->getTerminator()->getSuccessor(0)->print(outs());
        std::cout << "\n ------------------------------ \n" << std::endl;
        // replace uses of old induction var outside of inner loop latch (.inc)
        // (this should be only the inner body block)
        // with currenteltgep
        oldiv->replaceAllUsesWith( currenteltgep );
//        oldiv->replaceUsesOutsideBlock( currenteltgep, originalLoopLatch);
        //
        // delete the old loop latch and the old header block
        //
        originalLoopLatch->replaceAllUsesWith( innerLoopInc );
        originalLoopLatch->eraseFromParent();
        originalLoopHeader->replaceAllUsesWith( innerLoopCond );
        originalLoopHeader->eraseFromParent();

        //
        // 5 - Extract original (modified) loop into a function
        //
        // we get all blocks that are dominated by the header block (.cond) of the original loop
        // or by innerLoopRoot ?????
        DominatorTree DT(*F);
        std::vector<BasicBlock *> innerLoopBlocks;
        for( BasicBlock& BB : *F )
        {
            if ( DT.dominates(innerLoopCond,&BB) )
            {
                // WARNING: for.color.inc is dominated by originalLoopHeader but is not part of the inner loop
                //          so we need to remove it from innerLoopBlocks or not add it in the first place
                if ( colorLoopInc != &BB )
                {
                    innerLoopBlocks.push_back(&BB);
                }
            }
        }
        // extract the blocks into a separate Function
        CodeExtractor Extractor(innerLoopBlocks,&DT);
        Function *Extracted = Extractor.extractCodeRegion();
        //print
        std::cout << "\n\n\n ------------------------------ \n" << std::endl;
        Extracted->print(outs());
        std::cout << "\n ------------------------------ \n" << std::endl;
        // apollo loops are internal and not inlinable.
        Extracted->setName(utils::concat(F->getName(),"_inner_loop"));
        Extracted->setLinkage(Function::ExternalLinkage);
//        Extracted->removeFnAttr(Attribute::AlwaysInline);
//        Extracted->addFnAttr(Attribute::NoInline);
        FunctionType* functype = Extracted->getFunctionType();

        // get the block that calls function Extracted :
        BasicBlock* innerLoopCallingBlock = innerLoopRoot->getUniqueSuccessor();

        // Look for the call instruction (innerLoopCallInst) that calls apollo_loop_<N>_skeleton_4_inner_loop (that is Function Extracted)
        CallInst* innerLoopCallInst = nullptr;
        for( Instruction& I : *innerLoopCallingBlock )
        {
            CallInst* call = dyn_cast<CallInst>(&I);
            if ( call )
            {
                if( call->getCalledFunction()->getName()
                        == utils::concat(F->getName(),"_inner_loop") )
                {
                    innerLoopCallInst = call;
                    break;
                }
            }
        }
        // get the operands of the call and their type
        std::vector<Value *> ParamsExtracted;
        std::vector<Type *> ParamsExtractedType;
        unsigned int nbOperands = innerLoopCallInst->getNumArgOperands();
        for ( unsigned int i = 0; i < nbOperands; ++i )
        {
            ParamsExtracted.push_back(innerLoopCallInst->getArgOperand(i));
            ParamsExtractedType.push_back(innerLoopCallInst->getArgOperand(i)->getType());
        }
        //
        // 6 - replace innerLoopCallingBlock with a new block that calls the parallelizing function
        //     it is the parallelizing function that will call apollo_loop_<N>_skeleton_4_inner_loop
        //     in parallel with modified arguments
        //
        BasicBlock* innerLoopParallelizingBlock = BasicBlock::Create(
                C, "inner.loop.parallel", F, innerLoopCallingBlock );
        BranchInst::Create(colorLoopInc, innerLoopParallelizingBlock);
        innerLoopCallingBlock->replaceAllUsesWith(innerLoopParallelizingBlock);
        innerLoopCallingBlock->dropAllReferences();
        innerLoopCallingBlock->eraseFromParent();
        //
        //  I try to do something similar to CodeBonesCodeGeneration::visitClastParallelFor in apollo
        //
        IRBuilder<> innerLoopParallelizingBlockBuilder( innerLoopParallelizingBlock->getTerminator() );
        unsigned int nbParams = functype->getNumParams();
        ArrayRef< Type* > paramsType = functype->params();
        // We know the first 4 parameters of function Extracted : innverloopiv, colorsize, colorfirst, itsarray
        // but the other parameters are dependent on the body if the inner loop
        // so we encapsulate them in a struct, and pass it as a void* to the parallelizing function
        std::vector< Type* > unknownParamsType;
        std::vector< Value* > unknownParams;
        for ( unsigned int i=4; i<nbParams; ++i )
        {
            unknownParamsType.push_back( paramsType[i] );
            unknownParams.push_back( ParamsExtracted[i] );
        }
        //
        StructType *SymbolsStructTy = StructType::create(unknownParamsType);
        AllocaInst *StructAlloc =
                innerLoopParallelizingBlockBuilder.CreateAlloca(SymbolsStructTy, 0, "packed_symbols");
        int SymbolIdx = 0;
        std::vector< LoadInst* > newLoadedParams;
        for (auto &paramValue : unknownParams) {
          if (!isa<Function>(paramValue)) {
            llvm::Value *StructField = innerLoopParallelizingBlockBuilder.CreateStructGEP(
                cast<PointerType>(StructAlloc->getType()->getScalarType())
                    ->getElementType(),
                StructAlloc, SymbolIdx);
            newLoadedParams.push_back(innerLoopParallelizingBlockBuilder.CreateLoad(cast<PointerType>(paramValue->getType())->getElementType(), paramValue));
            innerLoopParallelizingBlockBuilder.CreateStore(newLoadedParams.back()->getPointerOperand(), StructField);
            SymbolIdx++;
          }
        }
        //
        // 7 - call parallelizing function
        //
        // We need to modify Function Extracted so that it takes a void* as argument
        // and then cast it back to the "real" argument types originalParamsType
        //
        std::cout << "------- creating new inner_loop.generic.func : -------" << std::endl;
//        FunctionType *ft = FunctionType::get(voidTy, {i32Ptr, i32Ty, i32Ty, i32Ptr, voidPtrTy}, false);
        FunctionType *ft = FunctionType::get(voidTy, {i32Ptr, i32Ty, i32Ty, i32Ptr, SymbolsStructTy->getPointerTo()}, false);
        Function* GenericInnerLoop = Function::Create( ft, Function::ExternalLinkage, utils::concat(Extracted->getName(), ".generic.func"), M);

        BasicBlock *Entry = BasicBlock::Create(C, "entry", GenericInnerLoop);
        BasicBlock *CallInnerLoopFunc = BasicBlock::Create(C, "call_inner_loop_func", GenericInnerLoop);
        BasicBlock *Exit = BasicBlock::Create(C, "exit", GenericInnerLoop);
        BranchInst::Create(CallInnerLoopFunc, Entry);
        BranchInst::Create(Exit, CallInnerLoopFunc);
        ReturnInst::Create(C, Exit);
        // Get the arguments
        llvm::Argument *innerLoopIVArg;
        llvm::Argument *colorSizeArg;
        llvm::Argument *colorFirstArg;
        llvm::Argument *itsArrayArg;
        llvm::Argument *symbolsStructArg;
        auto argument_it = GenericInnerLoop->arg_begin();
        innerLoopIVArg = &*argument_it++;
        colorSizeArg = &*argument_it++;
        colorFirstArg = &*argument_it++;
        itsArrayArg = &*argument_it++;
        symbolsStructArg = &*argument_it;
        innerLoopIVArg->setName("innerloopiv");
        colorSizeArg->setName("colorsize");
        colorFirstArg->setName("colorfirst");
        itsArrayArg->setName("itsarray");
        symbolsStructArg->setName("symbolsstruct");
        //print
        GenericInnerLoop->print(outs());
        // Reload the symbols
        std::vector< Value* > NewSymbols;
        IRBuilder<> EntryBuilder(Entry->getTerminator());
        SymbolIdx = 0;
        for (auto &symbol : unknownParams) {
            symbol->getType()->print(outs());
            std::cout << std::endl;
            cast<PointerType>(symbol->getType()->getScalarType())
                                  ->getElementType()->print(outs());
            std::cout << std::endl;
            symbolsStructArg->getType()->print(outs());
            std::cout << std::endl;
          if (!isa<Function>(symbol)) {
              llvm::Value *Ptr = EntryBuilder.CreateStructGEP(
                  cast<PointerType>(symbolsStructArg->getType()->getScalarType())
                      ->getElementType(),
                  symbolsStructArg, SymbolIdx);
              NewSymbols.push_back(EntryBuilder.CreateLoad(Ptr));
              SymbolIdx++;
          } else {
              NewSymbols.push_back(symbol);
          }
        }
        // Call inner loop function Extracted
        // build argument list
        std::vector< Value* > ArgList = { innerLoopIVArg, colorSizeArg, colorFirstArg, itsArrayArg };
        for ( int i = 0; i < NewSymbols.size(); ++i )
        {
            ArgList.push_back( NewSymbols[i] );
        }
        IRBuilder<> CallInnerLoopFuncBuilder(CallInnerLoopFunc->getTerminator());
        CallInst* innerLoopFuncCall = CallInnerLoopFuncBuilder.CreateCall(Extracted, ArgList);

        //print
        GenericInnerLoop->print(outs());


    //        std::vector<Type *> intype ={voidPtrTy, i32Ptr, i32Ptr, doublePtrPtrTy, i32Ptr, i32Ty, i32Ty, i32Ptr};
        std::vector<Type *> intype ={ i32Ptr, i32Ty, i32Ty, i32Ptr, SymbolsStructTy->getPointerTo() };
    //        std::vector<Type *> intype ={voidPtrTy, PointerType::get(SymbolsStructTy, 0), i32Ptr, i32Ty, i32Ty, i32Ptr};
    //        ParamsExtractedType.insert(ParamsExtractedType.begin(),voidPtrTy);  //add a parameter corresponding to the address of the loop function
        Function *GetRunInParallel = getApolloRunInParallel(M);
//        innerLoopParallelizingBlockBuilder.CreateCall(GetRunInParallel, {FuncCast,ParamsExtracted[4], ParamsExtracted[5], ParamsExtracted[6],
//                ParamsExtracted[0], ParamsExtracted[1], ParamsExtracted[2], ParamsExtracted[3]});

        std::cout << std::endl;
        GetRunInParallel->getType()->print(outs());
        std::cout << std::endl;
        ParamsExtracted[0]->getType()->print(outs());
        std::cout << std::endl;
        ParamsExtracted[1]->getType()->print(outs());
        std::cout << std::endl;
        ParamsExtracted[2]->getType()->print(outs());
        std::cout << std::endl;
        ParamsExtracted[3]->getType()->print(outs());
        std::cout << std::endl;
        StructAlloc->getType()->print(outs());
        std::cout << std::endl;
        GenericInnerLoop->getType()->print(outs());
        std::cout << std::endl;
        llvm::Value *StructCast = innerLoopParallelizingBlockBuilder.CreateBitCast(
            StructAlloc, voidPtrTy);
        llvm::Value *FuncCast = innerLoopParallelizingBlockBuilder.CreateBitCast(
                GenericInnerLoop, voidPtrTy);
        innerLoopParallelizingBlockBuilder.CreateCall(GetRunInParallel, {
                ParamsExtracted[0], ParamsExtracted[1], ParamsExtracted[2], ParamsExtracted[3],
                StructCast, FuncCast} );
//        //get a llvm function from Extracted with binded arguments for args >4  -> THIS IS NOT POSSIBLE WITH LLVM FUNCTION TYPE
//        Type *voidTy = Type::getVoidTy(C);
//        Type *i64Ty = Type::getInt64Ty(C);
//        Type *i64Ptr = Type::getInt64PtrTy(C);
//        std::vector<Type *> intype ={i64Ptr, i64Ty, i64Ty, i64Ptr};
//    //    FunctionType *ft = FunctionType::get(voidTy, func_type, false);
//        FunctionType *ft = FunctionType::get(voidTy, intype, false);
//        GlobalVariable::LinkageTypes InternalLinkage =
//            GlobalVariable::LinkageTypes::InternalLinkage;
//        std::function< void (int*,int,int,int*) > fn;
////        Function* fn = Function::Create(ft);
////        void (*fn)(int*,int,int,int*);
//        if ( ParamsExtracted.size() == 7 )
//        {
//    //        InnerLoopFunction::innerLoopFunc[ loopid->getSExtValue() ] = std::bind( &func, std::placeholders::_1,
//    //                std::placeholders::_2,
//    //                std::placeholders::_3,
//    //                std::placeholders::_4,
//    //                params[4], params[5], params[6] );
//            fn = std::bind( *Extracted, std::placeholders::_1,
//                    std::placeholders::_2,
//                    std::placeholders::_3,
//                    std::placeholders::_4,
//                    ParamsExtracted[4], ParamsExtracted[5], ParamsExtracted[6] );
//        }
//        else
//        {
//            std::cout << " \n\n\n\n\n\n\n ERROR !!!!! nbParamsExtracted != 7 !!!!!  \n\n\n\n\n\n\n\n" << std::endl;
//        }
        //register this binded function as the inner loop function for apollo loop loopid
//        newCallingBlockBuilder.CreateCall(ApolloRegisterInnerLoopFunction,{loopid,nbargs,Extracted});
//        newCallingBlockBuilder.CreateCall(ApolloRegisterInnerLoopFunction,{loopid,nbargs,fn});

//        IRBuilder<> newCallingBlockBuilder( &(newCallingBlock->front()) );
//        /*CallInst* runinparallel = */newCallingBlockBuilder.CreateCall(GetRunInParallel, ParamsExtracted);
////        runinparallel->setName("runinparallel");

//
        // PRINT
//        F->getEntryBlock().print(outs());
//        colorLoopHeader->print(outs());
//        colorLoopBody->print(outs());
//        colorLoopInc->print(outs());
//        originalLoopBody->print(outs());
//        originalLoopHeader->print(outs());
//        newInnerLoopLatch->print(outs());
    }
	return true;
}

char ApolloLoopParallelVersion::ID = 0;
//DefinePassCreator(ApolloLoopParallelVersion);
static RegisterPass<ApolloLoopParallelVersion> ApolloLoopParallelVersion("loopparallelversion","modifies parallel version of apollo loop (apollo_loop_<N>_skeleton_4) to make it parallel");


} // end namespace apollo

//namespace llvm {
//class PassRegistry;
//DeclarePassInitializator(ApolloLoopParallelVersion);
//} // end namespace llvm
//
//INITIALIZE_PASS_BEGIN(ApolloLoopParallelVersion, "APOLLO_INSTRUMENT_MEM_ACCESS",
//		"APOLLO_INSTRUMENT_MEM_ACCESS", false, false)
//	INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
//	INITIALIZE_PASS_END(ApolloLoopParallelVersion,
//			"APOLLO_INSTRUMENT_MEM_ACCESS", "APOLLO_INSTRUMENT_MEM_ACCESS",
//			false, false)

