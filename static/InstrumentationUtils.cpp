//===--- InstrumentationUtils.cpp -----------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Type.h"
#include <iostream>

using namespace llvm;

Function *getApolloRegisterEvent(Module *M) {
  const char *apolloRegisterEvent = "apollo_register_event";
  Function *RegisterFunction = M->getFunction(apolloRegisterEvent);
  if (RegisterFunction)
    return RegisterFunction;
  LLVMContext &Context = M->getContext();
  GlobalVariable::LinkageTypes ExternalLinkage =
      GlobalVariable::LinkageTypes::ExternalLinkage;
  Type *i64Ty = Type::getInt64Ty(Context);
  Type *voidTy = Type::getVoidTy(Context);
  Type *i8Ptr = Type::getInt8PtrTy(Context);
  std::vector<Type *> i8PtrI64(5, i64Ty); // EventType, Key, Val, N
  i8PtrI64[0] = i8Ptr;
  FunctionType *voidI8PtrI64 = FunctionType::get(voidTy, i8PtrI64, true);
  return Function::Create(voidI8PtrI64, ExternalLinkage, 
                          apolloRegisterEvent, M);
}

Function *getApolloRegisterStore(Module *M) {
  const char *apolloRegisterStore = "apollo_register_store";
  Function *RegisterFunction = M->getFunction(apolloRegisterStore);
  if (RegisterFunction)
    return RegisterFunction;
  LLVMContext &Context = M->getContext();
  GlobalVariable::LinkageTypes ExternalLinkage =
      GlobalVariable::LinkageTypes::ExternalLinkage;
  Type *i64Ty = Type::getInt64Ty(Context);
  Type *voidTy = Type::getVoidTy(Context);
  std::vector<Type *> i8PtrI64(3, i64Ty); // Key, Val, VI
  FunctionType *voidI8PtrI64 = FunctionType::get(voidTy, i8PtrI64, true);
  return Function::Create(voidI8PtrI64, ExternalLinkage,
                          apolloRegisterStore, M);
}

Function *getApolloLoopVersionChooser(Module *M) {
  const char *apolloLoopVersionChooser = "apollo_loop_version_chooser";
  Function *LoopVersionChooser = M->getFunction(apolloLoopVersionChooser);
  if (LoopVersionChooser)
    return LoopVersionChooser;
  LLVMContext &Context = M->getContext();
  GlobalVariable::LinkageTypes ExternalLinkage =
      GlobalVariable::LinkageTypes::ExternalLinkage;
  Type *i64Ty = Type::getInt64Ty(Context);
  Type *i32Ty = Type::getInt32Ty(Context);
  FunctionType *i32I64 = FunctionType::get(i32Ty, i64Ty, true);
  return Function::Create(i32I64, ExternalLinkage,
          apolloLoopVersionChooser, M);
}

Function *getApolloGetNbColors(Module *M) {
  const char *apolloGetNbColors = "apollo_get_nb_colors";
  Function *GetNbColors = M->getFunction(apolloGetNbColors);
  if (GetNbColors)
    return GetNbColors;
  LLVMContext &Context = M->getContext();
  GlobalVariable::LinkageTypes ExternalLinkage =
      GlobalVariable::LinkageTypes::ExternalLinkage;
  Type *i64Ty = Type::getInt64Ty(Context);
  Type *i32Ty = Type::getInt32Ty(Context);
  FunctionType *i32I64 = FunctionType::get(i32Ty, i64Ty, true);
  return Function::Create(i32I64, ExternalLinkage,
          apolloGetNbColors, M);
}

Function *getApolloGetItsArray(Module *M) {
  const char *apolloGetItsArray = "apollo_get_its_array";
  Function *GetItsArray = M->getFunction(apolloGetItsArray);
  if (GetItsArray)
    return GetItsArray;
  LLVMContext &Context = M->getContext();
  GlobalVariable::LinkageTypes ExternalLinkage =
      GlobalVariable::LinkageTypes::ExternalLinkage;
  Type *i64Ty = Type::getInt64Ty(Context);
  Type *i32Ty = Type::getInt32Ty(Context);
  PointerType* i32PtrTy = PointerType::get(i32Ty, 0);
  FunctionType *i32PtrI64 = FunctionType::get(i32PtrTy, i64Ty, true);
  return Function::Create(i32PtrI64, ExternalLinkage,
          apolloGetItsArray, M);
}

Function *getApolloColorSize(Module *M) {
  const char *apolloColorSize = "apollo_get_color_size";
  Function *ColorSize = M->getFunction(apolloColorSize);
  if (ColorSize)
    return ColorSize;
  LLVMContext &Context = M->getContext();
  GlobalVariable::LinkageTypes ExternalLinkage =
      GlobalVariable::LinkageTypes::ExternalLinkage;
  Type *i64Ty = Type::getInt64Ty(Context);
  Type *i32Ty = Type::getInt32Ty(Context);
  std::vector<Type *> i64I32 ={i64Ty, i32Ty}; // Id, Color
  FunctionType *i32I64I32 = FunctionType::get(i32Ty, i64I32, true);
  return Function::Create(i32I64I32, ExternalLinkage,
          apolloColorSize, M);
}

Function *getApolloColorFirst(Module *M) {
  const char *apolloColorFirst = "apollo_get_color_first";
  Function *ColorFirst = M->getFunction(apolloColorFirst);
  if (ColorFirst)
    return ColorFirst;
  LLVMContext &Context = M->getContext();
  GlobalVariable::LinkageTypes ExternalLinkage =
      GlobalVariable::LinkageTypes::ExternalLinkage;
  Type *i64Ty = Type::getInt64Ty(Context);
  Type *i32Ty = Type::getInt32Ty(Context);
  std::vector<Type *> i64I32 ={i64Ty, i32Ty}; // Id, Color
  FunctionType *i32I64I32 = FunctionType::get(i32Ty, i64I32, true);
  return Function::Create(i32I64I32, ExternalLinkage,
          apolloColorFirst, M);
}

Function *getApolloColorLast(Module *M) {
  const char *apolloColorLast = "apollo_get_color_last";
  Function *ColorLast = M->getFunction(apolloColorLast);
  if (ColorLast)
    return ColorLast;
  LLVMContext &Context = M->getContext();
  GlobalVariable::LinkageTypes ExternalLinkage =
      GlobalVariable::LinkageTypes::ExternalLinkage;
  Type *i64Ty = Type::getInt64Ty(Context);
  Type *i32Ty = Type::getInt32Ty(Context);
  std::vector<Type *> i64I32 ={i64Ty, i32Ty}; // Id, Color
  FunctionType *i32I64I32 = FunctionType::get(i32Ty, i64I32, true);
  return Function::Create(i32I64I32, ExternalLinkage,
          apolloColorLast, M);
}

Function *getApolloCreateAndColorDependencyGraph(Module *M) {
  const char *apolloCreateAndColorDependencyGraph = "apollo_create_and_color_dependency_graph";
  Function *CreateAndColorDependencyGraph = M->getFunction(apolloCreateAndColorDependencyGraph);
  if (CreateAndColorDependencyGraph)
    return CreateAndColorDependencyGraph;
  LLVMContext &Context = M->getContext();
  GlobalVariable::LinkageTypes ExternalLinkage =
      GlobalVariable::LinkageTypes::ExternalLinkage;
  Type *i64Ty = Type::getInt64Ty(Context); // Id
  Type *voidTy = Type::getVoidTy(Context);
  FunctionType *voidI64 = FunctionType::get(voidTy, i64Ty, true);
  return Function::Create(voidI64, ExternalLinkage,
          apolloCreateAndColorDependencyGraph, M);
}

