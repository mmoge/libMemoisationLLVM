//===--- ApolloInlineFunction.h --------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef MY_APOLLO_MEM_TO_REG_H
#define MY_APOLLO_MEM_TO_REG_H

#include "BasicPass/ApolloPass.h"
#include "BasicPass/ApolloPassOnSkeleton.h"

#include "llvm/Transforms/Utils/ValueMapper.h"
#include "llvm/IR/Instructions.h"

#include "llvm/IR/Function.h"
#include "llvm/IR/PassManager.h"
#include "llvm/IR/CallSite.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/Analysis/AliasAnalysis.h"

#include <utility>
#include <iostream>

namespace apollo {

/// \file ApolloInlineFunction.h
/// \brief this is almost identical to LLVM's InlineFunction file
///        it would probably be best to use LLVM's InlineFunction directly



/// A class for recording information about inlining a landing pad.
class LandingPadInliningInfo {
  llvm::BasicBlock *OuterResumeDest; ///< Destination of the invoke's unwind.
  llvm::BasicBlock *InnerResumeDest; ///< Destination for the callee's resume.
  llvm::LandingPadInst *CallerLPad;  ///< LandingPadInst associated with the invoke.
  llvm::PHINode *InnerEHValuesPHI;   ///< PHI for EH values from landingpad insts.
  llvm::SmallVector<llvm::Value*, 8> UnwindDestPHIValues;

public:
  LandingPadInliningInfo(llvm::InvokeInst *II)
    : OuterResumeDest(II->getUnwindDest()), InnerResumeDest(nullptr),
      CallerLPad(nullptr), InnerEHValuesPHI(nullptr) {
    // If there are PHI nodes in the unwind destination block, we need to keep
    // track of which values came into them from the invoke before removing
    // the edge from this block.
    llvm::BasicBlock *InvokeBB = II->getParent();
    llvm::BasicBlock::iterator I = OuterResumeDest->begin();
    for (; llvm::isa<llvm::PHINode>(I); ++I) {
      // Save the value to use for this edge.
        llvm::PHINode *PHI = llvm::cast<llvm::PHINode>(I);
      UnwindDestPHIValues.push_back(PHI->getIncomingValueForBlock(InvokeBB));
    }

    CallerLPad = llvm::cast<llvm::LandingPadInst>(I);
  }

  /// The outer unwind destination is the target of
  /// unwind edges introduced for calls within the inlined function.
  llvm::BasicBlock *getOuterResumeDest() const;

  llvm::BasicBlock *getInnerResumeDest();

  llvm::LandingPadInst *getLandingPadInst() const;

  /// Forward the 'resume' instruction to the caller's landing pad block.
  /// When the landing pad block has only one predecessor, this is
  /// a simple branch. When there is more than one predecessor, we need to
  /// split the landing pad block after the landingpad instruction and jump
  /// to there.
  void forwardResume(llvm::ResumeInst *RI,
          llvm::SmallPtrSetImpl<llvm::LandingPadInst*> &InlinedLPads);

  /// Add incoming-PHI values to the unwind destination block for the given
  /// basic block, using the values for the original invoke's source block.
  void addIncomingPHIValuesFor(llvm::BasicBlock *BB) const;

  void addIncomingPHIValuesForInto(llvm::BasicBlock *src, llvm::BasicBlock *dest) const;

};


bool InlineFunction(llvm::CallSite CS, llvm::InlineFunctionInfo &IFI,
        llvm::AAResults *CalleeAAR, bool InsertLifetime);

} // end namespace apollo

#endif
