//===- Reg2Mem.cpp - Convert registers to allocas -------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file demotes all registers to memory references.  It is intended to be
// the inverse of PromoteMemoryToRegister.  By converting to loads, the only
// values live across basic blocks are allocas and loads before phi nodes.
// It is intended that this should make CFG hacking much easier.
// To make later hacking easier, the entry block is split into two, such that
// all introduced allocas and nothing else are in the entry block.
//
//===----------------------------------------------------------------------===//


#include "ApolloMem2Reg.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/Analysis/AssumptionCache.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Utils/PromoteMemToReg.h"
#include "llvm/Transforms/Utils/UnifyFunctionExitNodes.h"
#include "Utils/MetadataHandler.h"
using namespace llvm;
using namespace apollo;

namespace {

static bool promoteMemoryToRegister(Function &F, DominatorTree &DT,
                                    AssumptionCache &AC) {
  std::vector<AllocaInst *> Allocas;
  BasicBlock &BB = F.getEntryBlock(); // Get the entry node for the function
  bool Changed = false;

  while (1) {
    Allocas.clear();

    // Find allocas that are safe to promote, by looking at all instructions in
    // the entry node
    for (BasicBlock::iterator I = BB.begin(), E = --BB.end(); I != E; ++I)
      if (AllocaInst *AI = dyn_cast<AllocaInst>(I)) // Is it an alloca?
        if (isAllocaPromotable(AI))
          Allocas.push_back(AI);

    if (Allocas.empty())
      break;

    PromoteMemToReg(Allocas, DT, nullptr, &AC);
    Changed = true;
  }
  return Changed;
}

}  //end anonymous namespace



namespace apollo {

  /// \brief To run this pass, first we calculate the alloca
  ///        instructions that are safe for promotion, then we promote each one.
  ///
  bool myPromoteLegacyPass::runOnFunction(Function &F) {
    if (skipFunction(F))
      return false;

    std::set< Function* > functions_to_inline;
    for ( BasicBlock& BB : F )
    {
        for ( Instruction& I : BB )
        {
            CallInst *CI = dyn_cast<CallInst>(&I);
            if ( CI && metadata::hasMetadataKind(CI, mdkind::apollo) )
            {
                functions_to_inline.insert( (CI->getCalledFunction()) );
            }
            InvokeInst *II = dyn_cast<InvokeInst>(&I);
            if ( II && metadata::hasMetadataKind(II, mdkind::apollo) )
            {
                functions_to_inline.insert( (II->getCalledFunction()) );
            }
        }
    }
    for ( std::set< Function* >::iterator itfunc = functions_to_inline.begin();
          itfunc != functions_to_inline.end();
          ++itfunc )
    {
        DominatorTree DT = DominatorTree (*(*itfunc));// = getAnalysis<DominatorTreeWrapperPass>().getDomTree();
        AssumptionCache &AC = getAnalysis<AssumptionCacheTracker>().getAssumptionCache(*(*itfunc));
        promoteMemoryToRegister(*(*itfunc), DT, AC);
    }
    return false;
  }

  void myPromoteLegacyPass::getAnalysisUsage(AnalysisUsage &AU) const {
    AU.addRequired<AssumptionCacheTracker>();
    AU.addRequired<DominatorTreeWrapperPass>();
    AU.setPreservesCFG();
  }


char myPromoteLegacyPass::ID = 0;
//DefinePassCreator(ApolloInstrumentMemAccess);
// Register ApolloLoopExtract in LLVM pass manager
// Required to use the pass with opt or clang
static RegisterPass<myPromoteLegacyPass> myPromoteLegacyPass("myapollomemtoreg","ApolloMem2Reg");

} // end namespace apollo



//char MyApolloRegToMem::ID = 0;
//INITIALIZE_PASS_BEGIN(MyApolloRegToMem, "reg2mem", "Demote all values to stack slots",
//                false, false)
//INITIALIZE_PASS_DEPENDENCY(BreakCriticalEdges)
//INITIALIZE_PASS_END(MyApolloRegToMem, "reg2mem", "Demote all values to stack slots",
//                false, false)



