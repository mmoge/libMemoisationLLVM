//===--- ParallelLoopExtract.cpp ----------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "SkeletonConfig.h"
#include "InstrumentationConfig.h"
#include "MetadataKind.h"
#include "ParallelLoopExtract.h"
#include "InstrumentationUtils.h"
//#include "Transformations/Instrumentation/ApolloLinearEquation.h"
//#include "Transformations/Instrumentation/ApolloOnCondition.h"
#include "ApolloCloneLoopFunctionMultipleVersions.h"
#include "Utils/FunctionUtils.h"
#include "Utils/LoopUtils.h"
#include "Utils/ViUtils.h"
#include "Utils/ConstantUtils.h"
#include "Utils/Utils.h"
#include "Utils/MetadataHandler.h"

#include "llvm/IR/Module.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Metadata.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/DerivedTypes.h"

#include "llvm/Transforms/Utils/CodeExtractor.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/Attributes.h"
#include "ParallelUtils.h"
#include "llvm/Transforms/Utils/ValueMapper.h"
#include "llvm/IR/CallSite.h"

using namespace llvm;
using namespace apollo;

namespace {

} // end anonymous namespace

namespace apollo {

ParallelLoopExtract::ParallelLoopExtract() :
		pass::ApolloPassOnSkeleton(ID,
				skeleton::sk_name<skeleton::parallel>(),
				"ParallelLoopExtract") {
}

void ParallelLoopExtract::getAnalysisUsage(AnalysisUsage &AU) const {
	AU.addRequired<LoopInfoWrapperPass>();
}



bool ParallelLoopExtract::runOnLoopLI(Loop *L, LoopInfo *LI) {
    if ( L->getLoopDepth() == 1 )
    {
        Function* F = L->getHeader()->getParent();
        //
        // 1 - extract outer loop in a separate function
        //
        DominatorTree DT(*F);
        CodeExtractor Extractor(DT, *L);
        Function *ParallelLoopFunc = Extractor.extractCodeRegion();
        // apollo loops are internal and not inlinable.
        ParallelLoopFunc->setName(utils::concat(F->getName(),"_parallel_loop"));
        ParallelLoopFunc->setLinkage(Function::ExternalLinkage);
        //
        // 2 - in function F, make sure that the call to function ParallelLoopFunc
        //     is in a separate block, with no other instruction
        CallInst* parallelLoopFuncCallInst = nullptr;
        BasicBlock* parallelLoopFuncCallBlock = nullptr;
        int nb_inst_before = 0;
        int nb_inst_after = 0;
        for ( BasicBlock& BB : *F )
        {
            nb_inst_before = 0;   //nb of instr before the call inst in parallelLoopFuncCallBlock
            nb_inst_after = 0;    //nb of instr after the call inst in parallelLoopFuncCallBlock
            for ( Instruction& I : BB )
            {
                CallInst* CI = dyn_cast<CallInst>(&I);
                if( CI )
                {
                    if ( CI->getCalledFunction() == ParallelLoopFunc )
                    {
                        //CI is the Call to function ParallelLoopFunc
                        parallelLoopFuncCallBlock = &BB;
                        parallelLoopFuncCallInst = CI;
                    }
                }
                if( !parallelLoopFuncCallBlock )
                {
                    nb_inst_before++;
                }
                else
                {
                    nb_inst_after++;
                }
            }
            if( parallelLoopFuncCallBlock )
            {
                break;
            }
        }
        //if there is any inst before the CallInst to ParallelLoopFunc, split the parallelLoopFuncCallBlock
        if( nb_inst_before > 0 )
        {
            parallelLoopFuncCallBlock->splitBasicBlock(parallelLoopFuncCallInst, "parallelfunc.callblock");
        }
        //if there is any inst after the CallInst to ParallelLoopFunc, split the parallelLoopFuncCallBlock
        if( nb_inst_after > 0 )
        {
            parallelLoopFuncCallBlock->splitBasicBlock(parallelLoopFuncCallInst->getNextNode(), "parallelfunc.continuationblock");
        }
    }
	return true;
}

char ParallelLoopExtract::ID = 0;
//DefinePassCreator(ParallelLoopExtract);
static RegisterPass<ParallelLoopExtract> ParallelLoopExtract("parallelloopextract","extracts the loop that will be executed in parallel in parallel version of apollo loop (apollo_loop_<N>_skeleton_4)");


} // end namespace apollo

//namespace llvm {
//class PassRegistry;
//DeclarePassInitializator(ParallelLoopExtract);
//} // end namespace llvm
//
//INITIALIZE_PASS_BEGIN(ParallelLoopExtract, "APOLLO_INSTRUMENT_MEM_ACCESS",
//		"APOLLO_INSTRUMENT_MEM_ACCESS", false, false)
//	INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
//	INITIALIZE_PASS_END(ParallelLoopExtract,
//			"APOLLO_INSTRUMENT_MEM_ACCESS", "APOLLO_INSTRUMENT_MEM_ACCESS",
//			false, false)

