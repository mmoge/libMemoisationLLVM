/**
 * This version is stamped on May 10, 2016
 *
 * Contact:
 *   Louis-Noel Pouchet <pouchet.ohio-state.edu>
 *   Tomofumi Yuki <tomofumi.yuki.fr>
 *
 * Web address: http://polybench.sourceforge.net
 */
/* gemm.c: this file is part of PolyBench/C */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#define NI 200
#define NJ 50
#define NK 30

#define TMAX 200

/*__attribute__((inline)) inline*/ void add_inline( int* a, int* b )
{
    *a += *b;
}

/*__attribute__((always_inline)) inline*/ void add_A_inline( double* A, int* i, double* x )
{
    if ( *i > 0 )
    {
        for ( int k = 0; k < (*i); ++k )
        {
            A[*i] += k;
        }
    }
    A[*i] += *x;
}

int main(int argc, char** argv)
{
  /* Retrieve problem size. */
  int ni = NI;
  int nj = NJ;
  int tmax = TMAX;
  
  /* Variable declaration/allocation. */
  int i=0;
  int j=0;
  double alpha = 1.5;
//  double* A = (double*) malloc(ni * sizeof(double));
  double* A = (double*) malloc((ni+1) * sizeof(double));
  

      /* Initialize array(s). */
//    #pragma apollo dcop
    {
      for (i = 0; i < ni+1; i++) {
	    A[i] = 1.*i;
//        printf("SALUT 1\n");
//            printf("loop 0:    A[%d] = %f !!!\n", i,A[i]);
      }
    }
    int ione = 1;
for( int t=0; t<tmax; ++t )
{
      /* Run kernel. */
    //#pragma apollo dcop islocal i j tmp itmp isshared A isprivate ione
    {
    #pragma apollomem islocal i j tmp itmp isshared A
    {
      for (i = 0; i < ni; i++) {
     // int j;
        for (j = 0; j < nj; j++) {
//            printf("loop 1:    A[%d] = %f *= %d !!!\n", i,A[i],j+1);
//	        A[i] += (j+1);
            double tmp = j+1;
            int itmp = i+1;
	        add_A_inline(A,&i,&tmp);
//            printf("loop 1:    A[%d] = %f *= %d !!!\n", i+1,A[i+1],j+1);
//	        A[i+1] += (j+1);
	        add_A_inline(A,&itmp,&tmp);
	        
	        add_inline(&j,&ione);
//            printf("loop 1:    j = %d | &j = %d !!!\n", j, &j);
	    }
      }
    }
    }
}
    /* print. */
    for (i = 0; i < ni; i++) {
        printf("%f\n",A[i]);
    }

  /* Be clean. */
  free(A);

  return 0;
}
